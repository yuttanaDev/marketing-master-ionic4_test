import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class nameteamService {
  userData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

 

  getData(postData: any): Observable<any> {
    return this.httpService.post('survey/showemployyee', postData);
  }
  postData(postData: any): Observable<any> {
    return this.httpService.post('survey/saveteamname', postData);
  }

}
