// import { resolve } from 'path';
import { Component, Injector, ViewChild, OnInit, ElementRef, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { nameteamService } from "./nameteam.service";
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
// import { Console } from 'console';

@Component({
  selector: 'app-nameteam',
  templateUrl: './nameteam.page.html',
  styleUrls: ['./nameteam.page.scss'],
})
export class NameteamPage implements OnInit {


  public posts: any; // <- I've added the private keyword 
  public searchQuery: string = '';
  public items: any; // <- items property is now of the same type as posts
  public codeData: any;
  public mainCode: any;
  public mainPorduct: any;

  public postsbranch: any;
  public myDate: any;
  public forms: any;
  public form: any;
  public company: any;

  public newbranch: any;
  public docno: any;

  public datacount: any;
  public empname: any;
  public employeecode: any;

  public isShown = true;
  public doc_no: any;
  public branchcode: any;
  public branchname: any;

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public alertCtrl: AlertController,
    public nameteamService : nameteamService,
    ) {

    this.datacount = {};



    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.doc_no = this.router.getCurrentNavigation().extras.state.doc_no;
        this.branchcode = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        //โชว์รายชื่อคนออกสำรวจ
        // this.showemployyee_old();
        this.showemployyee();
      }
    });



  }
  doRefresh() {
    this.isShown = true;
    // this.showemployyee_old();
    this.showemployyee();
  }

  showemployyee(){
    let myData = {
      "docno" : this.doc_no
    };

    // console.log("myData" ,myData);
    //ต่อ NODEJS API SERVICE
    this.nameteamService.getData(myData).subscribe(
      (res : any) => {
        this.empname = res.data;
        this.datacount.teamname = res['data'][0]['team_name'];
        // console.log("response Data : " ,this.datacount.teamname);

      }, error => {
        this.isShown = false;
        //console.log(JSON.stringify(error.json()));
      });
      setTimeout(() => {
        this.isShown = false;
      }, 7000
    );
  }

  
  ngOnInit() {
  }

  // showemployyee_old() {


  //   this.http.get(API_URL + 'showemployyee.php?docno=' + this.doc_no)
  //     .subscribe(data => {
  //       console.log("data!" + data);
  //       if (data != null) {

  //         this.empname = data;
  //         this.datacount.teamname = data[0]['team_name'];
  //         console.log("Show EmpName" + this.datacount.teamname);
  //         this.isShown = false;
  //       } else {
  //         this.isShown = false;
  //       }
  //     }, error => {
  //       console.log("error!");
  //       this.isShown = false;
  //     });
  //   setTimeout(() => {
  //     this.isShown = false;
  //   }, 10000);
  // }


  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }

  async showConfirm() {

    const alert = await this.alertCtrl.create({
      header: 'บันทึกข้อมูล?',
      message: 'ท่านต้องการ บันทึกข้อมูลหรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'บันทึกข้อมูล',
          handler: () => {
            console.log('Agree clicked');
            this.isShown = true;
            // this.saveteamname();
            this.saveteamname_();
          }
        }
      ]
    });

    await alert.present();
   
  }

  saveteamname_(){
    //get value object
    let teamname = this.datacount.teamname;
    //set value data json
    let myDtata = {
      "teamname" : teamname,
      "docno" : this.doc_no
    };
    // console.log("click btn = ", myDtata);

    //POST DATA TO NODEJS API SERVICE 
    this.nameteamService.postData(myDtata).subscribe(
      (res : any) => {
        this.showalert(JSON.stringify(res.data));
        this.isShown = false;
        this.doRefresh();
      }, error => {
        this.isShown = false;

      });
        setTimeout(() => {
        this.isShown = false;
      }, 10000
    );
  }
  // saveteamname() {

  //   // let poquantity = this.datacount.poquantity; 
  //   let teamname = this.datacount.teamname;

  //   this.http.get(API_URL + 'saveteamname.php?docno='
  //     + this.doc_no + '&' + 'employee=' + this.employeecode + '&' + 'branch=' + this.branchcode + '&' + 'teamname=' + teamname)
  //     .subscribe(
  //       data => {

  //         this.showalert(JSON.stringify(data));
  //         this.isShown = false;
  //         this.doRefresh();
  //       }, error => {
  //         this.isShown = false;

  //       });
  //   setTimeout(() => {
  //     this.isShown = false;
  //   }, 10000);
  // }

  async showalert(text) {
    let alert = await this.alertCtrl.create({
      header: 'การบันทึก',
      message: text,
      buttons: ['ตกลง']
    });
    alert.present();
  }


}
