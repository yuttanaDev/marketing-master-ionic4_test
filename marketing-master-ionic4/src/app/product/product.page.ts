import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController,ToastController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { CartPage } from '../cart/cart.page';

import { BASEURL, URLIMG, API_URL,systemsetting } from '../../providers/constants/constants';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';
  showPrev: any;
  showNext: any;
  currentIndex: any;
  public count: any = 1;
  product_barcode: any;
  product_name: any;
  total: number;

  productname: any = null;
  productdesct: any = null;
  barcode_code: any = null;
  vendor_code: any;
  unit_code: any;

  public itemsproduct: any = [];
  public items: any = null;
  public items_image: any = null;

  public profile_usercode: any;

  public newbranch: any;
  public employeecode: any;
  public picturename: any = null;

  public product_price1: any = 0;
  public pro_price: any = 0;
  public stockbalance: any = 0;
  public itemcount: any = 0;

  public idtype: any;

  public posts: any;
  public isShown: any = true;

  public cart: any = [];
  public cartItem: any = [];
  public additem: any = 0;
  public datacart: any = [];
  public totalprice: number;
  public docnoid: any = null;
  public docno: any = null;
  public product_item: any;

  public toast: any;

  base_url: any = systemsetting.base_url;
  base_token: any = systemsetting.token;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    private route: ActivatedRoute,
    public menu: MenuController,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
  ) {

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.product_item = this.router.getCurrentNavigation().extras.state.product_item;
        console.log('product_item', this.product_item);
       /*  this.product_barcode = this.product_item.barcode_code;
        this.product_name = this.product_item.barcode_bill_name;
        this.picturename = this.product_item.picturename;
        this.vendor_code = this.product_item.vendor_code;
        this.unit_code = this.product_item.unit_code; */

        this.product_barcode = this.product_item.pro_barcode;
        this.product_name = this.product_item.pro_name_new;
        this.picturename = this.product_item.pro_img1;
        this.vendor_code = this.product_item.vendor_code;
        this.unit_code = this.product_item.unit_code;

        this.storage.get('newbranch').then((newbranch) => {
          this.newbranch = newbranch;
         this.loadPrice();

        });



      }
    });

    this.loadcart();




  }
  async loadPrice() {

  
    this.isShown = true;
    let url = this.base_url + 'ProductsBranch/getproductprice';
    let myData = {
      employeecode: this.employeecode,
      branch_code: this.newbranch,
      token: this.base_token,
      barcode_code: this.product_barcode,
    };
    this.http.post(url, JSON.stringify(myData))
      .subscribe(data => {

        if(data != null){
          this.posts = data;

          if(this.posts.length > 0){
            this.product_price1 = this.posts[0].pro_sale_price;
            this.stockbalance = this.posts[0].stockonsale;
            let pro_price = this.posts[0].pro_price;
            //this.idtype = this.posts[0].idtype;
            if (pro_price == null || pro_price == undefined || pro_price == '') {
              this.pro_price = 0;
            } else {
              this.pro_price = pro_price;
            }
          }
        }else{
          this.posts = null;
        }
        

        

        console.log('typecustomer_list', this.posts);
        this.isShown = false;
      }, error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      });

    setTimeout(() => {
      this.isShown = false;
    }, 10000);

  }

  loadcart() {
    this.total = 0;
    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    // console.log("data1", this.cartItem);
    this.cart = this.cartItem;
    console.log("01count:" + this.cart, this.cartItem);
    if (this.cart != null) {
      this.cart.forEach(item => {  //ตัวอย่าง foreach
        this.total += item.subtotal;  //ยอดรวม
      });
      this.itemcount = parseInt(this.cart.length);
    }
  }

  ngOnInit() {
  }

  async itemCountProduct() {

    let prompt = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'กรอกจำนวนสินค้า',
      message: "",
      inputs: [
        {
          type: 'number',
          name: 'countitem',
          placeholder: '0',
          max: this.stockbalance,
        },
      ],
      buttons: [
        /* {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }, */
        {
          text: 'ตกลง',
          handler: async (data) => {
            console.log('stockbalance', this.stockbalance - this.count);
            if (data.countitem != '') {
              if ((this.stockbalance) >= (parseInt(data.countitem))) {
                console.log('countitem', (parseInt(data.countitem)));
                if ((parseInt(data.countitem)) > 0) {
                  this.additem = (parseInt(data.countitem));
                  console.log('additem', this.additem);
                  this.itemCount(this.additem);
                }
                else {
                }
              }
              else {
                console.log('จำนวนเยอะกว่าสต๊อคที่มีอยู่', data.countitem);
                let prompt = await this.alertCtrl.create({
                  cssClass: 'alertCustomCss',
                  header: 'จำนวนที่กรอกมากไป',
                  message: "จำนวนเยอะกว่าสต๊อคที่มีอยู่ สต็อคที่มี : " + (this.stockbalance - this.count),
                  buttons: [
                    /* {
                      text: 'Cancel',
                      handler: data => {
                        console.log('Cancel clicked');
                      }
                    }, */
                    {
                      text: 'ตกลง',
                      handler: data => {
                      }
                    },
                  ]
                });
                prompt.present();
                setTimeout(() => {
                  prompt.dismiss();
                }, 3000);
              }
            }
          }
        }
      ]
    });
    prompt.present();


  }

  itemCount(countitem: any) {

    //this.count = this.count + 1;

    this.count = countitem;

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.cartItem == null || this.cartItem == '') {
      this.itemcount = 0;
    } else {
      this.datacart = this.cartItem;
      this.itemcount = parseInt(this.datacart.length);

      for (var i = 0; i < this.itemcount; i++) {
        if (this.product_barcode == this.datacart[i].product_barcode) {
          //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน      
          console.log('Update :', this.count + this.datacart[i].count);
          if (this.stockbalance >= (this.count + this.datacart[i].count)) {

            this.datacart[i].count += this.count;

            console.log('Update datacart count :', this.datacart[i].count);


            if (this.datacart[i].pro_price > 0) {
              this.totalprice = (this.datacart[i].pro_price * this.datacart[i].count);

              console.log('Update totalprice1 :', this.totalprice);

            } else {
              this.totalprice = (this.datacart[i].product_price1 * this.datacart[i].count);
              console.log('Update totalprice1 :', this.totalprice);
            }

            this.datacart[i].subtotal = (this.totalprice);

            console.log('Update bar subtotal :', this.datacart[i].subtotal);

          } else {
            console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
          }
          break;
        }
      }
      /* this.cart = this.cartItem
      localStorage.setItem('cart', JSON.stringify(this.cart));
      //console.log('Insert bar2:', this.cart);

      let cartItem = JSON.parse(localStorage.getItem('cart'));
        console.log("Insert cartItem", cartItem);
        if(this.cartItem != null){
          this.itemcount = parseInt(this.cartItem.length);
        } */

    }

  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
    this.count = 1;
    this.cart = [];
    this.cartItem = [];
    this.datacart = [];

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
      });
    });
    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];
      console.log("docnoid", docnoItem[0]['docnoid']);
      this.itemcountproduct();
    } else {
      var data = JSON.parse(localStorage.getItem('cart'));
      if (data != null) {
        this.itemcount = parseInt(data.length);
        console.log("data1", data);
      }
    }


  }

  itemcountproduct() {
    this.http.get(API_URL + 'OrderNow/itemcount_product.php?sellerid=' + this.docnoid)
      .subscribe(data => {
        if (data != null) {
          console.log('itemcount_product : ', data);
          this.itemcount = parseInt(data[0]['countitem']);
        } else {
          this.itemcount = 0;
        }
      }, error => {

        console.log(JSON.stringify(error.json()));
      });
  }


  ionViewDidLoad() {
    this.menu.enable(false);

  }
  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }



  showProduct() {

    let url = API_URL + "OrderNow/productdetail.php";
    let myData = {
      employeecode: this.employeecode,
      branchcode: this.newbranch,
      product_barcode: this.product_barcode,
    };
    this.http.post(url, JSON.stringify(myData))
      .subscribe(data => {

        console.log('data', data);
        // let callback = JSON.parse(data);
        this.items = data;
        this.productname = data[0]['barcode_bill_name'];
        this.productdesct = data[0]['productdesct'];
        this.barcode_code = data[0]['barcode_code'];

        this.picturename = data[0]['picturename'];

        console.log('picturename', this.picturename);




      }, error => {

        // console.log(JSON.stringify(error.json()));
      });
    setTimeout(() => {

    }, 10000);

  }

  back() {
    this.navCtrl.pop();
  }
  add() {
    if (this.stockbalance > this.count) {
      this.count = this.count + 1;
    }

  }

  sub() {
    if (this.count > 1) {
      this.count = this.count - 1;
    }

  }

  addcartproduct() {
    if (this.product_price1 > 0 || this.pro_price > 0) {
      if (this.docnoid != null) {

        let link = API_URL + 'OrderNow/Addhave_itemproduct.php';
        let myData = {
          branchcode: this.newbranch,
          employeecode: this.employeecode,
          product_barcode: this.product_barcode,
          product_name: this.product_name,
          picturename: this.picturename,
          product_price1: this.product_price1,
          stockbalance: this.stockbalance,
          pro_price: this.pro_price,
          count: this.count,
          subtotal: this.total,
          vendor_code: this.vendor_code,
          unit_code: this.unit_code,
          sellerdtid: this.docnoid,
          typeadd_sub: 3 //ทำการลด item
        };

        this.http.post(link, JSON.stringify(myData))
          .subscribe(data => {
            //loader.dismissAll();
            console.log("Sub data:" + data);
            if (data != null) {

               //toast เพิ่มสินค้านี้ไปยังตระกร้าสินค้าแล้ว
              this.addtoastApp();

              this.itemcountproduct();
            } else {
            }

          }, error => {
            //loader.dismissAll();
            //console.log(JSON.stringify(error.json()));
          });

      } else {
        this.addcart();
      }

    } else {

    }

  }

  addtoastApp(){
    this.toast = this.toastCtrl.create({
      message: 'เพิ่มสินค้าไปยังรถเข็น เรียบร้อยแล้ว',
      duration: 3000,
      position: 'bottom',
      animated: true,
      cssClass: "toastgb"
    }).then((toastData) => {
      console.log(toastData);
      this.loadcart();
      toastData.present();
    });
  }

  addcart() {

    if (this.pro_price > 0) {
      this.total = (this.pro_price * this.count);
    } else {
      this.total = (this.product_price1 * this.count);
    }


    //this.popupLoading();

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    console.log("cartItem", this.cartItem);
    if (this.cartItem != null) {
      this.itemcount = parseInt(this.cartItem.length);
    }

    if (this.cartItem == null || this.cartItem == '') {

      this.cart.push({
        product_barcode: this.product_barcode,
        product_name: this.product_name,
        picturename: this.picturename,
        product_price1: this.product_price1,
        stockbalance: this.stockbalance,
        pro_price: this.pro_price,
        count: this.count,
        subtotal: this.total,
        vendor_code: this.vendor_code,
        unit_code: this.unit_code,

      });
      //this.storage.set('cart', JSON.stringify(this.cart));
      localStorage.setItem('cart', JSON.stringify(this.cart))
      console.log('Insert bar:', this.cart);

      this.cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("this.cartItem 2", this.cartItem);
      if (this.cartItem != null) {
        this.itemcount = parseInt(this.cartItem.length);
      }

    }
    else {
      console.log("update");
      //get product in cart to store

      this.cartItem = JSON.parse(localStorage.getItem('cart'));
      if (this.cartItem == null || this.cartItem == '') {
        this.itemcount = 0;
      } else {
        this.datacart = this.cartItem;
        this.itemcount = parseInt(this.datacart.length);

        for (var i = 0; i < this.itemcount; i++) {
          if (this.product_barcode == this.datacart[i].product_barcode) { //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน       
            if (this.stockbalance >= (this.count + this.datacart[i].count)) {
              this.datacart[i].count += this.count;
              this.datacart[i].subtotal = this.datacart[i].subtotal + this.total;
            } else {
              console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
            }
            break;
          } else if ((i + 1) >= this.itemcount) {  //ถ้าไม่มีให่เริ่มใหม่
            this.cartItem.push({
              product_barcode: this.product_barcode,
              product_name: this.product_name,
              picturename: this.picturename,
              product_price1: this.product_price1,
              stockbalance: this.stockbalance,
              pro_price: this.pro_price,
              count: this.count,
              subtotal: this.total,
              vendor_code: this.vendor_code,
              unit_code: this.unit_code,

            });

            break;
          }
        }
        this.cart = this.cartItem
        localStorage.setItem('cart', JSON.stringify(this.cart))
        console.log('Insert bar2:', this.cart);

        let cartItem = JSON.parse(localStorage.getItem('cart'));
        console.log("Insert cartItem", cartItem);
        if (this.cartItem != null) {
          this.itemcount = parseInt(this.cartItem.length);
        }

        //toast เพิ่มสินค้านี้ไปยังตระกร้าสินค้าแล้ว
        this.addtoastApp();

      }

      /* data.forEach(item => {  //ตัวอย่าง foreach
          console.log('bar:', item.barcode+" price: "+item.price);
     }); */
      // this.storage.set('cart',JSON.stringify(this.cart));  


    }

    // }); // Cart Storate


    // this.navCtrl.push(CartPage);
  }


  addcartnowproduct() {

    if (this.product_price1 > 0 || this.pro_price > 0) {

    if (this.docnoid != null) {

      let link = API_URL + 'OrderNow/Addhave_itemproduct.php';
      let myData = {
        branchcode: this.newbranch,
        employeecode: this.employeecode,
        product_barcode: this.product_barcode,
        product_name: this.product_name,
        picturename: this.picturename,
        product_price1: this.product_price1,
        stockbalance: this.stockbalance,
        pro_price: this.pro_price,
        count: this.count,
        subtotal: this.total,
        vendor_code: this.vendor_code,
        unit_code: this.unit_code,
        sellerdtid: this.docnoid,
        typeadd_sub: 3 //ทำการลด item
      };

      this.http.post(link, JSON.stringify(myData))
        .subscribe(data => {
          //loader.dismissAll();
          console.log("Sub data:" + data);
          if (data != null) {
            /* this.navCtrl.push(CartitemPage, {
              sellerid: this.docnoid,
            }); */
          } else {
          }

        }, error => {
          //loader.dismissAll();
          //console.log(JSON.stringify(error.json()));
        });



    } else {
      this.addcartnow();
    }

  } else {

  }

  }

  async addcartnow() {

    if (this.pro_price > 0) {
      this.total = (this.pro_price * this.count);
    } else {
      this.total = (this.product_price1 * this.count);
    }


    //this.popupLoading();

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    console.log("cartItem", this.cartItem);
    if (this.cartItem != null) {
      this.itemcount = parseInt(this.cartItem.length);
    }

    if (this.cartItem == null || this.cartItem == '') {

      this.cart.push({
        product_barcode: this.product_barcode,
        product_name: this.product_name,
        picturename: this.picturename,
        product_price1: this.product_price1,
        stockbalance: this.stockbalance,
        pro_price: this.pro_price,
        count: this.count,
        subtotal: this.total,
        vendor_code: this.vendor_code,
        unit_code: this.unit_code,

      });
      //this.storage.set('cart', JSON.stringify(this.cart));
      localStorage.setItem('cart', JSON.stringify(this.cart))
      console.log('Insert bar:', this.cart);

      this.cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("this.cartItem 2", this.cartItem);
      if (this.cartItem != null) {
        this.itemcount = parseInt(this.cartItem.length);
      }

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: null,
          docuno: null,
          idtype: null,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage', data);
        if (data == null) {


        } else {

        }

        this.loadcart();

      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });

    }
    else {
      console.log("update");
      //get product in cart to store

      this.cartItem = JSON.parse(localStorage.getItem('cart'));
      if (this.cartItem == null || this.cartItem == '') {
        this.itemcount = 0;
      } else {
        this.datacart = this.cartItem;
        this.itemcount = parseInt(this.datacart.length);

        for (var i = 0; i < this.itemcount; i++) {
          if (this.product_barcode == this.datacart[i].product_barcode) { //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน       
            if (this.stockbalance >= (this.count + this.datacart[i].count)) {
              this.datacart[i].count += this.count;
              this.datacart[i].subtotal = this.datacart[i].subtotal + this.total;
            } else {
              console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
            }
            break;
          } else if ((i + 1) >= this.itemcount) {  //ถ้าไม่มีให่เริ่มใหม่
            this.cartItem.push({
              product_barcode: this.product_barcode,
              product_name: this.product_name,
              picturename: this.picturename,
              product_price1: this.product_price1,
              stockbalance: this.stockbalance,
              pro_price: this.pro_price,
              count: this.count,
              subtotal: this.total,
              vendor_code: this.vendor_code,
              unit_code: this.unit_code,

            });

            break;
          }
        }
        this.cart = this.cartItem
        localStorage.setItem('cart', JSON.stringify(this.cart))
        console.log('Insert bar2:', this.cart);

        let cartItem = JSON.parse(localStorage.getItem('cart'));
        console.log("Insert cartItem", cartItem);
        if (this.cartItem != null) {
          this.itemcount = parseInt(this.cartItem.length);
        }

        let modal = await this.modalController.create({
          component: CartPage,
          componentProps: {
            sellerid: null,
            docuno: null,
            idtype: null,
          },
          showBackdrop: true,
          backdropDismiss: true,
          enterAnimation: myEnterAnimation,
          leaveAnimation: myLeaveAnimation
        });
        modal.onWillDismiss().then(dataReturned => {
          let data = dataReturned.data;
          console.log('CartPage', data);
          if (data == null) {


          } else {

          }
          this.loadcart();

        });
        return await modal.present().then(_ => {
          // triggered when opening the modal
        });

      }

    }



  }

  addcartlist() {

    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];
      console.log("docnoid", docnoItem[0]['docno']);
      /* this.navCtrl.push(CartitemPage, {
        sellerid: this.docnoid,
        docuno: this.docno
      }
      ); */
    } else {
      //this.navCtrl.push(CartPage);
    }

  }

  async addcartnowShow() {

    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];
      console.log("docnoid", docnoItem[0]['docno']);
      /* let navigationExtras: NavigationExtras = {
        state: {
          sellerid: this.docnoid,
          docuno: this.docno
        }
      };
      this.router.navigate(['cart'], navigationExtras); */

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: this.docnoid,
          docuno: this.docno,
          idtype: this.idtype,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage', data);
        if (data == null) {


        } else {

        }
        this.loadcart();

      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });


    } else {

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: null,
          docuno: null,
          idtype: null,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage', data);
        if (data == null) {


        } else {

        }
        this.loadcart();
      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });

    }



  }

}
