import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AddlocationPage } from './addlocation.page';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';
// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';
 
// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import mscombi2D from 'fusioncharts/viz/mscombi2d'; 
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
 
// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

const routes: Routes = [
  {
    path: '',
    component: AddlocationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    NgxIonicImageViewerModule,
    TranslateModule,
     // Specify FusionChartsModule as import
     FusionChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddlocationPage],
  providers: [
    LocationAccuracy,
    Geolocation,
    Camera,
  ],
})
export class AddlocationPageModule {}
