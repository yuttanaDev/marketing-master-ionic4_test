import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController, ToastController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { URLIMG, API_URL } from '../../providers/constants/constants';

import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from './../services/http.service';
declare var google;
declare var AdvancedGeolocation: any;

const BASEURL = API_URL;
const BASEURLIMG = API_URL + 'UploadedLocation/';

@Component({
  selector: 'app-addlocation',
  templateUrl: './addlocation.page.html',
  styleUrls: ['./addlocation.page.scss'],
})
export class AddlocationPage implements OnInit {

  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';


  newbranch: any;
  branchname: any;
  datacount: any = {};

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;

  public datacustomer: any = null;
  public base64Image: string;
  public idLanguage: number = 2;
  public togglecus: any = false;
  public togglecusnew: any = true;

  public items_img: any;
  public items_cus: any;

  fullname: any;
  latLng: any;

  pictureimg: any;
  public docno: any;
  public val: any;
  data: any = {};
  coords: any = {};
  id_suvey: any;
  id: any;
  catid: any = null;
  maxid: any;
  type_sub: any;
  idedit: any = null;
  imgupload: any;
  employeecode: any;
  public customertype: any;
  categories: any;
  selectedCategories: any;
  productcategory: any;
  gbh_customer_id: any;
  public identity: boolean = false;

  firstname: any;
  lastname: any;
  identification_card: any;
  mobile: any;
  full_address: any;
  email: any;
  age: any;

  itemstypecus: any;
  subtyp: any;
  date: any;

  public approve: boolean;
  imglink: any = BASEURLIMG;
  private items2: any;

  private names: Array<string>;

  images: any = null;
  cutomer_img: any;

  public photos: any = [];
  public toggle: any = 0;
  public datalength: any=null;
  // images: Array<string>;  
  grid: Array<Array<string>>; //array of arrays
  select_survey_type: any;
  load: any;
  //authForm: FormGroup;
  public itemscus: any = [];
  public itemsimg: any = null;
  information: any[];

  public width: any;
  public height: any;
  public type: any;
  public dataFormat: any;
  public dataSource: any;

  public togglecustomers: any;
  public typesubcusitem: any;

  public subtypecus: any = 0;

  public items: any = [];
  public post: any = [];
  public itemstype: any;
  public itemssub: any;
  public items_subtype: any;


  pizzaIng: any;
  isAllSelected: Boolean = false;

  public items_data: any = [];
  public items_score: any = [];

  public typeid: any = [];

  public score_net: any = 0;
  public score_pay: any = 0;
  public balance_score: any = 0;

  public visible = [];
  // public authForm: FormGroup;
  //public loginForm: FormGroup;

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public toastCtrl: ToastController,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    public alertCtrl: AlertController,
    public HttpService : HttpService,
    // public formBuilder: FormBuilder,
    private camera: Camera,
  ) {

    if (this.platform.is('android')) {

      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if (canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => console.log('Request successful'),
            error => console.log('Error requesting location permissions', error)
          );
        }

      });

    }

    this.datacount = {};
    this.width = '100%';
    this.height = '50%';
    this.type = 'Column2D';
    this.dataFormat = 'json';
    this.dataSource;

    /* this.authForm = formBuilder.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
      'mobile': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      'email': [null],
    });
*/
    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.docno;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        this.catid = this.router.getCurrentNavigation().extras.state.catid;
        this.datacount.customertype = this.router.getCurrentNavigation().extras.state.id;
        console.log('customertype', this.datacount.customertype);
        let subtypecus = this.router.getCurrentNavigation().extras.state.id;
        if (subtypecus == 2) {
          this.datacount.typesub = '1019';
        } else if (subtypecus == 12) {
          this.datacount.typesub = '1003';
        } else if (subtypecus == 1) {
          this.datacount.typesub = '1003';
        }
        //โชว์รายชื่อคนออกสำรวจ
        //this.loadData();
        console.log("catid", this.catid, "id", this.id);

        this.date = new Date();

        let typeitem = JSON.parse(localStorage.getItem('items_type'));
        console.log("items_type", subtypecus);
        this.itemstype = typeitem['items_type'];
        this.itemssub = typeitem['items_sub'];

        if (this.catid == null || this.catid == '') {
          this.loadMap();
          console.log("catid!" + this.catid);
        } else {
          this.addEditid();
          console.log("catid234!" + this.catid);
        }

        this.subtype(subtypecus);

      }
    });


    //this.subtypecustomer();

    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });



  }

  doRefresh() {
    this.addEditid();
  }

  fntogglecusnew() {
    if (this.togglecusnew == true) {
      this.togglecusnew = false;
      console.log('false');
    } else {
      this.togglecusnew = true;
      console.log('true');
    }
  }

  public subtype(subtypecus) {
    this.typesubcusitem = JSON.parse(localStorage.getItem('typesubcus'));
    // console.log("typesubcusitem", this.typesubcusitem,this.subtypecus);
    this.post = this.typesubcusitem.filter(function (item) {
      return item.id_type == subtypecus;
    });

    if (this.post.length > 0) {
      this.datacount.type_input = parseInt(this.post[0].id_sub_type);
      console.log(parseInt(this.post[0].id_sub_type));
    }
    this.items = this.post;
  }
  isValidNumber(event) {
    //console.log(event,'on a desktop device.',event.length );
    //return /\d|Backspace/.test(event.key);
    if (event.length == 13) {
      // backspace, enter, escape, arrows
      this.identity = true;
      //console.log('identity if.',this.identity );
    } else {
      this.identity = false;
      // console.log('identity else.',this.identity );
    }
  }

  searchForTerm() {
    this.images = [];
    this.items_data = [];

    let val2 = this.val;
    console.log('Sentar clicked' + val2);


    // let link = BASEURL + "checkscorecustomer_location.php?";
    // console.log('link data' + link);
    // this.http.post(link + 'idcard=' + val2)
    let myData = {
      idcard: val2,
      newbranch: this.newbranch,
    };

 

    // this.http.post(link, JSON.stringify(myData))
    this.HttpService.post("survey/checkscorecustomerlocation",myData).subscribe(data => {
        console.log('Sentar data', data);
        if (data != null) {
          //console.log('Sentar data' , data['items_score'] , data['items_data'] , data['items_img'] , data['items_subtype']);
          //this.datacustomer = data;

          this.items_score = data['items_score'];
          if (this.items_score.length > 0) {
            this.score_net = this.items_score[0].score_net;
            this.score_pay = this.items_score[0].score_pay;
            this.balance_score = this.items_score[0].balance_score;
          }

          console.log('items_score' + this.items_score);


          this.items_data = data['items_data'];

          /* this.images = data['items_img'];
          let subtype = data['items_subtype'];
          subtype.forEach(element => {
            let type = element.customer_subtype_id;
            this.typeid.push(type);
          });

          this.datacount.membership_type_globalclub = this.typeid; */

          console.log('items_data', this.items_data, this.items_score);

          if (this.items_data.length > 0) {

            this.gbh_customer_id = this.items_data[0]['gbh_customer_id'];
            this.datacount.cusid = this.items_data[0]['customer_barcode'];
            this.datacount.firstname = this.items_data[0]['firstname'];
            this.datacount.lastname = this.items_data[0]['lastname'];
            this.datacount.age = this.items_data[0]['age'];
            this.datacount.full_address = this.items_data[0]['full_address'];
            this.datacount.identification_card = this.items_data[0]['identification_card'];
            this.datacount.mobile = this.items_data[0]['mobile'];
            this.datacount.email = this.items_data[0]['email'];
            this.datacount.province_name = this.items_data[0]['province_name'];
            this.datacount.amphur_name = this.items_data[0]['amphur_name'];
            this.datacount.district_name = this.items_data[0]['district_name'];
            this.datacount.sex = this.items_data[0]['sex'];
            this.datacount.customer_type_globalclub = this.items_data[0]['customer_type_id'];
            // this.datacount.membership_type_globalclub = this.items_data[0]['member_type_id'];

            this.datacount.facebook = this.items_data[0]['facebook'];
            this.datacount.title_name = this.items_data[0]['titlename'];
            this.datacount.birthday = this.items_data[0]['date_birthday'];
            this.datacount.housenumber = this.items_data[0]['house_no'];
            this.datacount.province_id = this.items_data[0]['province_id'];
            this.datacount.amphur_id = this.items_data[0]['amphur_id'];
            this.datacount.district_id = this.items_data[0]['district_id'];
            this.datacount.postcode = this.items_data[0]['zipcode'];
            this.newbranch = this.items_data[0]['branch_code'];
            this.datacount.branchname = this.items_data[0]['branch_name'];
            //this.flag_newcus = '0';

            // this.coords.latitude = this.items_data[0]['latitude'];
            // this.coords.longitude = this.items_data[0]['longitude'];

            var date = new Date(this.datacount.birthday);

            console.log('date', date.toDateString());
            let date2 = new Date(date.getFullYear() + 543, date.getMonth(), date.getDate());
            this.datacount.birthday = this.datepipe.transform(date2, 'yyyy-MM-dd');
            console.log('birthday', this.datacount.birthday);

          }

          this.dataSource = {
            "chart": {
              "caption": "คะแนน Global Club",
              "palettecolors": "#0075c2,#FFC107,#1aaf5d",
              "showValues": "1",
              "theme": "fusion",

            },
            "data": [{
              "label": "คะแนนทั้งหมด",
              "value": this.score_net
            }, {
              "label": "คะแนนที่ใช้",
              "value": this.score_pay
            }, {
              "label": "คะแนนคงเหลือ",
              "value": this.balance_score
            }]
          };


        } else {
          /* let alert = this.alertCtrl.create({
            title: 'ไม่มีข้อมูล ลองอีกครั้ง',
            subTitle: 'ไม่มีข้อมูลหรือแสกนใหม่อีกครั้ง',
            buttons: ['ตกลง']
          });
          alert.present();
          loader2.dismissAll();
          console.log('ไม่มีข้อมูล' + data); */
        }

      },
        error => {
          //this.showError(error);
          /* let alert = this.alertCtrl.create({
            title: 'ไม่มีข้อมูล Error',
            subTitle: JSON.stringify(error),
            buttons: ['ตกลง']
          });
          alert.present();
          loader2.dismissAll(); */
        });

  }

  public subtypecustomer() {

    /**** Code เพิ่ม ....  ***/
    let myData = {subtyp:this.datacount.customertype,subtypecus: this.datacount.type_input};
    this.HttpService.post("survey/subtypecustomer",myData).subscribe(
      (res : any) => {
        if(res != null){
          this.itemstypecus = res;
          console.log("itemstypecus xxxx !", this.itemstypecus);
        }
      },error => {
        console.log("itemstypecus error!");
      });
    /****** End  *****/

    // console.log("itemstypecus!", this.datacount.customertype, this.datacount.type_input);
    // this.http.get(BASEURL + 'cat_type_customer.php?subtyp=' + this.datacount.customertype + '&' + 'subtypecus=' + this.datacount.type_input)
    //   .subscribe(data => {
    //     if (data != null) {
    //       this.itemstypecus = data;
    //       console.log("itemstypecus!", this.itemstypecus);
    //     } else {

    //     }
    //   }, error => {
    //     console.log("itemstypecus error!");
    //   });
  }



  onSelectChange(selectedValue) {
    console.log('Selected', selectedValue);
    console.log('Selected-detail', selectedValue.detail);
    console.log('Selected-detail', selectedValue.detail.value);
    let length = selectedValue.detail.value;
    if (length.length == 10) {
      console.log(" No1 length!", length.length);
      this.datalength = '';
    } else if (length.length < 10) {
      this.datalength = '* หมายเลขเบอร์โทรศัพท์ต้องมี 10 หลัก!';
      console.log(" No2 length!", length.length);
    } else if (length.length > 10) {
      this.datalength = '* หมายเลขเบอร์โทรศัพท์ต้องมี 10 หลัก!';
      console.log(" No3 length!", length.length);
    } else {
      this.datalength = '* หมายเลขเบอร์โทรศัพท์ต้องมี 10 หลัก!';
    }
  }

  addItemcustomer() {
    console.log("count:" + this.itemscus.length);
    this.itemscus.push({
      customer_code: this.datacount.customer_code,
      customer_name: this.datacount.customer_name,
    });
    this.cleanItemcustomer();
  }
  submitForm() {
    console.log('Form submitted!')

    this.itemscus.push({
      username: this.datacount.addusername,
      mobile: this.datacount.addmobile,
      email: this.datacount.addemail
    });
    this.togglecusnew = false;
    this.cleanItemcustomer();
  }
  cleanItemcustomer() {

    this.datacount.addusername = '';
    this.datacount.addmobile = '';
    this.datacount.addemail = '';

  }
  removeItemcustomer(index, slidingItem: IonItemSliding) {
    slidingItem.close();
    console.log("count:" + this.itemscus.length);
    if (this.itemscus.length == 1) {
      this.itemscus.splice(index, 1);
    } else {
      this.itemscus.splice(index, 1);
    }
  }

  async removeItemcustomerOnServer(id_cus) {

    let confirm = await this.alertCtrl.create({
      header: "คุณต้องการลบลูกค้ารายนี้หรือไม่ ไม่มีการเลิกทำ!",
      message: "",
      buttons: [
        {
          text: "เลิกทำ",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "ตกลง",
          handler: () => {
            console.log("Agree clicked", id_cus);

            // this.http.get(BASEURL + 'Delete_suvey_add_customer.php?id_cus=' + id_cus)
            let myData = {id_cus:id_cus};
            this.HttpService.post("survey/deletesuveyaddcustomer",myData).subscribe(data => {
                console.log(data);
                this.addEditid();
              }, error => {
                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    confirm.present();

  }
  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'md-images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          icon: 'md-camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
  takePicture(sourceType) {
    var option = {
      quality: 75,
      targetWidth: 960,
      targetHeight: 690,
      correctOrientation: true,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(option).then((imageData) => {

      if (sourceType == 0) {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      } else {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      }

    }, (err) => {
      this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
      //alert(JSON.stringify(err));
    });
  }
  async deletePhoto(index) {
    let confirm = await this.alertCtrl.create({
      header: "แน่ใจหรือไม่ว่าคุณต้องการลบภาพนี้? ไม่มีการเลิกทำ!",
      message: "",
      buttons: [
        {
          text: "เลิกทำ",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "ตกลง",
          handler: () => {
            console.log("Agree clicked");
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }
  async deletePhotoOnServer(index) {
    let confirm = await this.alertCtrl.create({
      header: "แน่ใจหรือไม่ว่าคุณต้องการลบภาพนี้? ไม่มีการเลิกทำ!",
      message: "",
      buttons: [
        {
          text: "เลิกทำ",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "ตกลง",
          handler: () => {
            console.log("Agree clicked", index);

            // this.http.get(BASEURL + 'Delete_survey_mobile_img.php?id_img=' + index)
            let myData = {id_img:index};
            this.HttpService.post("survey/dletesurveymobileimg",myData).subscribe(data => {
                console.log(data);
                this.addEditid();
              }, error => {
                //console.log(JSON.stringify(error.json()));
              });

          }
        }
      ]
    });
    confirm.present();
  }
  async notify() {
    if (this.approve == false) {
      let confirm = await this.alertCtrl.create({
        header: 'เปลี่ยนสถานะแก้ไขข้อมูลนี้?',
        message: 'ท่านต้องการเปลี่ยนสถานะข้อมูลนี้หรือไม่?',
        buttons: [
          {
            text: 'ยกเลิก',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              console.log('Agree clicked');

              // this.http.get(BASEURL + 'chang_catagory_approve.php?approve=' + this.approve + '&' + 'editid=' + this.catid)
              let myDataset = {approve:this.approve ,editid:this.catid };
              this.HttpService.post("survey/changcatagoryapprove",myDataset).subscribe(data => {
                  if (data != null) {


                  } else {

                  }
                }, (err) => {
                  console.log(err);
                });
              setTimeout(() => {

              }, 30000);

            }
          }
        ]
      });
      confirm.present();


    } else if (this.approve == true) {

      let confirm = await this.alertCtrl.create({
        header: 'ยืนยันข้อมูลสำเร็จ เสร็จสิ้น?',
        message: 'ท่านต้องการเปลี่ยนสถานะยืนยันข้อมูลนี้ เป็นสถานะสำเร็จหรือไม่?',
        buttons: [
          {
            text: 'ยกเลิก',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              console.log('Agree clicked');
              this.http.get(BASEURL + 'chang_catagory_approve.php?approve=' + this.approve + '&' + 'editid=' + this.catid)
                .subscribe(data => {
                  if (data != null) {


                  } else {

                  }
                }, (err) => {
                  console.log(err);
                });
              setTimeout(() => {

              }, 30000);

            }
          }
        ]
      });
      confirm.present();
    }
    console.log("Toggled: " + this.approve);
  }

  async showConfirmMap() {

    let confirm = await this.alertCtrl.create({
      header: 'ท่านต้องการเปิดตำเหน่งที่อยู่ปัจจุบัน?',
      message: 'เปิดตำเหน่งที่อยู่ปัจจุบันหรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Agree clicked');

            if (this.platform.is('android')) {

              this.locationAccuracy.canRequest().then((canRequest: boolean) => {

                if (canRequest) {
                  // the accuracy option will be ignored by iOS
                  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                    () => console.log('Request successful'),
                    error => console.log('Error requesting location permissions', error)
                  );
                }

              });

            }
            this.loadMap();

          }
        }
      ]
    });
    confirm.present();
  }
  loadMap() {

    if (this.platform.is('mobile')) {
      let options = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 30000
      };

      this.geolocation.getCurrentPosition(options).then((position) => {
        //this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.coords.latitude = position.coords.latitude;
        this.coords.longitude = position.coords.longitude;
        // this.showmap(this.latLng);

        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.showmap(this.latLng);


      }, (err) => {
        console.log(err);
      });

    }
    else if (this.platform.is('mobileweb')) {
      let options = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 30000
      };
      this.geolocation.getCurrentPosition(options).then((position) => {
        this.coords.latitude = position.coords.latitude;
        this.coords.longitude = position.coords.longitude;
        // this.showmap(this.latLng);
        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.showmap(this.latLng);
      }, (err) => {
        console.log(err);
      });

    } else if (this.platform.is('android')) {
      this.platform.ready().then(() => {
        AdvancedGeolocation.start((success) => {
          //loading.dismiss();
          // this.refreshCurrentUserLocation();
          try {
            var jsonObject = JSON.parse(success);
            console.log("Provider " + JSON.stringify(jsonObject));
            switch (jsonObject.provider) {
              case "gps":
                console.log("setting gps ====<<>>" + jsonObject.latitude);
                this.latLng = new google.maps.LatLng(jsonObject.latitude, jsonObject.longitude);

                this.coords.latitude = jsonObject.latitude;
                this.coords.longitude = jsonObject.longitude;
                this.showmap(this.latLng);
                break;

              case "network":
                console.log("setting network ====<<>>" + jsonObject.latitude);


                this.latLng = new google.maps.LatLng(jsonObject.latitude, jsonObject.longitude);

                this.coords.latitude = jsonObject.latitude;
                this.coords.longitude = jsonObject.longitude;
                this.showmap(this.latLng);

                break;

              case "satellite":
                //TODO
                break;

              case "cell_info":
                //TODO
                break;

              case "cell_location":
                //TODO
                break;

              case "signal_strength":
                //TODO
                break;
            }
          }
          catch (exc) {
            console.log("Invalid JSON: " + exc);
          }
        },
          function (error) {
            console.log("ERROR! " + JSON.stringify(error));
          },
          {
            "minTime": 500,         // Min time interval between updates (ms)
            "minDistance": 1,       // Min distance between updates (meters)
            "noWarn": true,         // Native location provider warnings
            "providers": "all",     // Return GPS, NETWORK and CELL locations
            "useCache": true,       // Return GPS and NETWORK cached locations
            "satelliteData": false, // Return of GPS satellite info
            "buffer": false,        // Buffer location data
            "bufferSize": 0,         // Max elements in buffer
            "signalStrength": false // Return cell signal strength data
          });

      });
    } else {

      let options = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 30000
      };
      this.geolocation.getCurrentPosition(options).then((position) => {
        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.coords.latitude = position.coords.latitude;
        this.coords.longitude = position.coords.longitude;
        this.showmap(this.latLng);
      }, (err) => {
        console.log(err);
      });
    }

  }

  showmap(latLng) {
    let mapOptions = {
      center: latLng,
      zoom: 17,
      compass: true,
      myLocationButton: true,
      indoorPicker: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      setMyLocationEnabled: true,
      timeout: 5000,
      enableHighAccuracy: true,

    }
    console.log("latLng" + this.coords.latitude + "latLng" + this.coords.longitude);
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //let locationOptions = { timeout: 30000, enableHighAccuracy: true }; 
    var marker = new google.maps.Marker({ position: this.latLng, map: this.map, title: 'Hello!' });
  }

  addEditid() {

    // this.start();

    this.images = [];
    this.items_data = [];
    this.items_img = [];
    this.items_cus = [];

    console.log('data catid', this.catid);



    const myData = {
      idsuvey: this.catid
    };
    // const link = BASEURL + 'survey_detail_locationcustomer.php';
    // console.log('link ', link);


    

    // this.http.post(link, JSON.stringify(myData))
    this.HttpService.post("survey/surveydetaillocationcustomer",myData).subscribe(data => {
      console.log("response data",data);
        if (data != null) {


          // console.log('approve ',  data, data['items_data']);

          this.items_data = data['items_data'];
          this.items_img = data['items_img'];
          this.items_cus = data['items_cus'];
          let subtype = data['items_subtype'];


          subtype.forEach(element => {
            let type = element.customer_subtype_id;
            this.typeid.push(type);
          });

          this.datacount.subtype = this.typeid;

          console.log('items_subtype', this.datacount.subtype);



          console.log('data', data);
          //  console.log('items', items);

          console.log('items_data', this.items_data);

          if (this.items_data.length > 0) {


            this.idedit = this.items_data[0]['catagory_id'];
            this.datacount.customertype = this.items_data[0]['type_sub'];
            this.docno = this.items_data[0]['survey_doc_no'];
            //this.employeecode = data[0]['survey_type_customer'];
            this.datacount.facilityname = this.items_data[0]['facilityname'];
            this.datacount.type_input = this.items_data[0]['type_input'];
            let approve = this.items_data[0]['survey_approve'];

            if (approve == 't') {
              this.approve = true;
              console.log('approve 2', this.approve);
            } else {
              this.approve = false;
            }


            //this.type_input = this.datacount.type_input;
            this.datacount.location = this.items_data[0]['location'];
            this.datacount.job_description = this.items_data[0]['job_description'];
            this.datacount.site_value = this.items_data[0]['site_value'];
            this.datacount.survey_details = this.items_data[0]['survey_details'];
            this.coords.latitude = this.items_data[0]['latitude'];
            this.coords.longitude = this.items_data[0]['longitude'];

            this.datacount.type_road = this.items_data[0]['type_road'];
            this.datacount.date_market = this.items_data[0]['date_market'];
            this.datacount.linetype = this.items_data[0]['linetype'];
            this.datacount.peak_time1 = this.items_data[0]['peak_time1'];
            this.datacount.peak_time2 = this.items_data[0]['peak_time2'];
            this.datacount.people_walking = this.items_data[0]['people_walking'];
            this.datacount.label_sizewide = this.items_data[0]['label_sizewide'];
            this.datacount.label_sizelong = this.items_data[0]['label_sizelong'];
            this.datacount.count = this.items_data[0]['count'];
            this.datacount.contacts = this.items_data[0]['contacts'];
            this.datacount.telephone = this.items_data[0]['telephone'];
            this.datacount.rental = this.items_data[0]['rental'];
            this.datacount.runway = this.items_data[0]['runway'];
            this.datacount.number_car = this.items_data[0]['number_car'];
            this.datacount.away_branch = this.items_data[0]['away_branch'];
            this.datacount.building_shop = this.items_data[0]['building_shop'];
            this.datacount.product_category = this.items_data[0]['product_category'];
            this.datacount.competitive = this.items_data[0]['competitive'];
            this.datacount.competitor = this.items_data[0]['competitor'];
            this.datacount.distribution = this.items_data[0]['distribution'];
            this.datacount.open_credit = this.items_data[0]['open_credit'];
            this.datacount.limit_credit = this.items_data[0]['limit_credit'];
            this.datacount.credit_status = this.items_data[0]['credit_status'];
            this.datacount.budget = this.items_data[0]['budget'];
            this.datacount.license_banner = this.items_data[0]['license_banner'];
            this.datacount.count_tables = this.items_data[0]['count_tables'];
            this.datacount.count_toothpick = this.items_data[0]['count_toothpick'];
            this.datacount.interested_product = this.items_data[0]['interested_product'];
            this.datacount.whatsupport = this.items_data[0]['whatsupport'];
            this.datacount.details_survey_construction = this.items_data[0]['details_survey_construction'];
            this.datacount.startDate = this.items_data[0]['start_budget'];
            this.datacount.endDate = this.items_data[0]['end_budget'];
            this.datacount.presentation_conditions = this.items_data[0]['presentation_conditions'];

            this.datacount.cusid = this.items_data[0]['customer_barcode'];
            this.datacount.identification_card = this.items_data[0]['identification_card'];
            this.fullname = this.items_data[0]['fullname'];
            this.datacount.mobile = this.items_data[0]['mobile'];

            this.datacount.problem_customer = this.items_data[0]['problem_customer'];

            console.log('cusid data' + this.datacount.cusid);
            if (this.datacount.cusid != '') {
              this.togglecus = true;
            }

          }

          //ดึง Map
          this.loadMapEdit();
          // this.showImage();
          console.log("data edit!" + data);
        } else {

        }
      }, error => {
        console.log("error!");

      });
    setTimeout(() => {

    }, 4000);

  }

  loadMapEdit() {

    // this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    //  this.coords.latitude = position.coords.latitude;
    // this.coords.longitude = position.coords.longitude;

    this.latLng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);

    let mapOptions = {
      center: this.latLng,
      zoom: 17,
      compass: true,
      myLocationButton: true,
      indoorPicker: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      setMyLocationEnabled: true,
      timeout: 4000,
      enableHighAccuracy: true,

    }
    console.log("latLng2" + this.coords.latitude + "latLng2" + this.coords.longitude);

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //let locationOptions = { timeout: 30000, enableHighAccuracy: true }; 
    var marker = new google.maps.Marker({ position: this.latLng, map: this.map, title: 'Hello!' });



  }
  async showConfirm() {


    if (this.datacount.customertype == 1 || this.datacount.customertype == 2 || this.datacount.customertype == 3 || this.datacount.customertype == 12 ||
      this.datacount.customertype == 13 || this.datacount.customertype == 14 || this.datacount.customertype == 15 || this.datacount.customertype == 17 ||
      this.datacount.customertype == 18) {

      if (this.itemscus.length > 0 || this.datacount.cusid != '' || this.idedit != null) {
        console.log('cusid clicked', this.datacount.cusid);
        let confirm = await this.alertCtrl.create({
          header: 'ท่านต้องการบันทึก?',
          message: 'ท่านต้องการบันทึกข้อมูลหรือไม่?',
          buttons: [
            {
              text: 'ยกเลิก',
              handler: () => {
                console.log('Disagree clicked');
              }
            },
            {
              text: 'ยืนยัน',
              handler: () => {
                console.log('Agree clicked');
                this.saveproduct()
              }
            }
          ]
        });
        confirm.present();

      } else {
        let alert = await this.alertCtrl.create({
          header: 'ท่านต้องเพิ่มข้อมูลลูกค้าก่อน',
          message: 'ท่านต้องเพิ่มข้อมูลลูกค้า, เพื่อติดตามการขายต่อไป!',
          buttons: ['ตกลง']
        });
        alert.present();
        setTimeout(() => {
          alert.dismiss();
        }, 3000);

      }

    } else {

      let confirm = await this.alertCtrl.create({
        header: 'ท่านต้องการบันทึก?',
        message: 'ท่านต้องการบันทึกข้อมูลหรือไม่?',
        buttons: [
          {
            text: 'ยกเลิก',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              console.log('Agree clicked');
              this.saveproduct()
            }
          }
        ]
      });
      confirm.present();

    }

  }

  async saveproduct() {


    let link = BASEURL + 'insert_data_locationcustomer.php';
    let myData = {
      getCustomertype: this.datacount.customertype,
      getCustomersubtype: this.datacount.subtype,
      getIdedit: this.idedit,
      getDocno: this.docno,
      getNewbranch: this.newbranch,
      getEmployeecode: this.employeecode,
      getFacilityname: this.datacount.facilityname,
      getType_input: this.datacount.type_input,
      getLocation: this.datacount.location,
      getJob_description: this.datacount.job_description,
      getSite_value: this.datacount.site_value,
      getSurvey_details: this.datacount.survey_details,
      getLatitude: this.coords.latitude,
      getLongitude: this.coords.longitude,
      getType_road: this.datacount.type_road,
      getDate_market: JSON.stringify(this.datacount.date_market),
      getLinetype: this.datacount.linetype,
      getPeak_time1: this.datacount.peak_time1,
      getPeak_time2: this.datacount.peak_time2,
      getPeople_walking: this.datacount.people_walking,
      getLabel_sizewide: this.datacount.label_sizewide,
      getLabel_sizelong: this.datacount.label_sizelong,
      getCount: this.datacount.count,
      getContacts: this.datacount.contacts,
      getTelephone: this.datacount.telephone,
      getRental: this.datacount.rental,
      getRunway: this.datacount.runway,
      getNumber_car: this.datacount.number_car,
      getAway_branch: this.datacount.away_branch,
      getBuilding_shop: this.datacount.building_shop,
      getProduct_category: JSON.stringify(this.datacount.product_category),
      getCompetitive: this.datacount.competitive,
      getCompetitor: this.datacount.competitor,
      getDistribution: JSON.stringify(this.datacount.distribution),
      getOpen_credit: this.datacount.open_credit,
      getLimit_credit: this.datacount.limit_credit,
      getCredit_status: this.datacount.credit_status,
      getBudget: this.datacount.budget,
      getLicense_banner: this.datacount.license_banner,
      getCount_tables: this.datacount.count_tables,
      getCount_toothpick: this.datacount.count_toothpick,
      getInterested_product: this.datacount.interested_product,
      getWhatsupport: this.datacount.whatsupport,
      getDetails_survey_construction: this.datacount.details_survey_construction,
      getStartDate: this.datacount.startDate,
      getEndDate: this.datacount.endDate,
      getPresentation_conditions: this.datacount.presentation_conditions,
      getItemscus: this.itemscus,
      sentPhotos: this.photos,
      getCuscode: this.datacount.cusid,
      sentIdcard: this.datacount.identification_card,
      getFullname: this.fullname,
      sentMobile: this.datacount.mobile,
      problem_customer: this.datacount.problem_customer

    };

    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {

        //ทำการดึง id


        this.photos = [];
        //this.LoadData();

        if (data['status'] === 400) {

          this.noSucces();

        } else if (data['status'] === 200) {

          //this.events.publish('reloadLocation');
          //this.navCtrl.popTo(AddlistlocationPage);
          this.nav.back();

        } else if (data['status'] === 201) {

          this.editAlert();

        } else {

          this.noSucces();

        }

      }, Error => {

        this.showalert(Error);

      });
    setTimeout(() => {

    }, 30000);

  }

  async noSucces() {
    let alert = await this.alertCtrl.create({
      header: 'ไม่สำเร็จ',
      message: 'บันทึกข้อมูลไม่สำเร็จ, ลองตรวจสอบข้อมูลแล้วบันทึกอีกครั้ง!',
      buttons: ['ตกลง']
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }

  async editAlert() {
    let alert = await this.alertCtrl.create({
      header: 'แก้ไขสำเร็จ',
      message: 'บันทึกข้อมูลแก้ไขสำเร็จ',
      buttons: ['ตกลง']
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }

  public getItems(ev: any) {
    // set val to the value of the searchbar
    this.val = ev.target.value;
  }
  listCustumer() {

  }

  async showalert(text) {
    let alert = await this.alertCtrl.create({
      header: 'การบันทึก',
      message: text,
      buttons: ['ตกลง']
    });
    alert.present();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }

}
