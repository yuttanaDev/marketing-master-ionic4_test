import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { HttpService } from './../services/http.service';
@Component({
  selector: 'app-endbill',
  templateUrl: './endbill.page.html',
  styleUrls: ['./endbill.page.scss'],
})
export class EndbillPage implements OnInit {
  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';
  public loadingimg: any;
  datacount: any= {};
  pictureimg: any;
  docno: any;
  employeecode: any;
  newbranch: any;

  showbill: any;
  sumpoquantity: any;

  survey_imgmile: any = null;

  linkimg: any = API_URL+'Uploaded/';

  public photos: any  = [];
  public base64Image: string;
  branchname: any;
  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    private camera: Camera,
    public alertCtrl: AlertController,
    public HttpService : HttpService,
    ) { 

      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        console.log(employeecode);
  
      });

      //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.doc_no;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        //โชว์รายชื่อคนออกสำรวจ
        this.showemployyee();
        this.imgmile();
      }
    });

      

    }
    doRefresh(){
      
    }

   async showemployyee() {

      let loader = await this.loadingController.create({
        message: "กรุณารอสักครู่..."
      });
  
      loader.present();

      let  myData = {docno:this.docno};
      this.HttpService.post("survey/checkneedle", myData).subscribe(
        (res : any) => {
          // console.log("response data",res);
          if(res != null){
            loader.dismiss();
            this.showbill = res;
            this.datacount.poquantitystart = res[0]['survey_incout'];
            this.datacount.poquantity = res[0]['survey_outcout'];
            console.log("Show EmpName" + this.showbill);
  
          }else{
            loader.dismiss();
          }
        },error => {
          loader.dismiss();
          console.log("showemployyee node error");
        }
      );
      // this.http.get(API_URL+'check_needle.php?docno=' + this.docno)
      //   .subscribe(data => {
      //     console.log("data!" + data);
      //     if (data != null) {
      //       loader.dismiss();
      //       this.showbill = data;
      //       this.datacount.poquantitystart = data[0]['survey_incout'];
      //       this.datacount.poquantity = data[0]['survey_outcout'];
      //       console.log("Show EmpName" + this.showbill);
  
      //     } else {
      //       loader.dismiss();
      //     }
      //   }, error => {
      //    // console.log("error!");
      //     loader.dismiss();
      //   });
  
    }
  

  
    async presentActionSheet() {
      let actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image Source',
        buttons: [
          {
            text: 'Load from Library',
            icon: 'md-images',
            handler: () => {
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
          },
          {
            text: 'Use Camera',
            icon: 'md-camera',
            handler: () => {
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      actionSheet.present();
    }
  
    takePicture(sourceType) {
      var option = {
        quality: 75,
        targetWidth: 960,
        targetHeight: 690,
        correctOrientation: true,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
  
      this.camera.getPicture(option).then((imageData) => {
  
        if (sourceType == 0) {

          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();
  
          const myData = {
            docno: this.docno,
            employee: this.employeecode, 
            branch: this.newbranch ,
            photos:this.photos
          };
          this.http.post(API_URL+'upload_outbase.php', JSON.stringify(myData))
            .subscribe(data => {
               console.log('detail', data);
              
              if (data != null) {
               
              this.showalert("อัพโหลดประสบความสำเร็จ");
              this.pictureimg = data;
              //this.imgcar();
              this.imgmile()
              this.photos=[];
      
              } else {
      
              }
            }, error => {
             
              this.showalert("ไม่สำเร็จ");
            });

 
            
  
        } else {
  
          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();
  
          const myData = {
            docno: this.docno,
            employee: this.employeecode, 
            branch: this.newbranch ,
            photos:this.photos
          };
          this.http.post(API_URL+'upload_outbase.php', JSON.stringify(myData))
            .subscribe(data => {
               console.log('detail', data);
              
              if (data != null) {
               
              this.showalert("อัพโหลดประสบความสำเร็จ");
              this.pictureimg = data;
              //this.imgcar();
              this.imgmile()
              this.photos=[];
      
              } else {
      
              }
            }, error => {
             
              this.showalert("ไม่สำเร็จ");
            });
  
        }
  
      }, (err) => {
        this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
        //alert(JSON.stringify(err));
      });
    }
  
    async deletePhoto(index) {
      let confirm = await this.alertCtrl.create({
        header: "Sure you want to delete this photo? There is NO undo!",
        message: "",
        buttons: [
          {
            text: "No",
            handler: () => {
              console.log("Disagree clicked");
            }
          },
          {
            text: "Yes",
            handler: () => {
              console.log("Agree clicked");
              this.photos.splice(index, 1);
            }
          }
        ]
      });
      confirm.present();
    }
  
   
  
    imgmile() {
      //เช็คปิดป้อนเข็มไมล์
      let myData = {docno : this.docno};
      this.HttpService.post("survey/checkimgmilend",myData).subscribe(
        (res:any) =>{
          if(res != null){
            this.survey_imgmile = res[0]['survey_img'];
          }else{

          }
        }, error => {
          console.log("error!");
        }
      );
      // this.http.get(API_URL+'check_img_mile2.php?docno=' + this.docno)
      //   .subscribe(data => {
      //     // loader3.dismissAll();
      //     console.log("data!" + data);
      //     if (data != null) {
      //       //this.survey_incout = data[0]['survey_incout']; 
      //       this.survey_imgmile = data[0]['survey_img'];
      //       //this.survey_doc_no = data[0]['survey_doc_no'];
  
      //       // this.needle = data;
  
      //     } else {
  
      //     }
      //   }, error => {
      //     console.log("error!");
      //     //  loader3.dismissAll();
      //   });
  
    }
  
  
    async showConfirm() {
      let confirm = await this.alertCtrl.create({
        header: 'ท่านต้องการบันทึก?',
        message: 'ท่านต้องการบันทึกข้อมูลปิดบิลหรือไม่?',
        buttons: [
          {
            text: 'ยกเลิก',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              console.log('Agree clicked');
              this.saveproduct()
            }
          }
        ]
      });
      confirm.present();
    }

    onSelectChange(selectedValue) {
     
      let length = selectedValue.detail.value;
      
      this.sumpoquantity = this.datacount.poquantity - this.datacount.poquantitystart;

        console.log('sumpoquantity', this.sumpoquantity);
    }
  
    async saveproduct() {
  
      let poquantity = this.datacount.poquantity;
      let poquantitystart = this.datacount.poquantitystart;
  
  
      let loader = await this.loadingController.create({
        message: "กรุณารอสักครู่..."
      });

    
      loader.present();

      let myData = {docno : this.docno, poquantitystart : poquantitystart , poquantity : poquantity  };
      console.log(myData);
      this.HttpService.post("survey/addcount",myData).subscribe(
        (data : any) => {
          this.showalert(JSON.stringify(data));
          loader.dismiss();
          this.showemployyee();
        },err => {
          this.showalert(JSON.stringify(err));
          loader.dismiss();
        });


      // this.http.get(API_URL+'addcount_out.php?poquantity='
      //   + poquantity + '&' + 'docno=' + this.docno + '&' + 'employee=' + this.employeecode + '&' + 'branch=' + this.newbranch + '&' + 'poquantitystart=' + poquantitystart)
      //   .subscribe(
      //     data => {
  
      //       this.showalert(JSON.stringify(data));
      //       loader.dismiss();
      //       this.showemployyee();
  
      //     }, err => {
      //       this.showalert(JSON.stringify(err));
      //       loader.dismiss();
      //     })
    }
  
  
   async showalert(text) {
      let alert = await this.alertCtrl.create({
        header: 'การบันทึก',
        message: text,
        buttons: ['ตกลง']
      });
      alert.present();
    }
  
    presentLoadingDefault() {
      this.loadingimg = this.loadingController.create({
        message: 'Please wait...'
      });
  
      this.loadingimg.present();
  
      /* setTimeout(() => {
        this.loadingimg.dismiss();
      }, 15000); */
    }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }


}

