import { Component, OnInit } from '@angular/core';
import { Platform, NavController, AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { BASEURL, URLIMG, API_URL,token } from '../../providers/constants/constants';


@Component({
  selector: 'app-selectprovice',
  templateUrl: './selectprovice.page.html',
  styleUrls: ['./selectprovice.page.scss'],
})
export class SelectprovicePage implements OnInit {
  public items: any = [];
  public itemsAll: any = [];
  constructor(public navCtrl: NavController,
    public platform: Platform,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public http: HttpClient,
    public modalController: ModalController,
    public navParams: NavParams,
    private router: Router) {

    this.loadProvice();
   }

    // load Provice
  loadProvice() {

    this.http.get('assets/dataJson/dataProvice.json')
      .subscribe(data => {
        if (data != null) {
          this.itemsAll = data;
          this.items = this.itemsAll;
          //console.log(data);
        }
      }, error => {
        this.items = [];
        
      });
  }

  fillter(ev) {
    this.items = this.itemsAll;
    var val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.text.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  selectedItem(txt: string, id: number) {
    let item = { text: txt, value: id };
    console.log(item);
    this.modalController.dismiss({
      item,
    });
  }

  closeModal() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }


  ngOnInit() {
  }

}
