import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { LoadingController, Platform, NavController, MenuController, Events, ModalController, NavParams } from '@ionic/angular';

import { BASEURL } from '../../providers/constants/constants';



@Component({
  selector: 'app-dismiss-modal',
  templateUrl: './dismiss-modal.page.html',
  styleUrls: ['./dismiss-modal.page.scss'],
})
export class DismissModalPage {

  private posts: any; // <- I've added the private keyword 
  private searchQuery: string = '';
  public items: any; // <- items property is now of the same type as posts

  public modalTitle: string;
  public modalContent: string;

  public brncode: any;
  public brnname: any;
  linkimg: any;
  procode: any;
  product_code: any;
  product_name: any;
  inactive: any;
  stockqty: any;
  picture_name: any = null;
  product_group_name: any;
  abc_total: any;
  vendor_code: any;
  product_unit_code: any;
  product_cost: any;
  product_price: any;
  datacount: any;
  dataPrice: any;
  sellerid: any;
  idedit: any;

  datacode: any = null;

  isShown = true;
  constructor(public storage: Storage,
    public modalController: ModalController,
    private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public platform: Platform,
    public events: Events,
    private router: Router,
    public loadingController: LoadingController,
    public navParams: NavParams,
    private http: HttpClient, ) {

    this.datacount = {};

    this.selectBranch();

  }
  async selectBranch() {


    this.http.get(BASEURL + 'selectstore-new.php')
      .subscribe(data => {
       // console.log('load product', data);

        if (data != null) {
          this.posts = data;
          //console.log('data 2', data);
          this.initializeItems();

          setTimeout(() => {
            this.isShown = false;
          }, 2000);

        } else {

          setTimeout(() => {
            this.isShown = false;
          }, 2000);

        }




      }, error => {
        setTimeout(() => {
          this.isShown = false;
        }, 2000);
        console.log(JSON.stringify(error.json()));
      });

  }
  initializeItems() {
    this.items = this.posts;
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.branchname.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }



  modalhide() {
    this.datacode = null;
    this.modalController.dismiss(this.datacode);
  }

  async clickToHome(item) {

    let branchcode = item.branchcode;
    let namebranch = item.branchname;

    this.datacode = { 'status': true,'branchcode': branchcode, 'namebranch': namebranch };
    this.modalController.dismiss(this.datacode);



  }



}
