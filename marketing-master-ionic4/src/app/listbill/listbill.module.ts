import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';


import { ListbillPage } from './listbill.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListbillPage
      }
    ])
  ],
  providers: [
   
  ],
  declarations: [ListbillPage]
})
export class ListbillPageModule {}
