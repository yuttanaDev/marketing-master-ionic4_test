import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import {  IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { API_URL } from '../../providers/constants/constants';

const imgpath = API_URL + 'CompetitorSurvey/Logo/';

@Component({
  selector: 'app-listbill',
  templateUrl: 'listbill.page.html',
  styleUrls: ['listbill.page.scss']
})
export class ListbillPage  {

  public imgpath: any = imgpath;
  public posts: any= [];
  public posts2: any; // <- I've added the private keyword 
  public searchQuery: string = '';
  public items: any= [];// <- items property is now of the same type as posts
  public itemslength: any= [];
  public items2: any;
  public newbranch: any;
  checkdata: any;
  public employeecode: any;

  public namebranch: any;
  public branch: any;
  public setting: any;

  signInType: string;
  public idedit: any;

  datacount: any;
  datefirst: any;
  dateend: any;
  myDate: any;

  public databasename: any;
  public servername: any;
  public BASEURL: any = API_URL;

  private isLogedin: any = 0;
  public remember: any = '';

  pageNumber: number = 1;
  pageAllpage: number = 50;
  isShown = true;

  constructor(
    private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private http: HttpClient
  ) {



    this.datacount = {};
    this.signInType = 'Normalsale';

   


  }


  /* ngAfterViewInit() {
    this.listenEvents();
  } */
  ionViewWillEnter() {


    this.posts = [];
    this.items = [];

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        this.CompetitorSurvey(this.pageNumber);
      });
    });
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
      this.datacount.namebranch = namebranch;
    });

    
  }



  initializeItems() {
    this.items = this.posts;
    this.itemslength = this.items.length;
  }
  initializeItems2() {
    this.items2 = this.posts2;
  }
  searchdate() {
   
  }

  goToAdd(page:any){
    console.log('page ', page);
    this.router.navigate(['list']);
  }

  itemCatalog(item:any) {
    //item.rival_code,item.rival_name,item.rival_id,item.branch_code
    let navigationExtras: NavigationExtras = {
      state: {
        data: item
      }
    };
    this.router.navigate(['second'], navigationExtras);
  }

  async CompetitorSurvey(pageNum: number) {
    

      console.log('newbranch 2', this.namebranch,this.newbranch);

    let link = this.BASEURL + 'CompetitorSurvey/rival_branch.php';
    let myData = {
      branch: this.newbranch,
      sentempcode: this.employeecode,
      tokenapi: 'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
    };
    this.http.post(link, JSON.stringify(myData))
        .subscribe(data => {

          if(data != null){
            this.posts = data;
            //let code = data[0].competitor_code;
            this.initializeItems();
            console.log('data 2', data);
            setTimeout(() => {
              this.isShown = false;
            }, 1000);
          }else{
            this.posts = [];
            setTimeout(() => {
              this.isShown = false;
            }, 1000);
          }
          
        }, error => {
          console.log(JSON.stringify(error.json()));
          setTimeout(() => {
            this.isShown = false;
          }, 1000);
        });
      setTimeout(() => {
        this.isShown = false;
      }, 15000);

  }
  async listNormal2(branchcode,namebranch,pageNum: number) {
    

    let loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 2000
    });
    await loading.present();
    
      console.log('newbranch 2', this.namebranch,branchcode);
      let link = this.BASEURL + 'Cyclemobile/docstore-new.php';
      let myData = {
        branchcode: branchcode,
        empcode: this.employeecode,
        pageNum: pageNum,
  
      };
      this.http.post(link, JSON.stringify(myData))
        .subscribe(data => {
          if(data != null){
            this.posts = data;
            if(this.posts == null){
              this.posts = [];
            }
            console.log('data 2', data);
            this.initializeItems();
            loading.dismiss();
          }else{
            this.posts = [];
          }
        }, error => {
          console.log(JSON.stringify(error.json()));
          loading.dismiss();
        });
      setTimeout(() => {
        loading.dismiss();
      }, 10000);

  }

  loadInfinite(infiniteScroll) {

    //this.pageNumber = this.pageNumber+1;

    setTimeout(() => {

      console.log('pageNumber', this.items.length, this.pageNumber, this.pageAllpage);

      if (this.items.length < this.pageAllpage) {
        this.pageNumber++;
        this.CompetitorSurvey(this.pageNumber);
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();

    }, 1500);

    
  }

  doRefresh() {
    this.pageNumber = 1 ;
    this.items = [];
    this.CompetitorSurvey(this.pageNumber);

   /*  console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000); */
  }


  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.rival_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  getItems2(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items2 = this.items2.filter((item2) => {
        return (item2.docuno.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  async Removebill(sellerid: any, slidingItem: IonItemSliding, id: any) {

    slidingItem.close();

    const alert = await this.alertController.create({
      header: 'ลบข้อมูล?',
      message: 'ท่านยืนยันการการลบสินค้านี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ตกลง',
          handler: () => {
            console.log('Confirm Okay id',id);

            // ทำการแสดงข้อมูล 

            this.http.get(this.BASEURL + 'OrderNow/Delete_listbill.php?sellerid=' + sellerid)
              .subscribe(data => {

                

              }, error => {

                //console.log(JSON.stringify(error.json()));
              });

          }
        }
      ]
    });

    await alert.present();


  }

 
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }


}
