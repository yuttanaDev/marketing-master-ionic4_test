import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdercategoryPage } from './ordercategory.page';

describe('OrdercategoryPage', () => {
  let component: OrdercategoryPage;
  let fixture: ComponentFixture<OrdercategoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdercategoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdercategoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
