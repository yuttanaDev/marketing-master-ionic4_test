import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras,ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { CartPage } from '../cart/cart.page';

import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';

@Component({
  selector: 'app-ordercategory',
  templateUrl: './ordercategory.page.html',
  styleUrls: ['./ordercategory.page.scss'],
})
export class OrdercategoryPage implements OnInit {

  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';

  private posts: any; // <- I've added the private keyword 
  public items: any; // <- items property is now of the same type as posts
  private categories: any;
  public newbranch: any;
  checkdata: any;
  public employeecode: any;

  public namebranch: any;
  public branch: any;
  public remember: any = '';
  public barcode: any;
  public idtype: any;
  public gencode: any = '';
  public approve: boolean;
  private idedit: any;
  private docustatus: any;
  public orderid: any;
  datacount: any;
  private setting: any;
  public itemcount: any = 0;

  private isLogedin: any = 0;
  private genbillcode: any;
  private linkurl: any;

  private docuno: any;
  private branchcode: any;

  private approvedata: any;
  private totalPrice: any;

  public strbranch: any;


  public product: any;

  public list: any;
  public input: string = '';
  public countries: string[] = [];
  public datacart: any = [];

  public selectedCategory: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;

  public sellerid: any = null;
  public docnoid: any = null;
  public docno: any = null;

  public items_cat: any;

  public category_code: any;
  public product_category_id: any ;
  public product_category_name: any;

  public items_group: any;
  public items_pattern: any;
  public category_id: any;

  public group: any = null;
  public pattern: any = null;

  public total: number;
  public cartdata: any = [];
  public cartItem: any = [];

  public keyword: any = null;
  public pageid: any;
  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    private route: ActivatedRoute, 
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
  ) { 
    
    this.approve = false;

    this.product = {
      scanData: '',
    }
    this.datacount = {};
    this.datacount.picture_name = null;

    this.loadcart();
    

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.category_code = this.router.getCurrentNavigation().extras.state.category_code;
        this.product_category_id = this.router.getCurrentNavigation().extras.state.product_category_id;
        this.product_category_name = this.router.getCurrentNavigation().extras.state.product_category_name;
        this.keyword = this.router.getCurrentNavigation().extras.state.keyword;
        this.pageid = this.router.getCurrentNavigation().extras.state.pageid;
 
        this.input =this.keyword;
        this.selectgroup(this.product_category_id);
        this.storage.get('newbranch').then((newbranch) => {
          this.newbranch = newbranch;
          this.strbranch = newbranch.replace('-', '');
          console.log('newbranch : ' ,this.newbranch,this.input,this.product_category_id,this.category_code);
    
          this.searchproduct();
    
        });
      }
    });
    

  }

  loadcart() {
    this.total = 0;
    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    // console.log("data1", this.cartItem);
    this.cartdata = this.cartItem;
    console.log("01count:" + this.cartdata,this.cartItem);
    if (this.cartdata != null) {
      this.cartdata.forEach(item => {  //ตัวอย่าง foreach
        this.total += item.subtotal;  //ยอดรวม
      });
      this.itemcount = parseInt(this.cartdata.length);
    }

  }

  search(){
    this.searchproductgroup();
  }

  async searchproductgroup(){

    this.items = null;

 

    console.log('Data input' ,this.input,this.group,this.pattern);

    let url ;
    let myData;
    if(this.keyword != null){
      url = API_URL + "OrderNow/search_product_all_noprice.php";
      myData = {
       employeecode: this.employeecode,
       branchcode: this.newbranch,
       keyword: this.input,
     };
     console.log('Data input1' + this.input);
   }else{
      url = API_URL + "OrderNow/search_product_catall_noprice.php";
      myData = {
       employeecode: this.employeecode,
       branchcode: this.newbranch,
       category_code: this.category_code,
       product_category_id:this.product_category_id,
       group:this.group,
       pattern:this.pattern,
       keyword: this.input,
     };
     console.log('Data input2' + this.input);
   }
    this.http.post(url, JSON.stringify(myData))
      .subscribe(data => {

        if (data != null) {

          console.log('Data Results' + this.items);
          setTimeout(() => {
            this.items = data;
          }, 1000);

        } else {

          this.items = [];
        }

      }, error => {
 
          setTimeout(() => {
            this.items = [];
          }, 1000);
      });

  }


   // SEARCH FUNCTION
   searchproduct() {
    let url;
    let myData;
    if(this.keyword != null){
       url = API_URL + "OrderNow/search_product_all_noprice.php";
       myData = {
        employeecode: this.employeecode,
        branchcode: this.newbranch,
        keyword: this.input,
      };
      console.log('Data input1' + this.input);
    }else{
       url = API_URL + "OrderNow/search_product_catall_noprice.php";
       myData = {
        employeecode: this.employeecode,
       branchcode: this.newbranch,
       category_code: this.category_code,
       product_category_id:this.product_category_id,
       group:this.group,
       pattern:this.pattern,
       keyword: this.input,
      };
      console.log('Data myData' , myData);
    }

    this.http.post(url, JSON.stringify(myData))
      .subscribe(data => {

        if (data != null) {

          
          console.log('Data Results',data);
          setTimeout(() => {
            this.items = data;
          }, 1000);
        
        } else {
          this.items = [];
          

        }

      }, error => {
        // loader.dismiss();
        setTimeout(() => {
          this.items = [];
        }, 2000);
        //console.log(JSON.stringify(error.json()));
      });




  }

  selectgroup(product_category_id){

    this.category_id = parseInt(this.product_category_id);
    console.log("product_category_id", this.category_id);
    this.items_group = JSON.parse(localStorage.getItem('items_group'));
    this.items_group = this.items_group.filter(function (item) {
      return item.product_category_id  == product_category_id;
    });
    console.log("items_group", this.items_group);

  }
  selectpattern(){
    
    let group = this.group;

    console.log("Toggled: "+ this.group); 

    this.items_pattern = JSON.parse(localStorage.getItem('items_pattern'));
    this.items_pattern = this.items_pattern.filter(function (item) {
      return item.product_group_id == group;
    });
    console.log("items_pattern", this.items_pattern);
    
  }

  itemcountproduct() {
    this.http.get(API_URL + 'OrderNow/itemcount_product.php?sellerid=' + this.docnoid)
      .subscribe(data => {
        if (data != null) {
          console.log('itemcount_product : ', data);
          this.itemcount = parseInt(data[0]['countitem']);
        } else {
          this.itemcount = 0;
        }
      }, error => {

        console.log(JSON.stringify(error.json()));
      });
  }

  ngOnInit() {
  }

  gotoCategoryProduct(product_category_id:any,category_code:any,product_category_name:any){

    /* this.navCtrl.push(OrdercategoryPage, {
      product_category_id: product_category_id,
      category_code: category_code,
      product_category_name:product_category_name
    }); */

    let navigationExtras: NavigationExtras = {
      state: {
        product_category_id: product_category_id,
      category_code: category_code,
      product_category_name:product_category_name
      }
    };
    this.router.navigate(['ordercategory'], navigationExtras);



  }

  gotoProductDetail(item) {

    console.log('product_barcode', item.barcode_code);
    /* this.navCtrl.push(ProductPage, {
      //items:item,
      product_barcode: item.barcode_code,
      product_name: item.barcode_bill_name,
      picturename: item.picturename,
      product_price1: item.product_price1,
      pro_price: item.pro_price,
      stockbalance: item.stockbalance,
      vendor_code: item.vendor_code,
      unit_code: item.unit_code
    }); */

    let navigationExtras: NavigationExtras = {
      state: {
        product_item:item,
      }
    };
    this.router.navigate(['product'], navigationExtras);

  }

  async addcartnow() {

    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];
      console.log("docnoid", docnoItem[0]['docno']);
      /* let navigationExtras: NavigationExtras = {
        state: {
          sellerid: this.docnoid,
          docuno: this.docno
        }
      };
      this.router.navigate(['cart'], navigationExtras); */

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: this.docnoid,
          docuno: this.docno,
          idtype: this.idtype,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage',data);
        if (data == null) {
  
          
        }else{
  
        }
  
      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });
      

    } else {

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: null,
          docuno: null,
          idtype: null,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage',data);
        if (data == null) {
  
          
        }else{
  
        }
  
      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });

    }



  }

}
