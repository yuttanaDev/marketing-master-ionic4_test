import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { HttpService } from './../services/http.service';
import { BASEURL, URLIMG,API_URL } from '../../providers/constants/constants';
@Component({
  selector: 'app-listreport',
  templateUrl: './listreport.page.html',
  styleUrls: ['./listreport.page.scss'],
})
export class ListreportPage  {


  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';

  public items: any;
  public linkurl: any = URLIMG;
  public posts: any;
  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public isShown = true;

  public datacount: any = {};
  public posts2: any;
  public searchQuery: string = '';
  public items2: any;

  checkdata: any;
  public branch: any;
  public typeadmin: any = 0;

  public isLogedin: any = 0;
  public remember: any = '';
  type_admin: any = 0;

  myDate: any;
  
  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
    public HttpService : HttpService,
  ) {

    let dateString = new Date();
    let date = new Date(dateString.getFullYear(), dateString.getMonth(), 1);

    let dateString2 = new Date();
    let date2 = new Date(dateString2.getFullYear(), dateString2.getMonth(), dateString2.getDate());

    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        this.storage.get('type_admin').then((type_admin) => {
          this.type_admin = type_admin;
          // console.log("type_admin" ,this.type_admin);
          if (this.type_admin == 0 || this.type_admin == null) {
            this.teamdoc();
            //  console.log("team" );
          }else if (this.type_admin == 1 ){
            this.teamadmin();
            //   console.log("teamadmin" );
          }else if (this.type_admin == 2 ){
            this.teamadmin();
            //   console.log("teamadmin" );
          }else {
            this.teamadmin();
            //   console.log("teamadmin" );
          }
        });
      });
    });
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
      this.datacount.namebranch =this.namebranch;
      console.log("test xx" );
    });




  }

  presentConfirm() {
    let date = new Date(this.datacount.datefirst);
    let date2 = new Date(this.datacount.dateend);
    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    if (this.type_admin == 0 || this.type_admin == null) {
      this.items =null;
      this.teamdoc();
      console.log("team");
    } else {
      this.items2 =null;
      this.teamadmin();
      console.log("teamadmin");
    }
  }

  teamdoc() {

    this.isShown = true;

    /** Call nodejs API **/
    let myData = {
      employeecode : this.employeecode,
      datefirst :  this.datacount.datefirst,
      dateend : this.datacount.dateend
    };

    // console.log(" ckkk ",myData);

    this.HttpService.post("survey/suveydocteamreport",myData).subscribe(
      (res : any) => {
        this.posts = res;
        this.initializeItems();
        console.log('posts nodeJs', this.posts);
        this.isShown = false;
      },error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      }
    );

    // this.http.get(API_URL + 'suvey_doc_teamreport.php?employeecode=' + this.employeecode + '&' + 'datefirst=' + this.datacount.datefirst + '&' + 'dateend=' + this.datacount.dateend)
    //   .subscribe(data => {
    //     this.posts = data;
    //     this.initializeItems();
    //     console.log('postsxxx', this.posts);
    //     this.isShown = false;
    //   }, error => {
    //     this.isShown = false;
    //     console.log(JSON.stringify(error.json()));
    //   });

    setTimeout(() => {
      this.isShown = false;
    }, 7000);

  }
  teamadmin() {
    this.isShown = true;
    let myData = {employeecode : this.employeecode ,branchcode :  this.newbranch , datefirst: this.datacount.datefirst ,dateend :  this.datacount.dateend };
    this.HttpService.post("survey/suveydocteamadminreport",myData).subscribe(
      (res : any) => {
        this.posts2 = res;
        this.initializeItems2();
        console.log('posts2', this.posts2);
        this.isShown = false;
      },error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      }
    );
    // console.log('employeecode', this.employeecode,this.newbranch,this.datacount.datefirst ,this.datacount.dateend );
    // this.http.get(API_URL + 'suvey_doc_teamadminreport.php?employeecode=' + this.employeecode + '&' + 'branchcode=' + this.newbranch + '&' + 'datefirst=' + this.datacount.datefirst + '&' + 'dateend=' + this.datacount.dateend)
    // .subscribe(data => {
    //       this.posts2 = data;
    //       this.initializeItems2();
    //       console.log('posts2', this.posts2);
    //       this.isShown = false;
    //     }, error => {
    //       this.isShown = false;
    //       console.log(JSON.stringify(error.json()));
    //     });

    setTimeout(() => {
      this.isShown = false;
    }, 7000);
  }

  async openModal() {
    let modal = await this.modalController.create({
      component: DismissModalPage,
      componentProps: {
        //barCode: couponCode,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        

        this.datacount.namebranch = data.namebranch;
        this.newbranch = data.branchcode;
        this.storage.set('namebranch', data.namebranch);
        this.storage.set('newbranch', data.branchcode);

        if (this.type_admin == 0 || this.type_admin == null) {
          this.items =null;
          this.teamdoc();
          console.log("team");
        } else {
          this.items2 =null;
          this.teamadmin();
          console.log("teamadmin");
        }

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }

  itemTapped(survey_doc_no: any, branchcode: any, branchname: any) {

    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: survey_doc_no,
        branchcode:branchcode,
        branchname:branchname
      }
    };
    this.router.navigate(['home'], navigationExtras);

  }

  openNotify() {

  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.isShown = true;
    setTimeout(() => {
      console.log('Async operation has ended');
      if (this.type_admin == 0 || this.type_admin == null) {
        this.items =null;
        this.teamdoc();
        console.log("team");
      } else {
        this.items2 =null;
        this.teamadmin();
        console.log("teamadmin");
      }
      event.target.complete();
    }, 1500);
  }
  doRefresh2() {
    this.isShown = true;
    setTimeout(() => {
      if (this.type_admin == 0 || this.type_admin == null) {
        this.items =null;
        this.teamdoc();
        console.log("team");
      } else {
        this.items2 =null;
        this.teamadmin();
        console.log("teamadmin");
      }
    }, 1500);
  }


 

  initializeItems() {
    this.items = this.posts;
  }
  initializeItems2() {
    this.items2 = this.posts2;
    console.log("data3!" + this.items2);
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.docuno.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

 async editDocnoItem(id_suvey: any, slidingItem: IonItemSliding) {
    slidingItem.close();

    let confirm = await this.alertCtrl.create({
      header: 'ต้องการแก้ไขข้อมูลเอกสารนี้?',
      message: 'ท่านต้องการแก้ไขข้อมูลเอกสารนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยันแก้ไข',
          handler: () => {
            console.log('id_suvey clicked', id_suvey);

            let link = API_URL + 'edit_docno_emp.php';
            let myData = {
              getid_suvey: id_suvey,

            };

            this.http.post(link, JSON.stringify(myData))
              .subscribe(data => {
                if (data != null) {
                  this.showdialog();
                  if (this.type_admin == 0 || this.type_admin == null) {
                    this.teamdoc();
                  } else {
                    this.teamadmin();
                  }
                } else {
                }
              }, error => {
                console.log(JSON.stringify(error.json()));
              });

          }
        }
      ]
    });
    confirm.present();

  }

 async showdialog(){

  let confirm = await this.alertCtrl.create({
    header: 'คำร้องขอส่งแก้ไขเอกสารไปบัญชีใหม่?',
    message: 'ท่านร้องขอส่งแก้ไขเอกสารไปบัญชีใหม่ สำเร็จแล้ว รอ ผอ.อนุมัติ เอกสาร',
    buttons: [{
      text: 'ตกลง'
    }]
  });
  confirm.present();

 }


}
