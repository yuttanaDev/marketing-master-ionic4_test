
import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ModalController, NavParams, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { SelectprovicePage } from '../selectprovice/selectprovice.page';

@Component({
  selector: 'app-dismiss-modalnormal',
  templateUrl: './dismiss-modalnormal.page.html',
  styleUrls: ['./dismiss-modalnormal.page.scss'],
})
export class DismissModalnormalPage implements OnInit {
  defaultImage = 'assets/registration.jpg';
  imageToShowOnError = 'assets/registration.jpg';

  imageShowOnError = 'assets/No_Image.jpg';
  public posts: any; // <- I've added the private keyword 
  public searchQuery: string = '';
  public items: any; // <- items property is now of the same type as posts

  public modalTitle: string;
  public modalContent: string;

  public brncode: any;
  public brnname: any;
  procode: any;
  product_code: any;
  product_name: any;
  inactive: any;
  stockqty: any;
  picture_name: any = null;
  product_group_name: any;
  abc_total: any;
  vendor_code: any;
  product_unit_code: any;
  product_cost: any;
  product_price: any;
  datacount: any;
  dataPrice: any;
  sellerid: any;
  idedit: any;
  public loadingimg: any;
  public BASEURL: any = API_URL;
  public linkimg: any = API_URL + 'ImgCar/';
  public togglecusnew: any = false;
  public base64Image: string;
  public photos: any = [];
  public docno: any;
  public newbranch: any;
  public branchname: any;
  public employeecode: any;
  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    private camera: Camera,
    public alertCtrl: AlertController,
    public navParams: NavParams) {

    this.datacount = {};

    this.docno = navParams.get("doc_no");
    this.newbranch = navParams.get("branchcode");
    this.branchname = navParams.get("branchname");

    this.loadcar();
    console.log('idedit ', this.idedit);

    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });
  }

  fntogglecusnew() {
    if (this.togglecusnew == true) {
      this.togglecusnew = false;
      console.log('false');
    } else {
      this.togglecusnew = true;
      console.log('true');
    }
  }

  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'md-images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          icon: 'md-camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
  takePicture(sourceType) {
    var option = {
      quality: 75,
      targetWidth: 960,
      targetHeight: 690,
      correctOrientation: true,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(option).then((imageData) => {

      if (sourceType == 0) {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      } else {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      }

    }, (err) => {
      this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
      //alert(JSON.stringify(err));
    });
  }

  async selectAutoComplate() {

    let modal = await this.modalController.create({
      component: SelectprovicePage,
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation,
      componentProps: {
      }
    });
    modal.onDidDismiss()
      .then((data) => {
        let item_data = data.data;
        if (item_data.dismissed == true) {
        }else{
          if (item_data.item != null || item_data.item != undefined) {
            this.datacount.provice = item_data.item.text;
            this.datacount.provicevalue = item_data.item.value;
          }
        }

        
      });
    return await modal.present();

  }

  selectedItem(item: any) {
    console.log(item);
    this.modalController.dismiss({
      item : item
    });
  }
  closeModal() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  async loadcar() {

    let loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 2000
    });
    await loading.present();

    let myData = {
      docno: this.docno,
      newbranch: this.newbranch,
      branchname: this.branchname,
    }
    let link = API_URL + "loadregiscar.php?";
    this.http.post(link, JSON.stringify(myData))
    .subscribe(data => {

      if(data['status'] === 200){
        this.items = data['status_mess'];
        loading.dismiss();
      }else{
        loading.dismiss();
      }

        console.log('loadregiscar', data);
        loading.dismiss();
 

      }, error => {
        loading.dismiss();
        console.log(JSON.stringify(error.json()));
      });

  }

  async removeItem(id_car: any, slidingItem: IonItemSliding) {
    slidingItem.close();

    const alert = await this.alertCtrl.create({
      header: 'ลบข้อมูล?',
      message: 'ท่านยืนยันการการลบข้อมูลการนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ตกลง',
          handler: () => {
            console.log('Confirm Okay');
            //ฟังก์ชั่นลบสินค้า จำนวนสินค้า
            this.http.get(API_URL + 'Delete_regiscar.php?id_car=' + id_car)
              .subscribe(data => {
                if(data != null){
                  this.items = [];
                  this.loadcar();
                }
                
              }, error => {
              });
          }
        }
      ]
    });

    await alert.present();


  }

  async showConfirm() {
    let confirm = await this.alertCtrl.create({
      header: 'ท่านต้องการบันทึก?',
      message: 'ท่านต้องการบันทึกข้อมูลปิดบิลหรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Agree clicked');
            this.saveregisCar()
          }
        }
      ]
    });
    confirm.present();
  }

  async alertPopup(title: string, Msg: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      message: Msg,
      buttons: ['ตกลง']
    })
    await alert.present();
  }
  async saveregisCar() {


    if (this.datacount.provice == "" || this.datacount.provice  == null) {
      this.alertPopup('กรอกข้อมูลไม่ครบ', 'กรุณากรอก "จังหวัด" ก่อนทำการบันทึก');
      return;
    }

    if (this.photos.length < 0) {
      this.alertPopup('ยังไม่มีรูป', 'กรุณาถ่ายรูปทะเบียนรถ ก่อนทำการบันทึก');
      return;
    }

    let loader = await this.loadingController.create({
      message: "กรุณารอสักครู่..."
    });

    loader.present();

    let myData = {
      initials: this.datacount.initials,
      numeral: this.datacount.numeral,
      provice: this.datacount.provice,
      photos: this.photos,
      docno: this.docno,
      newbranch: this.newbranch,
      branchname: this.branchname,
      employeecode:this.employeecode
    }
    let link = API_URL + "saveregiscar.php?";
    this.http.post(link, JSON.stringify(myData))
      .subscribe(
        data => {
          if(data['status'] === 200){
            this.showalert(data['status_mess']);
            loader.dismiss();
            this.togglecusnew = false;
            this.loadcar();
          }else{
            loader.dismiss();
          }
          

        }, err => {
          this.showalert(JSON.stringify(err));
          loader.dismiss();
        })
  }


 async showalert(text) {
    let alert = await this.alertCtrl.create({
      header: 'การบันทึก',
      message: text,
      buttons: ['ตกลง']
    });
    alert.present();
  }

  async deletePhoto(index) {
    let confirm = await this.alertCtrl.create({
      header: "แน่ใจหรือไม่ว่าคุณต้องการลบภาพนี้? ไม่มีการเลิกทำ!",
      message: "",
      buttons: [
        {
          text: "เลิกทำ",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "ตกลง",
          handler: () => {
            console.log("Agree clicked");
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  presentLoadingDefault() {
    this.loadingimg = this.loadingController.create({
      message: 'Please wait...'
    });

    this.loadingimg.present();

    /* setTimeout(() => {
      this.loadingimg.dismiss();
    }, 15000); */
  }

  modalhide() {
    let datacode = null;
    this.modalController.dismiss(datacode);
  }

  async selectregisCar() {
    console.log('poquantity', this.datacount.poquantity);
    if (this.datacount.poquantity > 0) {
      let datacode = { 'status': 'Y', 'totalPrice': this.datacount.dataPrice, 'poquantity': this.datacount.poquantity }
      this.modalController.dismiss(datacode);
    } else {
      const alert = await this.alertCtrl.create({
        header: 'ยังไม่ได้กรอกจำนวนสินค้า',
        message: 'ท่านยังไม่ได้กรอกจำนวนสินค้า. ไม่มีจำนวนสินค้า',
      });
      await alert.present();
      console.log('poquantity2', this.datacount.poquantity);
    }

  }


  ngOnInit() {
  }

}
