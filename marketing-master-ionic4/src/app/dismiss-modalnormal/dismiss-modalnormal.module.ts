import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { DismissModalnormalPage } from './dismiss-modalnormal.page';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SelectprovicePage } from '../selectprovice/selectprovice.page';
const routes: Routes = [
  {
    path: '',
    component: DismissModalnormalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    LazyLoadImageModule,
    NgxIonicImageViewerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DismissModalnormalPage,SelectprovicePage],
  providers: [
    Camera,
  ],
  entryComponents: [
    SelectprovicePage,
  ],
})
export class DismissModalnormalPageModule {}
