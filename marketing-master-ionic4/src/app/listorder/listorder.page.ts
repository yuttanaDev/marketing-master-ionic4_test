import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { CartPage } from '../cart/cart.page';

import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';

@Component({
  selector: 'app-listorder',
  templateUrl: './listorder.page.html',
  styleUrls: ['./listorder.page.scss'],
})
export class ListorderPage {



  public defaultImage = 'assets/No_Image.jpg';
  public imageToShowOnError = 'assets/No_Image.jpg';

  public items: any;
  public linkurl: any = URLIMG;
  public posts: any;
  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public isShown = true;

  public datacount: any = {};
  public posts2: any;
  public searchQuery: string = '';
  public items2: any;

  public checkdata: any;
  public branch: any;
  public typeadmin: any = 0;

  public isLogedin: any = 0;
  public remember: any = '';
  public type_admin: any = 0;

  public myDate: any;
  public strbranch: any;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
  ) {

    let dateString = new Date();
    let date = new Date(dateString.getFullYear(), dateString.getMonth(), 1);

    let dateString2 = new Date();
    let date2 = new Date(dateString2.getFullYear(), dateString2.getMonth(), dateString2.getDate());

    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    this.datacount.typedoc = 'D';

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.strbranch = newbranch.replace('-', '');
      console.log('strbranch : ' + this.strbranch);
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        this.storage.get('type_admin').then((type_admin) => {
          this.type_admin = type_admin;
          // console.log("type_admin" ,this.type_admin);
          this.teamdoc();
        });
      });
    });
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
      this.datacount.namebranch = this.namebranch;
    });




  }

  presentConfirm() {

    let date = new Date(this.datacount.datefirst);
    let date2 = new Date(this.datacount.dateend);
    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    this.teamdoc();

  }

  teamdoc() {

    this.isShown = true;

    let link = API_URL + 'OrderNow/list_Normal_sale.php';
    let myData = {
      empcode: this.employeecode,
      branchcode: this.newbranch,
      strbranch: this.strbranch,
      datefirst: this.datacount.datefirst,
      dateend: this.datacount.dateend,
      typedoc: this.datacount.typedoc,

    };

    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        this.posts = data;
        this.initializeItems();
        console.log('posts', this.posts);
        this.isShown = false;
      }, error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      });
    setTimeout(() => {
      this.isShown = false;
    }, 7000);

  }


  async openModal() {
    let modal = await this.modalController.create({
      component: DismissModalPage,
      componentProps: {
        //barCode: couponCode,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        this.datacount.namebranch = data.namebranch;
        this.newbranch = data.branchcode;
        this.storage.set('namebranch', data.namebranch);
        this.storage.set('newbranch', data.branchcode);

        this.teamdoc();

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }

  itemTapped(survey_doc_no: any, branchcode: any, branchname: any) {

    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: survey_doc_no,
        branchcode: branchcode,
        branchname: branchname
      }
    };
    this.router.navigate(['home'], navigationExtras);

  }

  openNotify() {

  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.isShown = true;
    setTimeout(() => {
      console.log('Async operation has ended');
      this.teamdoc();
      event.target.complete();
    }, 1500);
  }
  doRefresh2() {
    this.isShown = true;
    setTimeout(() => {
      this.teamdoc();
    }, 1500);
  }




  initializeItems() {
    this.items = this.posts;
  }
  initializeItems2() {
    this.items2 = this.posts2;
    console.log("data3!" + this.items2);
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.docuno.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  async clickToCartitem(item: any) {

    /* this.navCtrl.push(CartitemPage, {
      idtype: item.typeorderid,
      docuno: item.docuno,
      sellerid: item.sellerid,
    }); */

    let modal = await this.modalController.create({
      component: CartPage,
      componentProps: {
        idtype: item.typeorderid,
        docuno: item.docuno,
        sellerid: item.sellerid,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      console.log('CartPage', data);
      if (data == null) {

      } else {

        if(data.status == 'Y'){
          this.teamdoc();
        }

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });

  }


  async Removebill(sellerid: any, slidingItem: IonItemSliding) {

    let confirm = await this.alertCtrl.create({
      header: 'ลบข้อมูล?',
      message: 'ท่านยืนยันการการลบเลขที่เอกสารนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            console.log('Agree clicked');

            this.http.get(API_URL + 'OrderNow/Delete_listbill.php?sellerid=' + sellerid)
              .subscribe(data => {
                this.teamdoc();
              }, error => {

                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    confirm.present();
    slidingItem.close();
  }



  async showdialog() {

    let confirm = await this.alertCtrl.create({
      header: 'คำร้องขอส่งแก้ไขเอกสารไปบัญชีใหม่?',
      message: 'ท่านร้องขอส่งแก้ไขเอกสารไปบัญชีใหม่ สำเร็จแล้ว รอ ผอ.อนุมัติ เอกสาร',
      buttons: [{
        text: 'ตกลง'
      }]
    });
    confirm.present();

  }

  async  openApp() {
    let modal = await this.modalController.create({
      component: CartPage,
      componentProps: {
        sellerid: null,
        docuno: null
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      console.log('CartPage', data);
      if (data == null) {


      } else {

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }

}
