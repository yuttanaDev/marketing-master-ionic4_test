import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { TranslateService } from '@ngx-translate/core';
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { HttpService } from './../services/http.service';
// import { error } from 'console';
// import { error } from 'console';
@Component({
  selector: 'app-addlistlocation',
  templateUrl: './addlistlocation.page.html',
  styleUrls: ['./addlistlocation.page.scss'],
})
export class AddlistlocationPage implements OnInit {
  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';

  public linkimg: any = API_URL + 'UploadedLocation/';
  public isShown = true;

  public posts: any;
  public items: any;
  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public docno: any;
  public branchname: any;

  public typecusitem: any = [];
  public idLanguage: number = 2;
  public select_survey_type: any;
  public redioinput: any = [];

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public translate: TranslateService,
    public alertCtrl: AlertController,
    public HttpService : HttpService,
    ) {



    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });

    /* this.storage.get('language').then((idLanguage) => {
      this.idLanguage = idLanguage;
      if (idLanguage == 1) {
        console.log(' created login' + idLanguage);
      } else if (idLanguage == 2) {
      }
    }); */
    this.translate.get('addlistlocation.Select_survey_type').subscribe((res: string) => {
      this.select_survey_type = res;
      console.log('menu OK', this.select_survey_type);
    });


    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.doc_no;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        //โชว์รายชื่อคนออกสำรวจ
        this.loadData();
      }
    });

  }
  ionViewWillEnter() {
    this.doRefresh();
  }

  doRefresh() {
    this.isShown = true;
    this.items = [];
    setTimeout(() => {
      this.loadData();
    }, 1500);
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }
 /** API NODEJS */
  async loadData() {
    let mydata = {docno : this.docno};
    this.HttpService.post("survey/typecustomerlist",mydata).subscribe(
      (res : any)=> {
        this.posts = res;
        this.initializeItems();
        console.log('typecustomer_list', this.posts);
        this.isShown = false;
      },error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      }
    );
    // this.http.get(API_URL + 'typecustomer_list.php?docno=' + this.docno)
    //   .subscribe(data => {
    //     this.posts = data;
    //     this.initializeItems();
    //     console.log('typecustomer_list', this.posts);
    //     this.isShown = false;
    //   }, error => {
    //     this.isShown = false;
    //     console.log(JSON.stringify(error.json()));
    //   });

    setTimeout(() => {
      this.isShown = false;
    }, 10000);

  }

  initializeItems() {
    this.items = this.posts;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.facilityname.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }





  async removeItem(catagory_id: any, slidingItem: IonItemSliding) {
    slidingItem.close();

    const alert = await this.alertCtrl.create({
      header: 'ลบข้อมูล?',
      message: 'ท่านยืนยันการการลบข้อมูลการสำรวจนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ตกลง',
          handler: () => {
            console.log('Confirm Okay');
            let myData = {catagory_id : catagory_id };
            this.HttpService.post("survey/deletelocation" , myData).subscribe(
              (res : any) => {
                this.doRefresh();
              },error => {

              });
            //ฟังก์ชั่นลบสินค้า จำนวนสินค้า
            // this.http.get(API_URL + 'Delete_location.php?catagory_id=' + catagory_id)
            //   .subscribe(data => {
            //     this.doRefresh();
            //   }, error => {
            //   });
          }
        }
      ]
    });

    await alert.present();


  }



  itemTapped(catagory_id: any, type_sub: any) {
    console.log('type_sub data:', type_sub);

      //this.nav.navigateForward(['/endbill']);
    let navigationExtras: NavigationExtras = {
      state: {
        catid: catagory_id,
        id: parseInt(type_sub),
        docno: this.docno,
        branchcode: this.newbranch,
        branchname: this.branchname
      }
    };
    this.router.navigate(['addlocation'], navigationExtras);

  }
  itemFab(radiodata: any) {
    console.log('type_customer radiodata:', parseInt(radiodata));
    //this.nav.navigateForward(['/endbill']);
    let navigationExtras: NavigationExtras = {
      state: {
        docno: this.docno,
        id: parseInt(radiodata),
        branchcode: this.newbranch,
        branchname: this.branchname,
        catid: null,
      }
    };
    this.router.navigate(['addlocation'], navigationExtras);

  }

  async presentConfirm() {

    this.typecusitem = JSON.parse(localStorage.getItem('typecus'));
    console.log("typecusitem", this.typecusitem);

    this.typecusitem.forEach(element => {
      if (this.idLanguage == 1) {
        console.log("idLanguage1", this.idLanguage);
          this.redioinput.push({ type: 'radio', value: element.id_type, label: element.type_customer_en });
      } else if (this.idLanguage == 2) {
        console.log("idLanguage2", this.idLanguage);
        this.redioinput.push({ type: 'radio', value: element.id_type, label: element.type_customer });
      }else{
        console.log("idLanguage3", this.idLanguage);
      }
    });

    console.log("redioinput", this.redioinput);

    const alert = await this.alertCtrl.create({
      header: this.select_survey_type,
      inputs: this.redioinput,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ตกลง',
          handler: (data) => {
            console.log('Confirm Ok',data);
            this.itemFab(data);
          }
        }
      ]
    });

    await alert.present();


  }

  async presentAlertRadio() {
    const alert = await this.alertCtrl.create({
      header: this.select_survey_type,
      inputs: this.redioinput,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ตกลง',
          handler: (data) => {
            console.log('Confirm Ok',data);
            this.itemFab(data);
          }
        }
      ]
    });

    await alert.present();
  }



}


