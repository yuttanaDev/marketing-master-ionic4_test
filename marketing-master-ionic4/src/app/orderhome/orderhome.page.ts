import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController,ToastController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { CartPage } from '../cart/cart.page';
import { IonContent } from '@ionic/angular';
import { ScrollDetail } from '@ionic/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { BASEURL, URLIMG, API_URL,systemsetting } from '../../providers/constants/constants';


@Component({
  selector: 'app-orderhome',
  templateUrl: './orderhome.page.html',
  styleUrls: ['./orderhome.page.scss'],
})
export class OrderhomePage implements OnInit {
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';

  base_url: any = systemsetting.base_url;
  base_token: any = systemsetting.token;

  private posts: any; // <- I've added the private keyword 
  public items: any; // <- items property is now of the same type as posts
  private categories: any;
  public newbranch: any;
  public checkdata: any;
  public employeecode: any;
  public showToolbar: any;
  public namebranch: any;
  public branch: any;
  public remember: any = '';
  public barcode: any;
  public idtype: any;
  public gencode: any = '';
  public approve: boolean;
  private idedit: any;
  private docustatus: any;
  public orderid: any;
  public datacount: any;
  private setting: any;
  public itemcount: any = 0;

  public productData: any;

  public strbranch: any;


  public product: any;

  public list: any;
  public input: string = '';
  public countries: string[] = [];
  public datacart: any = [];

  public selectedCategory: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;

  public sellerid: any = null;
  public docnoid: any = null;
  public docno: any = null;

  public items_cat: any;
  public total: number;
  public cartdata: any = [];
  public cartItem: any = [];

  public pageNumber: number = 1;
  public pageAllpage: number = 0;
  public pageAllproduct: number = 0;
  custcode: any = null;
  idcard: any;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    private barcodeScanner: BarcodeScanner,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
  ) {

    this.approve = false;

    this.product = {
      scanData: '',
    }
    this.datacount = {};
    this.datacount.picture_name = null;

    let items_cat = JSON.parse(localStorage.getItem('items_category'));
    console.log("items_category", items_cat);
    this.items_cat = items_cat;

    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];

      console.log("docnoid", docnoItem[0]['docnoid']);
      this.itemcountproduct();
    } else {
      var data = JSON.parse(localStorage.getItem('cart'));
      console.log("data1", data);
      if (data != null) {
        this.itemcount = parseInt(data.length);
      }
    }

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.strbranch = newbranch.replace('-', '');
      console.log('newbranch : ' + this.newbranch);
      this.productData = null;
      this.loadProductallpage();
      this.loadProductPromotion(1);

    });

    this.loadcart();

    

  }

  /* ngAfterViewInit() {

    

  } */
  gotoSearch() {
    //this.navCtrl.push(SearchproductPage);
    this.navCtrl.navigateForward("searchproduct");
  }

  gotoProductDetail(item) {

    console.log('product_barcode', item.barcode_code);

    let navigationExtras: NavigationExtras = {
      state: {
        product_item:item,
      }
    };
    this.router.navigate(['product'], navigationExtras);

  }

  doRefresh(event) {
    this.productData = null;
    this.loadProductallpage();
    this.loadProductPromotion(1);
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onScroll($event: CustomEvent<ScrollDetail>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
    const scrollTop = $event.detail.scrollTop;
    //this.showToolbar = scrollTop >= 225;

    if (scrollTop >= 850) {
      // console.log("scrolling down, hiding footer...");
      this.showToolbar = true;
     } else if (scrollTop < 850 ){
      // console.log("scrolling up, revealing footer...");
       this.showToolbar = false;
     };  

    }

    

    }

    ScrollToTop(){
      this.showToolbar = false;
      this.content.scrollToTop(1500);
    }

    checkAllnumber(str: string) {
      if (str.match(/^-{0,1}\d+$/)) {
        return true;
      } else {
        return false;
      }
    }
  
    ValidURL(str) {
      var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
      if (!regex.test(str)) {
        return false;
      } else {
        return true;
      }
    }
  
    similar(a, b) {
      var equivalency = 0;
      var minLength = (a.length > b.length) ? b.length : a.length;
      var maxLength = (a.length < b.length) ? b.length : a.length;
      for (var i = 0; i < minLength; i++) {
        if (a[i] == b[i]) {
          equivalency++;
        }
      }
      var weight = equivalency / maxLength;
      return (weight * 100);
    }

     //----Search scan-----
  scanSearchQR() {
    this.barcodeScanner.scan().then(barcodeData => {
      var getScanData = barcodeData.text; //'https://globalhouse.co.th/product/detail/8855983007513/441';

      if (this.checkAllnumber(getScanData)) { // is Number
        //this.navCtrl.push(CatalogGridPage, { txtSearch: getScanData, txtgroupId: 0 });
  
          let navigationExtras: NavigationExtras = {
            state: {
              txtSearch: getScanData,
              txtgroupId: 0
            }
          };
          this.router.navigate(['tabs/shop/catalog-grid'], navigationExtras);
   
      } else {  // check URL

        if (this.ValidURL(getScanData)) { // is URL
          var url = getScanData;
          if (this.similar(url, 'https://globalhouse.co.th/product/detail/') > 60) { // is URL global
            var getBarcode = url.split('/');
            //this.navCtrl.push(CatalogGridPage, { txtSearch: getBarcode[5], txtgroupId: 0 });
            let navigationExtras: NavigationExtras = {
              state: {
                txtSearch: getBarcode[5],
                txtgroupId: 0
              }
            };
            this.router.navigate(['tabs/shop/catalog-grid'], navigationExtras);

          } else if (this.similar(url, 'https://sellercenter-global.com/Qrtracker/marketingpro') > 60) {  // is open URL other
            if (this.custcode != null && this.custcode != '') {
              let newurl = url + '&custCode=' + this.custcode + '&custId=' + this.idcard;
               //this.alertPopup('แจ้งเตือน', newurl);
              window.open(newurl, '_system', 'location=yes');
            } else {
              this.alertPopup('แจ้งเตือน', 'กรุณาเข้าสู่ระบบหรือลงทะเบียน ก่อนทำการแสกนรับคูปอง');
            }
          } else {  // is open URL other
            window.open(url, '_system', 'location=yes');
          }
        } else {
          // other data
          this.alertPopup('แสดงข้อมูล', getScanData);
        }

      }
    }).catch(err => {
      // alert('Barcode data:'+ err);
      //this.alertPopup('ผิดพลาด', err);

      this.toastPopup();
    });



  }

    async loadProductallpage() {
      var postData = JSON.stringify({
        token: this.base_token
      });
      this.http.post(this.base_url + 'Products/getproductpromotionall', postData)
  
        .subscribe(data => {
          if (data['status'] == 200) {
            this.pageAllpage = data['pro_allpage'];
            this.pageAllproduct = data['pro_all'];
  
            //console.log('mim:'+data.minPrice+" max:"+data.maxPrice);
          }
        },
          error => { console.log('no all Data Cat'); });
    }

  async loadProductPromotion(setpageNum: number) {

    /* var postData = JSON.stringify({
      "pageNum": "1",
      "token": "R2wwYkBsQXBwNWVydjFjZQ==",
      "newbranch": "GH-101"
    });

    this.http.post(this.base_url + 'ProductsBranch/getproductpromotion', postData) */
    let url = this.base_url + 'ProductsBranch/getproductpromotion';
    let myData = {
      pageNum: setpageNum,
      branch_code: this.newbranch,
      token: this.base_token
    };

    console.log('no all Data Cat',JSON.stringify(myData));
    this.http.post(url, JSON.stringify(myData))
      .subscribe(data => {
        if (data != null) {

          if (this.productData == null) {
            this.productData = data;

          } else {
            this.items = data;
            this.items.forEach(element => {
              //console.log(element);
              this.productData.push({
                pro_barcode: element.pro_barcode,
                pro_img1: element.pro_img1,
                pro_name_new: element.pro_name_new,
                pro_price: element.pro_price,
                pro_sale_price: element.pro_sale_price,
                status: 200
              }
              );
            });
          }
         

        }

        console.log('Data ', data);
      },
        error => {
          console.log('no Data ');

        });

  }

   //load more pro
   loadProductInfinite(infiniteScroll) {
    setTimeout(() => {

      // ถ้าไม่ได้มาจาก cat
      if (this.pageNumber <= this.pageAllpage) {
        this.pageNumber++;
        //this.loadProduct(this.pageNumber);
        this.loadProductPromotion(this.pageNumber);
      }

      console.log('pageNumber', this.pageNumber);

      infiniteScroll.target.complete();

    }, 300);
  }

  loadcart() {
    this.total = 0;
    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    // console.log("data1", this.cartItem);
    this.cartdata = this.cartItem;
    console.log("01count:" + this.cartdata, this.cartItem);
    if (this.cartdata != null) {
      this.cartdata.forEach(item => {  //ตัวอย่าง foreach
        this.total += item.subtotal;  //ยอดรวม
      });
      this.itemcount = parseInt(this.cartdata.length);
    }

  }

  itemcountproduct() {
    this.http.get(API_URL + 'OrderNow/itemcount_product.php?sellerid=' + this.docnoid)
      .subscribe(data => {
        if (data != null) {
          console.log('itemcount_product : ', data);
          this.itemcount = parseInt(data[0]['countitem']);
        } else {
          this.itemcount = 0;
        }
      }, error => {

        console.log(JSON.stringify(error.json()));
      });
  }

  ngOnInit() {
  }

  gotoCategoryProduct(product_category_id: any, category_code: any, product_category_name: any) {

    /* this.navCtrl.push(OrdercategoryPage, {
      product_category_id: product_category_id,
      category_code: category_code,
      product_category_name:product_category_name
    }); */

    let navigationExtras: NavigationExtras = {
      state: {
        product_category_id: product_category_id,
        category_code: category_code,
        product_category_name: product_category_name,
        keyword: null,
        pageid:1
      }
    };
    this.router.navigate(['ordercategory'], navigationExtras);



  }

  async addcartnow() {

    var docnoItem = JSON.parse(localStorage.getItem('docnoid'));
    if (docnoItem != null) {
      this.docnoid = docnoItem[0]['docnoid'];
      this.docno = docnoItem[0]['docno'];
      console.log("docnoid", docnoItem[0]['docno']);
      /* let navigationExtras: NavigationExtras = {
        state: {
          sellerid: this.docnoid,
          docuno: this.docno
        }
      };
      this.router.navigate(['cart'], navigationExtras); */

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: this.docnoid,
          docuno: this.docno,
          idtype: this.idtype,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage', data);
        if (data == null) {


        } else {

        }

      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });


    } else {

      let modal = await this.modalController.create({
        component: CartPage,
        componentProps: {
          sellerid: null,
          docuno: null,
          idtype: null,
        },
        showBackdrop: true,
        backdropDismiss: true,
        enterAnimation: myEnterAnimation,
        leaveAnimation: myLeaveAnimation
      });
      modal.onWillDismiss().then(dataReturned => {
        let data = dataReturned.data;
        console.log('CartPage', data);
        if (data == null) {


        } else {

        }

      });
      return await modal.present().then(_ => {
        // triggered when opening the modal
      });

    }



  }

  search() {


    let navigationExtras: NavigationExtras = {
      state: {
        product_category_id: null,
        category_code: null,
        product_category_name: null,
        keyword: this.input.toLowerCase(),
        pageid:2
      }
    };
    this.router.navigate(['ordercategory'], navigationExtras);
  }


  async alertPopup(title: string, Msg: string) {
    let alert = await this.alertCtrl.create({
     header: title,
      message: Msg,
      buttons: ['OK']
    })
    await alert.present();
  }

  async toastPopup() {
    let toast = await this.toastCtrl.create({
      message: "ขออภัยในความไม่สะดวก ตอนนี้ไม่สามารถเชื่อมต่อระบบได้ กรุณาลองใหม่อีกครั้ง",
      duration: 5000,
      position: "middle"
    });
    await toast.present();
  }

}
