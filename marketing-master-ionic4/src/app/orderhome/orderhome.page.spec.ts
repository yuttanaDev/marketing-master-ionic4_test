import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderhomePage } from './orderhome.page';

describe('OrderhomePage', () => {
  let component: OrderhomePage;
  let fixture: ComponentFixture<OrderhomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderhomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderhomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
