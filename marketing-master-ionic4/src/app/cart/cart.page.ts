import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController, AlertController, ToastController, NavParams } from '@ionic/angular';
import { ModalController, MenuController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';   // loading
import { Events } from '@ionic/angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { DismissModalnormalPage } from '../dismiss-modalnormal/dismiss-modalnormal.page';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { API_URL } from '../../providers/constants/constants';

const BASEURL = API_URL;

declare var cordova;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage {

  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';
  public togglecustomers: any;
  public options: BarcodeScannerOptions;
  discuont: any = {};
  myDate: any;
  setQty: number;
  getprice: number;
  total: number;
  docno: string;
  image_url: string;
  public cartdata: any = [];
  public cartItem: any = [];
  public datacart: any = [];
  public cart: any = [];
  public itemcount: any = 0;
  public additem: any = 0;
  public count: any = 1;
  public totalprice: number;

  public newbranch: any;
  public employeecode: any;
  public namebranch: any;
  public strbranch: any;
  public pay_status: any = 0;
  public discount: any = 0;
  public orderamnt_sum: any = 0;
  public grant_total: any = 0;
  public val: any;
  public gbh_customer_id: any;
  public datacount: any = {};
  public docnoid: any = null;

  public branch_code: any;
  public branch_name: any;
  public latitude: any;
  public longitude: any;

  public product: any = {
    scanData: '',
  };
  public barcode: any;

  data: any;
  test: any;
  public idtype: any;
  public docuno: any;
  public gencode: any;
  public approve: boolean;
  public idedit: any;
  public sellerid: any = null;

  public datacode: any;
  public items_detail: any;
  public typeemp: any;
  constructor(public navCtrl: NavController,
    //public navParams: NavParams,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public httpClient: HttpClient,
    public storage: Storage,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public barcodeScanner: BarcodeScanner,
    public route: ActivatedRoute,
    public router: Router,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public events: Events, ) {

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.strbranch = newbranch.replace('-', '');
      console.log('strbranch : ' + this.strbranch);
      this.checkBranch();
    });
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
    });
    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
    });
    this.storage.get('typeemp').then((typeemp) => {
      this.typeemp = typeemp;
    });
    
    this.sellerid = navParams.get("sellerid");
    this.docuno = navParams.get("docuno");
    this.idtype = navParams.get("idtype");

    console.log('sellerid : ', this.sellerid, this.docuno, this.idtype);

    this.loadcart();

    /*   //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.sellerid = this.router.getCurrentNavigation().extras.state.sellerid;
        this.docuno = this.router.getCurrentNavigation().extras.state.docuno;
        this.idtype = this.router.getCurrentNavigation().extras.state.idtype;
        
      }
    });
 */
  }
  async thaiqrCode() {

    const url = 'https://globalhouse.co.th/Checkout/payment_thaiqr?order_code=' + this.docuno
    const options = {
      zoom: 'no',
      toolbar: 'yes',
      toolbarcolor: '#ffc107',
      location: 'yes',
      clearcache: 'yes',
      hardwareback: 'no',
      hideurlbar: 'yes',
      footer: 'no',
      toolbarposition: 'top'
    }
    const browser = this.iab.create(url, '_blank', { options });
    browser.on('loadstart').subscribe((eve) => {
      // this.spinnerDialog.show(null, null, true);
    }, err => {
      //this.spinnerDialog.hide();
    })

    browser.on('loadstop').subscribe(() => {
      // this.spinnerDialog.hide();
    }, err => {
      // this.spinnerDialog.hide();
    })

    browser.on('loaderror').subscribe(() => {
      //  this.spinnerDialog.hide();
    }, err => {
      //  this.spinnerDialog.hide();
    })

    browser.on('exit').subscribe(() => {
      //load status
      //this.loadOrderstatus();

    }, err => {
      console.error(err);
    });
  }
  async clearAll() {

    let confirm = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'เคลียสินค้าทั้งหมด?',
      message: 'ท่านยืนยันเคลียสินค้าทั้งหมดหรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            console.log('Agree clicked');
            this.datacount = {};
            this.datacount.cusid = '';
            this.datacount.identification_card = '';
            this.datacount.mobile = '';
            this.datacount.fullname = '';
            this.datacount.comment = '';
            this.total = 0;
            this.grant_total = 0;
            this.cartItem = [];
            this.cartdata = [];
            this.totalprice = 0;
            
            this.cart = [];
            this.itemcount = 0;
            this.count = 1;
            localStorage.setItem('cart', JSON.stringify(this.cartItem));
            localStorage.setItem('docnoid', null);
          }
        }
      ]
    });
    confirm.present();


  }

  //Open check out
  async checkOut() {

    const alert = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'ยืนยันการสั่งซื้อหรือไม่',
      message: 'คุณต้องการยืนยันการสั่งซื้อนี้หรือไม่',
      /* inputs: [
        {
          name: 'comment',
          type: 'text',
          placeholder: 'comment'
        },
  
      ], */
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: (data) => {
            //ปิดตัวเอง ออก
            this.storage.get('profile').then((profile) => {
            });
            this.saveproductNormal();
          }
        }
      ]
    });
    alert.present();



  }
  async Discount() {

    let confirm = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'ให้ส่วนลดเอกสารบิลนี้?',
      message: 'ท่านยืนยันให้ส่วนลดเอกสารบิลนี้นี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ให้ส่วนลด',
          handler: () => {
            console.log('Agree clicked');
            let link = BASEURL + 'OrderNow/discount_order.php';
            let myData = {
              sellerid: this.sellerid,
              docuno: this.docuno,
              idtype: this.idtype,
              empcode: this.employeecode,
              newbranch: this.newbranch,
              discount: this.datacount.discount,
              typeemp: this.typeemp
            };
        
            this.httpClient.post(link, JSON.stringify(myData))
              .subscribe(data => {
                console.log("data Discount", data);
                  let title = data['title'];
                  let detail = data['detail'];
                if (data['status'] === 200) {
                  this.saveAlert(title,detail);
                } else if (data['status'] === 201) {
                  this.saveAlert(title,detail);
                } else if (data['status'] === 202) {
                  this.saveAlert(title,detail);
                } else if (data['status'] === 203) {
                  this.saveAlert(title,detail);
                } else if (data['status'] === 401) {
                  this.saveAlert(title,detail);
                }else {
                  this.saveAlert(title,detail);
                }

                this.loadcart();
        
              }, Error => {
                //loading.dismiss();
              });
          }
        }
      ]
    });
    confirm.present();

  }

  async saveAlert(title:any,detail:any) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: detail,
      buttons: ['ตกลง']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 7000);
  }

  saveproductNormal() {

    /*  let loading = this.loadingCtrl.create({
       content: 'please wait...',
       duration: 1000
     });
     loading.present(); */

    let link = BASEURL + 'OrderNow/insert_data_product_normal.php';
    let myData = {

      itemproduct: this.cartdata,
      branchcode: this.newbranch,
      employeecode: this.employeecode,
      strbranch: this.strbranch,
      cusid: this.datacount.cusid,
      idcard: this.datacount.identification_card,
      mobile: this.datacount.mobile,
      fullname: this.datacount.fullname,
      comment: this.datacount.comment,
      email: this.datacount.email,
      sexcus: this.datacount.sexcus
    };

    this.httpClient.post(link, JSON.stringify(myData))
      .subscribe(data => {

        console.log("data Save doc:" + data);

        if (data['status'] === 200) {

          this.clearAlliTem();

          this.sellerid = data['sellerid'];
          this.docuno = data['docuno'];

          console.log('Update sellerid:', this.sellerid, this.docuno);
          this.loadcart();
          this.datacode = null;
          //this.modalController.dismiss(this.datacode);

          //loading.dismiss();
          //this.events.publish('reloadListorer');
          //this.navCtrl.popToRoot();
          ///this.navCtrl.popTo(ListOrderPage);

        } else {

          this.datacode = null;
          //this.modalController.dismiss(this.datacode);

        }


      }, Error => {
        //loading.dismiss();
      });

  }
  clearAlliTem() {

    this.datacount = {};
    this.datacount.cusid = '';
    this.datacount.identification_card = '';
    this.datacount.mobile = '';
    this.datacount.fullname = '';
    this.datacount.comment = '';
    this.total = 0;
    this.grant_total = 0;
    this.cartItem = [];
    this.cartdata = [];
    this.totalprice = 0;
    this.cart = [];
    this.itemcount = 0;
    this.count = 1;

    localStorage.setItem('cart', JSON.stringify(this.cartItem));
    localStorage.setItem('docnoid', null);


  }

  add(item: any) {

    //this.count = this.count + 1;

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.cartItem == null || this.cartItem == '') {
      this.itemcount = 0;
    } else {
      this.datacart = this.cartItem;
      this.itemcount = parseInt(this.datacart.length);

      for (var i = 0; i < this.itemcount; i++) {
        if (item.product_barcode == this.datacart[i].product_barcode) { //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน       
          if (item.stockbalance >= (this.count + this.datacart[i].count)) {
            this.datacart[i].count += this.count;

            if (this.datacart[i].pro_price > 0) {
              this.totalprice = (this.datacart[i].pro_price * this.count);
            } else {
              this.totalprice = (this.datacart[i].product_price1 * this.count);
            }

            this.datacart[i].subtotal = (this.datacart[i].subtotal) + (this.totalprice * this.count);

            console.log('Update bar subtotal :', this.datacart[i].subtotal);

          } else {
            // console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
          }
          break;
        }
      }
      this.cartdata = this.cartItem
      localStorage.setItem('cart', JSON.stringify(this.cartdata));
      //console.log('Insert bar2:', this.cart);

      /* let cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("Insert cartItem", cartItem);
      if(this.cartItem != null){
        this.itemcount = parseInt(this.cartItem.length);
      } */

      this.loadcart();

    }

  }

  sub(item: any) {


    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.cartItem == null || this.cartItem == '') {
      this.itemcount = 0;
    } else {
      this.datacart = this.cartItem;
      this.itemcount = parseInt(this.datacart.length);

      for (var i = 0; i < this.itemcount; i++) {
        if (item.product_barcode == this.datacart[i].product_barcode) { //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน   

          console.log('Update count  :', this.count - this.datacart[i].count);

          if (item.stockbalance > (this.count - this.datacart[i].count) && this.datacart[i].count > 1) {
            this.datacart[i].count -= this.count;
            console.log('Update count2  :', this.datacart[i].count);
            if (this.datacart[i].pro_price > 0) {
              this.totalprice = (this.datacart[i].pro_price * this.count);
            } else {
              this.totalprice = (this.datacart[i].product_price1 * this.count);
            }

            this.datacart[i].subtotal = (this.datacart[i].subtotal) - (this.totalprice * this.count);

            console.log('Update bar subtotal :', this.count - this.datacart[i].count);

          }
          break;
        }
      }
      this.cartdata = this.cartItem
      localStorage.setItem('cart', JSON.stringify(this.cartdata));
      //console.log('Insert bar2:', this.cart);

      /* let cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("Insert cartItem", cartItem);
      if(this.cartItem != null){
        this.itemcount = parseInt(this.cartItem.length);
      } */

      this.loadcart();

    }




  }

  async itemCountProduct(item: any) {

    let prompt = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'กรอกจำนวนสินค้า',
      message: "",
      inputs: [
        {
          type: 'number',
          name: 'countitem',
          placeholder: '0',
          max: item.stockbalance,
        },
      ],
      buttons: [
        /* {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }, */
        {
          text: 'ตกลง',
          handler: async (data) => {
            console.log('stockbalance', item.stockbalance - item.count, data);
            if (data.countitem != '') {
              if ((item.stockbalance - item.count) >= (parseInt(data.countitem))) {
                console.log('countitem', (parseInt(data.countitem)));
                if ((parseInt(data.countitem)) > 0) {
                  this.additem = (parseInt(data.countitem));
                  console.log('additem', this.additem);
                  this.itemCount(this.additem, item);
                }
              }
              else {
                let prompt = await this.alertCtrl.create({
                  cssClass: 'alertCustomCss',
                  header: 'จำนวนที่กรอกมากไป',
                  message: "จำนวนเยอะกว่าสต๊อคที่มีอยู่ สต็อคที่มี : " + (item.stockbalance - item.count),
                  buttons: [
                    {
                      text: 'ตกลง',
                      handler: data => {
                      }
                    },
                  ]
                });
                prompt.present();
                setTimeout(() => {
                  prompt.dismiss();
                }, 3000);
              }
            }
          }
        }
      ]
    });
    prompt.present();


  }

  itemCount(countitem: any, item: any) {

    //this.count = this.count + 1;

    this.additem = countitem;

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.cartItem == null || this.cartItem == '') {
      this.itemcount = 0;
    } else {
      this.datacart = this.cartItem;
      this.itemcount = parseInt(this.datacart.length);

      for (var i = 0; i < this.itemcount; i++) {
        if (item.product_barcode == this.datacart[i].product_barcode) {
          //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน      
          console.log('Update :', this.additem + this.datacart[i].count);
          if (item.stockbalance >= (this.additem + this.datacart[i].count)) {

            this.datacart[i].count += this.additem;

            console.log('Update datacart count :', this.datacart[i].count);


            if (this.datacart[i].pro_price > 0) {
              this.totalprice = (this.datacart[i].pro_price * this.datacart[i].count);

              console.log('Update totalprice1 :', this.totalprice);

            } else {
              this.totalprice = (this.datacart[i].product_price1 * this.datacart[i].count);
              console.log('Update totalprice1 :', this.totalprice);
            }

            this.datacart[i].subtotal = (this.totalprice);

            console.log('Update bar subtotal :', this.datacart[i].subtotal);

          } else {
            console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
          }
          break;
        }
      }
      this.cartdata = this.cartItem
      localStorage.setItem('cart', JSON.stringify(this.cartdata));
      //console.log('Insert bar2:', this.cart);

      /* let cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("Insert cartItem", cartItem);
      if(this.cartItem != null){
        this.itemcount = parseInt(this.cartItem.length);
      } */

      this.loadcart();

    }

  }

  loadcart() {
    this.total = 0;
    this.grant_total = 0;
    this.cartItem = [];
    this.cartdata = [];

    if (this.sellerid == null) {

      this.cartItem = JSON.parse(localStorage.getItem('cart'));
      // console.log("data1", this.cartItem);
      this.cartdata = this.cartItem;
      console.log("01count:" + this.cartdata, this.cartItem);
      if (this.cartdata != null) {
        this.cartdata.forEach(item => {  //ตัวอย่าง foreach
          this.total += item.subtotal;  //ยอดรวม
        });
        this.itemcount = parseInt(this.cartdata.length);
      }

    } else {


      let link = BASEURL + 'OrderNow/list_cartitem_product.php';
      let myData = {
        sellerid: this.sellerid,
        docuno: this.docuno,
        idtype: this.idtype,
        empcode: this.employeecode,
        newbranch: this.newbranch,

      };

      this.httpClient.post(link, JSON.stringify(myData))
        .subscribe(data => {

          if (data != null) {

            this.cartdata = data['items_product'];
            this.items_detail = data['items_detail'];

            this.pay_status = data['items_detail'][0]['pay_status'];
            let countitem = data['countitem'];
            this.itemcount = countitem;
            let cartdata = this.cartdata;
            console.log("pay_status:", this.pay_status, this.items_detail);
            
            this.discount = this.items_detail[0]['discount_amnt'];
            this.orderamnt_sum = this.items_detail[0]['orderamnt_sum'];
            this.grant_total = this.items_detail[0]['grant_total'];

            this.total = this.grant_total;
            console.log("total data:", this.total,this.cartdata);

          } else {

          }
          // loading.dismiss();

        }, Error => {
          //loading.dismiss();
        });
      setTimeout(() => {
        //loading.dismiss();
      }, 15000);



    }


    /* this.docnoid = localStorage.getItem('docnoid');
    if(this.docnoid != null){
      console.log("docnoid", this.docnoid);
    } */


    //console.log("vendor_code", this.cartdata[0]['vendor_code']);

  }
  modalhide() {
    this.datacode = null;
    this.modalController.dismiss(this.datacode);
  }




  checkBranch() {

    let link = API_URL + "OrderNow/checkBranchLocation.php?";
    this.httpClient.get(link + 'branchcode=' + this.newbranch)
      .subscribe(data => {
        console.log("Branch latitude1" + data);
        if (data != null) {
          this.branch_code = data[0].branch_code;
          this.branch_name = data[0].branch_name;
          this.latitude = data[0].latitude;
          this.longitude = data[0].longitude;

          console.log("Branch latitude" + this.latitude, this.longitude);

        } else {

        }

      },
        error => {

        })
    setTimeout(() => {
    }, 7000);


  }

  Scan() {
    this.options = {
      prompt: "แสกนรหัสสินค้า"
    }

    this.barcodeScanner.scan(this.options).then((barcodeData) => {

      console.log(barcodeData);

      if (barcodeData === null) {
        return;
      }
      let barcode = barcodeData.text;
      let productCode: any = barcode.substring(barcode.lastIndexOf('/') + 1, barcode.length);
      //alert(productCode);

      if (this.idtype == 1) {
        this.prefillFromDBNormal(productCode);
        console.log("Normal");
      } else if (this.idtype == 2) {
        //this.prefillFromDB(productCode);
        console.log("Stock");
      }

    }, (err) => {
      console.log("Error occured : " + err);
      alert(err);
    });

  }

  public getItems(ev: any) {
    // set val to the value of the searchbar
    this.val = ev.target.value;

  }

  searchForTerm() {
    this.datacount = {};
    //let val2 = this.val;
    console.log("val : ", this.val);
    let link = BASEURL + "OrderNow/checkcustomer.php?";
    let myData = {
      idcard: this.val,
      branchcode: this.newbranch,
      employeecode: this.employeecode,
      strbranch: this.strbranch

    };
    this.httpClient.post(link, JSON.stringify(myData))
      .subscribe(async data => {
        if (data != null) {

          console.log('Sentar data' + data);
          this.gbh_customer_id = data[0]['gbh_customer_id'];
          this.datacount.cusid = data[0]['customer_barcode'];
          this.datacount.firstname = data[0]['firstname'];
          this.datacount.lastname = data[0]['lastname'];
          this.datacount.fullname = data[0]['fullname'];
          this.datacount.sexcus = data[0]['sex'];
          this.datacount.full_address = data[0]['full_address'];
          this.datacount.identification_card = data[0]['identification_card'];
          this.datacount.mobile = data[0]['mobile'];
          this.datacount.email = data[0]['email'];
          this.datacount.branchname = data[0]['branch_name'];


        } else {
          let alert = await this.alertCtrl.create({
            header: 'ไม่มีข้อมูล',
            subHeader: 'ไม่มีข้อมูล',
            buttons: ['ตกลง']
          });
          alert.present();

        }

      },
        async error => {
          //this.showError(error);
          let alert = await this.alertCtrl.create({
            header: 'ไม่มีข้อมูล',
            subHeader: error,
            buttons: ['ตกลง']
          });
          alert.present();

        })
  }

  async prefillFromDBNormal(barcodeDataText) {

    this.barcode = barcodeDataText;

    let loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 2000
    });
    await loading.present();

    let link = BASEURL + 'OrderNow/Normal_sale.php';
    let myData = {
      branchcode: this.newbranch,
      barcode: this.barcode,
      employeecode: this.employeecode,
    };
    this.httpClient.post(link, JSON.stringify(myData))
      .subscribe(
        data => {


          if (data != null) {
            //this.itemproduct = data;
            console.log("itemproduct", data);
            var productcode = data[0].product_code;
            // this.datacount.vendor_code =  data[0].vendor_code;
            this.datacount.barcode_bill_name = data[0].barcode_bill_name;
            this.datacount.barcode_code = data[0].barcode_code;
            this.datacount.unit_code = data[0].unit_code;
            this.datacount.product_price1 = data[0].product_price1;
            this.datacount.pro_price = data[0].pro_price;
            this.datacount.stockbalance = data[0].stockbalance;
            this.datacount.item_r11 = data[0].item_r11;
            this.datacount.stockonsale = data[0].stockonsale;

            this.datacount.vendor_code = data[0].vendor_code;
            this.datacount.branch_code = data[0].branch_code;
            this.datacount.branch_name = data[0].branch_name;
            this.datacount.category_id = data[0].category_id;
            this.datacount.category_name = data[0].category_name;
            this.datacount.group_id = data[0].group_id;
            this.datacount.group_name = data[0].group_name;
            this.datacount.good_pattern_id = data[0].good_pattern_id;
            this.datacount.good_pattern_name = data[0].good_pattern_name;
            // this.datacount.picturename = data[0].picturename;

            console.log("branch_name", this.datacount.branch_name);

            this.openModalNormal(null, productcode);
            loading.dismiss();

          } else {
            this.noProductAlert();
            loading.dismiss();

          }

        },
        error => {
          loading.dismiss();
          this.serverAlert();
        });

  }



  async noProductAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่พบสินค้า',
      subHeader: 'ไม่พบสินค้า, หรือให้ทำการแสกนใหม่อีกครั้ง',
      //buttons: ['OK']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }
  async noDataAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่มีข้อมูล',
      subHeader: 'ไม่มีข้อมูล, หรือให้ทำการแสกนใหม่อีกครั้ง',
      //buttons: ['OK']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }
  async serverAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้',
      subHeader: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้, หรือให้ทำการแสกนใหม่อีกครั้ง!',
      //buttons: ['OK']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }

  showimage(productCode) {

    this.httpClient.get(BASEURL + 'OrderNow/showimage.php?productCode=' + productCode)
      .subscribe(data => {
        this.datacount.picturename = data[0].picturename;
      }, error => {
      });

  }


  async openModalNormal(sellerdtid: any, codeData: any) {
    const modal = await this.modalController.create({
      component: DismissModalnormalPage,
      componentProps: {
        procode: codeData,
        barcode_code: this.datacount.barcode_code,
        barcode_bill_name: this.datacount.barcode_bill_name,
        unit_code: this.datacount.unit_code,
        product_price1: this.datacount.product_price1,
        pro_price: this.datacount.pro_price,
        stockbalance: this.datacount.stockbalance,
        item_r11: this.datacount.item_r11,
        stockonsale: this.datacount.stockonsale,
        sellerid: this.sellerid,
        sellerdtid: sellerdtid,
        vendor_code: this.datacount.vendor_code,
        branch_code: this.datacount.branch_code,
        branch_name: this.datacount.branch_name,
        category_id: this.datacount.category_id,
        category_name: this.datacount.category_name,
        group_id: this.datacount.group_id,
        group_name: this.datacount.group_name,
        good_pattern_id: this.datacount.good_pattern_id,
        good_pattern_name: this.datacount.good_pattern_name,
        picturename: this.datacount.picturename,
        other: { couldAlsoBeAnObject: true }
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();

    console.log('data modal', data);

    if (data != null) {

      let status = data.status;
      this.datacount.dataPrice = data.totalPrice;
      this.datacount.poquantity = data.poquantity;

      if (status = 'Y') {
        console.log("saveproduct", 'บันทึก' + this.datacount.dataPrice + this.datacount.poquantity);
        //this.saveproductNormal();
      }

    } else {
      console.log("saveproduct", 'ไม่บันทึก');
    }

  }

  async removeItem(index, slidingItem: IonItemSliding) {

    //alert(index);
    console.log("0count:" + this.cartdata.length);

    const alert = await this.alertCtrl.create({
      header: 'ยืนยันลบหรือไม่',
      message: 'คุณต้องการยืนยันการลบสินค้านี้หรือไม่',

      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยันลบ',
          handler: (data) => {
            //ปิดตัวเอง ออก
            if (this.cartdata.length == 1) {
              this.cartdata.splice(index, 1);
              this.cartItem = [];
              localStorage.setItem('cart', JSON.stringify(this.cartItem));

              this.loadcart();
              console.log("1count:" + this.cartdata.length);
            } else {
              this.cartdata.splice(index, 1);
              this.cartItem = this.cartdata;
              localStorage.setItem('cart', JSON.stringify(this.cartItem));

              this.loadcart();
              console.log("2count:" + this.cartdata);

            }
          }
        }
      ]
    });
    alert.present();

    slidingItem.close();
    //this.storage.set('cart',null);
  }

  refreshItem(item: any) {

    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    if (this.cartItem == null || this.cartItem == '') {
      this.itemcount = 0;
    } else {
      this.datacart = this.cartItem;
      this.itemcount = parseInt(this.datacart.length);

      for (var i = 0; i < this.itemcount; i++) {
        if (item.product_barcode == this.datacart[i].product_barcode) {
          //ถ้ามีสินค้าเดิมอยู่ให้ เพิ่มจำนวน      
          console.log('Update :', 1);
          if (item.stockbalance >= 1) {
            this.datacart[i].count = 1;
            console.log('Update datacart count :', 1);

            if (this.datacart[i].pro_price > 0) {
              this.totalprice = (this.datacart[i].pro_price * 1);

              console.log('Update totalprice1 :', this.totalprice);

            } else {
              this.totalprice = (this.datacart[i].product_price1 * 1);
              console.log('Update totalprice1 :', this.totalprice);
            }

            this.datacart[i].subtotal = (this.totalprice);

            console.log('Update bar subtotal :', this.datacart[i].subtotal);

          } else {
            console.log('Update bar stockbalance ,count:', this.datacart[i].stockbalance, this.datacart[i].count);
          }
          break;
        }
      }
      this.cartdata = this.cartItem
      localStorage.setItem('cart', JSON.stringify(this.cartdata));
      //console.log('Insert bar2:', this.cart);

      /* let cartItem = JSON.parse(localStorage.getItem('cart'));
      console.log("Insert cartItem", cartItem);
      if(this.cartItem != null){
        this.itemcount = parseInt(this.cartItem.length);
      } */

      this.loadcart();

    }

  }





  //Open check out
  async checkOutServer() {

    const alert = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'ยืนยันใบสั่งขายหรือไม่',
      message: 'คุณต้องการยืนยันบันทึกใบสั่งขายนี้หรือไม่',
      /* inputs: [
        {
          name: 'comment',
          type: 'text',
          placeholder: 'comment'
        },
  
      ], */
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: (data) => {
            console.log("sellerid:" + this.sellerid);
            this.approveBill();
          }
        }
      ]
    });
    alert.present();



  }


  approveBill() {


    let link = BASEURL + 'OrderNow/approve_bill.php';
    let myData = {
      branchcode: this.newbranch,
      employeecode: this.employeecode,
      strbranch: this.strbranch,
      sellerid: this.sellerid,
      docuno: this.docuno
    };

    this.httpClient.post(link, JSON.stringify(myData))
      .subscribe(data => {

        console.log("data Save doc:" + data);

        if (data != null) {
          localStorage.setItem('docnoid', null);
          this.datacode = {
            status: 'Y',
            msg: "บันทักสำเร็จ"
          };
          this.modalController.dismiss(this.datacode);
        } else {

        }

      }, Error => {

      });

  }


  async removeItemServer(sellerdtid, slidingItem: IonItemSliding) {


    console.log('Disagree clicked', sellerdtid);
    //alert(index);
    let confirm = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'ลบข้อมูล?',
      message: 'ท่านยืนยันการลบเลขที่เอกสารนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            console.log('Agree clicked');
            /* let loader = this.loadingCtrl.create({
              content: "กรุณารอสักครู่..."
            });
 
            loader.present(); */
            // ทำการแสดงข้อมูล 
            this.httpClient.get(BASEURL + 'OrderNow/Delete_itemproduct.php?sellerdtid=' + sellerdtid)
              .subscribe(data => {
                // loader.dismissAll();
                if (data != null) {
                  this.loadcart();
                }
                else {
                }
              }, error => {
                // loader.dismissAll();
                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    confirm.present();
    slidingItem.close();
  }
  async refreshItemServer(sellerdtid: any) {

    let confirm = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'รีเฟรชสินค้านี้?',
      message: 'ท่านยืนยันรีเฟรชสินค้านี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            console.log('Agree clicked');
            /* let loader = this.loadingCtrl.create({
              content: "กรุณารอสักครู่..."
            });
 
            loader.present(); */
            // ทำการแสดงข้อมูล 
            let link = BASEURL + 'OrderNow/refresh_itemproduct.php';
            let myData = {
              branchcode: this.newbranch,
              employeecode: this.employeecode,
              strbranch: this.strbranch,
              sellerdtid: sellerdtid,
              count: 1,
            };
            this.httpClient.post(link, JSON.stringify(myData))
              .subscribe(data => {
                // loader.dismissAll();
                if (data != null) {
                  this.loadcart();
                }
                else {
                }
              }, error => {
                // loader.dismissAll();
                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    confirm.present();

  }


  addServer(item: any) {

    if (item.stockqty > item.orderqty) {
      let link = BASEURL + 'OrderNow/Addsub_itemproduct.php';
      let myData = {
        branchcode: this.newbranch,
        employeecode: this.employeecode,
        strbranch: this.strbranch,
        sellerdtid: item.sellerdtid,
        count: 1,
        typeadd_sub: 1 //ทำการเพิ่ม item
      };

      this.httpClient.post(link, JSON.stringify(myData))
        .subscribe(data => {
          console.log("Add data:" + data);

          //loader.dismissAll();
          if (data != null) {
            this.loadcart();
          } else {
          }

        }, error => {
          //loader.dismissAll();
          //console.log(JSON.stringify(error.json()));
        });

    }

  }

  subServer(item: any) {

    if (item.orderqty > 1) {

      let link = BASEURL + 'OrderNow/Addsub_itemproduct.php';
      let myData = {
        branchcode: this.newbranch,
        employeecode: this.employeecode,
        strbranch: this.strbranch,
        sellerdtid: item.sellerdtid,
        count: 1,
        typeadd_sub: 2 //ทำการลด item
      };

      this.httpClient.post(link, JSON.stringify(myData))
        .subscribe(data => {
          //loader.dismissAll();
          console.log("Sub data:" + data);
          if (data != null) {
            this.loadcart();
          } else {
          }

        }, error => {
          //loader.dismissAll();
          //console.log(JSON.stringify(error.json()));
        });

    }

  }

  async itemCountProductServer(item: any) {

    let prompt = await this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      header: 'กรอกจำนวนสินค้า',
      message: "",
      inputs: [
        {
          type: 'number',
          name: 'countitem',
          placeholder: '0',
          max: item.stockbalance,
        },
      ],
      buttons: [
        /* {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }, */
        {
          text: 'ตกลง',
          handler: async (data) => {
            console.log('stockbalance', item.stockqty - item.orderqty, item);
            if (data.countitem != '') {
              if ((item.stockqty - item.orderqty) >= (parseInt(data.countitem))) {
                console.log('countitem', (parseInt(data.countitem)));
                if ((parseInt(data.countitem)) > 0) {
                  this.additem = (parseInt(data.countitem));
                  console.log('additem', this.additem);
                  this.itemCountServer(this.additem, item);
                }
              }
              else {
                let prompt = await this.alertCtrl.create({
                  cssClass: 'alertCustomCss',
                  header: 'จำนวนที่กรอกมากไป',
                  message: "จำนวนเยอะกว่าสต๊อคที่มีอยู่ สต็อคที่มี : " + (item.stockqty - item.orderqty),
                  buttons: [
                    {
                      text: 'ตกลง',
                      handler: data => {
                      }
                    },
                  ]
                });
                prompt.present();
                setTimeout(() => {
                  prompt.dismiss();
                }, 3000);
              }
            }
          }
        }
      ]
    });
    prompt.present();


  }
  itemCountServer(countitem: any, item: any) {

    //this.count = this.count + 1;

    this.additem = countitem;

    let link = BASEURL + 'OrderNow/Addsubkey_itemproduct.php';
    let myData = {
      branchcode: this.newbranch,
      employeecode: this.employeecode,
      strbranch: this.strbranch,
      sellerdtid: item.sellerdtid,
      count: this.additem,
      //typeadd_sub: 2 //ทำการลด item
    };

    this.httpClient.post(link, JSON.stringify(myData))
      .subscribe(data => {
        //loader.dismissAll();
        console.log("Sub data:" + data);
        if (data != null) {
          this.loadcart();
        } else {
        }

      }, error => {
        //loader.dismissAll();
        //console.log(JSON.stringify(error.json()));
      });



  }



}
