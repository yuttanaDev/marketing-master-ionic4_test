import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ToastService } from './../services/toast.service';
import { StorageService } from './../services/storage.service';
import { AuthConstants } from '../config/auth-constants';
import { AuthService } from './../services/auth.service';
import { HttpService } from './../services/http.service';
@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  public version_app: any = '0.5.1';
  public version: any;

  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';

  public items: any = [];
  public linkurl: any = URLIMG;
  public posts: any;
  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public isShown = true;

  public datacount: any = {};
  public posts2: any;
  public searchQuery: string = '';
  public items2: any = [];

  checkdata: any;
  public branch: any;
  public typeadmin: any = 0;

  public isLogedin: any = 0;
  public remember: any = '';
  type_admin: any = 0;

  myDate: any;
  public authUser: any = [];

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public datepipe: DatePipe,
    public storage: Storage,
    private router: Router,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public alertController: AlertController,
    public alertCtrl: AlertController,
    private iab: InAppBrowser,
    public platform: Platform,
    private auth: AuthService,
    public toastService: ToastService,
    private storageService: StorageService,
    private httpService: HttpService
  ) {

    let dateString = new Date();
    let date = new Date(dateString.getFullYear(), dateString.getMonth(), 1);

    let dateString2 = new Date();
    let date2 = new Date(dateString2.getFullYear(), dateString2.getMonth(), dateString2.getDate());

    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');


    this.updateApp();


  }
  async ngOnInit() {

    const ret = await this.storageService.get(AuthConstants.AUTH);
    this.authUser = ret[0];
    console.log("xxx" ,  this.authUser);
    if (ret.length > 0) {
      this.newbranch = this.authUser['branchcode'];
      console.log('newbranch', this.newbranch);
      this.employeecode = this.authUser['username'];
      this.type_admin = this.authUser['type_admin'];
      this.namebranch = this.authUser['branchname'];
      this.datacount.namebranch = this.namebranch;
      if (this.type_admin == 0 || this.type_admin == null) {
        this.teamdoc();
        console.log("team");
      } else if (this.type_admin == 1) {
        this.teamadmin();
        console.log("teamadmin");
      } else if (this.type_admin == 2) {
        this.teamadmin();
        console.log("teamadmin");
      } else {
        this.teamadmin();
        console.log("teamadmin");
      }

    }
    //this.authUser = await this.auth.getUserData();
    //console.log('authUser', this.authUser);

    //this.feedData(this.pageNumber);


  }

  async getAuth() {
    this.isShown = true;
    // this.items = [];
    // this.posts = [];
    const ret = await this.storageService.get(AuthConstants.AUTH);
    this.authUser = ret[0];
    if (ret.length  > 0) {
      this.newbranch = this.authUser['branchcode'];
      console.log('newbranch', this.newbranch);
      this.employeecode = this.authUser['username'];
      this.type_admin = this.authUser['type_admin'];
      this.namebranch = this.authUser['branchname'];
      this.datacount.namebranch = this.namebranch;
      if (this.type_admin == 0 || this.type_admin == null) {
        this.teamdoc();
        console.log("team");
      } else if (this.type_admin == 1) {
        this.teamadmin();
        console.log("teamadmin");
      } else if (this.type_admin == 2) {
        this.teamadmin();
        console.log("teamadmin");
      } else {
        this.teamadmin();
        console.log("teamadmin");
      }

    }

  }

  updateApp() {
    const link = 'https://global-house.co.th/AppStore/Api/detail.php';
    const myData = {
      tokenapi: "EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD",
      appid: '8',
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        console.log('detail', data);
        if (data != null) {
          this.version = data[0].app_version;
          if (this.version_app == this.version) {

          } else {

            this.showConfirm();
          }

        } else {

        }
      }, error => {
        //console.log(JSON.stringify(error.json()));
      });
  }

  async showConfirm() {
    const alert = await this.alertController.create({
      header: 'UPDATE NOW??',
      message: 'Your current version of the app is outdated. Want to update?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Update',
          handler: () => {
            console.log('Agree clicked');
            this.platform.ready().then(() => {
              const browser = this.iab.create('https://www.globalhouse.co.th/AppStore/#/detail?appid=8', "_system");
            });
          }
        }
      ]
    });
    alert.present();
  }

  presentConfirm() {

    let date = new Date(this.datacount.datefirst);
    let date2 = new Date(this.datacount.dateend);
    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    if (this.type_admin == 0 || this.type_admin == null) {
      this.items = null;
      this.teamdoc();
      console.log("team");
    } else {
      this.items2 = null;
      this.teamadmin();
      console.log("teamadmin");
    }


  }

  async teamdoc() {

    this.isShown = true;
   // let link = API_URL + "suvey_doc_team2.php?";

    let postData = {
      username: this.employeecode,
      datefirst: this.datacount.datefirst,
      dateend: this.datacount.dateend,
    };
    this.httpService.post('survey/suvey-doc', postData).subscribe(
      (res: any) => { 
        if(res.status === 200){
          this.posts = res.data;
          this.initializeItems2();
          this.isShown = false;
        }else{
          this.isShown = false;
          this.toastService.presentToast(res.subTitle);
        }
      },
      (error: any) => {
        this.isShown = false;
        this.toastService.presentToast('Network Issue.');
      }
    );
    setTimeout(() => {
      this.isShown = false;
    }, 7000);

    /* this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        console.log("team", data);
        this.posts = data;
        this.initializeItems();
        this.isShown = false;
      }, error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      });
    setTimeout(() => {
      this.isShown = false;
    }, 7000); */

  }
 async teamadmin() {
    this.isShown = true;
    console.log('employeecode', this.employeecode, this.newbranch, this.datacount.datefirst, this.datacount.dateend);
    let link = API_URL + "suvey_doc_teamadmin_test2.php?";
    //this.showLoading();

    let postData = {
      username: this.employeecode,
      datefirst: this.datacount.datefirst,
      dateend: this.datacount.dateend,
      branchcode: this.newbranch,
    };
    this.httpService.post('survey/suvey-docadmin', postData).subscribe(
      (res: any) => { 
        if(res.status === 200){
          this.posts2 = res.data;
          console.log('this.posts2',this.posts2)
          this.initializeItems2();
          this.isShown = false;
        }else{
          this.isShown = false;
          this.toastService.presentToast(res.subTitle);
        }
      },
      (error: any) => {
        this.isShown = false;
        this.toastService.presentToast('Network Issue.');
      }
    );
    setTimeout(() => {
      this.isShown = false;
    }, 7000);

    /* this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        this.posts2 = data;
        this.initializeItems2();

        this.isShown = false;
      }, error => {
        this.isShown = false;
        console.log(JSON.stringify(error.json()));
      });

    setTimeout(() => {
      this.isShown = false;
    }, 7000); */
  }

  async openModal() {
    let modal = await this.modalController.create({
      component: DismissModalPage,
      componentProps: {
        //barCode: couponCode,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        // this.datacount.namebranch = data.namebranch;
        // this.newbranch = data.branchcode;
        // this.storage.set('namebranch', data.namebranch);
        // this.storage.set('newbranch', data.branchcode);

        let branchcode = data.branchcode;
        let namebranch = data.namebranch;
  
        var auth = [{
          branchcode: branchcode,
          branchname: namebranch,
          branchshorten: this.authUser.branchshorten,
          divisioncode: this.authUser.divisioncode,
          firstname: this.authUser.firstname,
          fullname: this.authUser.fullname,
          lastname: this.authUser.lastname,
          positioncode: this.authUser.positioncode,
          positionname: this.authUser.positionname,
          sectioncode: this.authUser.sectioncode,
          sectionname: this.authUser.sectionname,
          username: this.authUser.username,
          userpassword: this.authUser.userpassword,
          type_admin : this.authUser.type_admin,
          type_admin_branch : this.authUser.type_admin_branch,
        }];
  
        this.storageService
          .store(AuthConstants.AUTH, auth)
          .then(async res => {
            //const ret = await this.storageService.get(AuthConstants.AUTH);
            //console.log('storageService', ret);
            this.getAuth();
          });

        if (this.type_admin == 0 || this.type_admin == null) {
          this.items = null;
          this.teamdoc();
          console.log("team");
        } else {
          this.items2 = null;
          this.teamadmin();
          console.log("teamadmin");
        }

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }

  itemTapped(survey_doc_no: any, branchcode: any, branchname: any) {

    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: survey_doc_no,
        branchcode: branchcode,
        branchname: branchname
      }
    };
    this.router.navigate(['home'], navigationExtras);

  }

  openNotify() {

  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.isShown = true;
    setTimeout(() => {
      console.log('Async operation has ended');
      if (this.type_admin == 0 || this.type_admin == null) {
        this.items = null;
        this.teamdoc();
        console.log("team");
      } else {
        this.items2 = null;
        this.teamadmin();
        console.log("teamadmin");
      }
      event.target.complete();
    }, 1500);
  }
  doRefresh2() {
    this.isShown = true;
    setTimeout(() => {
      if (this.type_admin == 0 || this.type_admin == null) {
        this.items = null;
        this.teamdoc();
        console.log("team");
      } else {
        this.items2 = null;
        this.teamadmin();
        console.log("teamadmin");
      }
    }, 1500);
  }




  initializeItems() {
    this.items = this.posts;
    console.log("data items" + this.items, this.posts);
  }
  initializeItems2() {
    this.items2 = this.posts2;
    console.log("data3!" + this.items2);
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.docuno.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }



}
