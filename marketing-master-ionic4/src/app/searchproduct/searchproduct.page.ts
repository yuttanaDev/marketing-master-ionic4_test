import { Component, OnInit,ViewChild  } from '@angular/core';
import { NavController,AlertController,LoadingController} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { debounceTime } from "rxjs/operators";
import { FormBuilder, FormGroup , Validators , FormControl } from "@angular/forms";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { IonSearchbar,IonContent} from '@ionic/angular';
import { BASEURL, URLIMG, API_URL,systemsetting } from '../../providers/constants/constants';

@Component({
  selector: 'app-searchproduct',
  templateUrl: './searchproduct.page.html',
  styleUrls: ['./searchproduct.page.scss'],
})
export class SearchproductPage {
  @ViewChild(IonSearchbar,{ static: false }) searchbar: IonSearchbar;
  
  public searchControl: FormControl;
  items: any = [];
  itemsProduct: any = [];
  itemsincat: any = [];

  txtSearchAll: string = "";
  loader: any;

  public showToolbar: any;

  base_url: any = systemsetting.base_url;
  token: any = systemsetting.token;

  constructor(
    public navCtrl: NavController,
    private http: HttpClient,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public router: Router
  ) {
    this.searchControl = new FormControl();

    this.items = [
      { pro_name_new: "ประตู" },
      { pro_name_new: "หน้าต่าง" },
      { pro_name_new: "ไม้อัด" },
      { pro_name_new: "เครื่องปรับอากาศ" },
      { pro_name_new: "เหล็กเส้น" },
      { pro_name_new: "เครื่องทำน้ำอุ่น" },
      { pro_name_new: "ทีวี" }
    ];
  }
  ionViewWillEnter() {
    setTimeout(() => { this.searchbar.setFocus(); }, 150);
  }

  


  setFilteredItems() {
    this.searchControl.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(search => {
        console.log(search);
        if (search != "") {
          this.txtSearchAll = search;
          this.searchProductincat(search);
        } else {
          this.items = [
            { pro_name_new: "ประตู" },
            { pro_name_new: "หน้าต่าง" },
            { pro_name_new: "ไม้อัด" },
            { pro_name_new: "เครื่องปรับอากาศ" },
            { pro_name_new: "เหล็กเส้น" },
            { pro_name_new: "เครื่องทำน้ำอุ่น" },
            { pro_name_new: "ทีวี" }
          ];
        }
      });
  }

  searchProductincat(text: string) {
    var link = this.base_url + "Searchcat";
    var loginData = JSON.stringify({
      textsearch: this.txtSearchAll, //  Textsearch
      token: this.token
    });

    this.http.post(link, loginData).subscribe(
      data => {
        this.itemsincat = data;

        this.searchProductlist(this.txtSearchAll);
      },
      error => {
        //this.loader.dismiss();
        console.log("Oooops!");
        this.alertPopup(
          "ล้มเหลว",
          "ไม่สามารถเชื่อมต่อ ระบบได้ ลองใหม่อีกครั้ง"
        );
      }
    );
  }

  searchProductlist(text: string) {
    var link = this.base_url + "search";
    var loginData = JSON.stringify({
      textsearch: this.txtSearchAll, //  Textsearch
      token: this.token
    });

    this.http.post(link, loginData).subscribe(
      data => {
        //console.log(data);
        this.itemsProduct = data;
        //this.loader.dismiss();
      },
      error => {
        //this.loader.dismiss();
        console.log("Oooops!");
        this.alertPopup(
          "ล้มเหลว",
          "ไม่สามารถเชื่อมต่อ ระบบได้ ลองใหม่อีกครั้ง"
        );
      }
    );
  }

  gotoGridsearch(txt: string, groupId: number, id: any) {
    let strsearch: string = "";
    if (id == 1) {
      strsearch = this.txtSearchAll;
    } else if (id == 2) {
      strsearch = this.txtSearchAll;
    } else if (id == 3) {
      strsearch = txt;
    } else if (id == 4) {
      strsearch = txt;
    } else {
     // strsearch = this.txtSearchAll;
    }
    console.log("gotoGridsearch!",txt,strsearch,id,groupId);
    /* this.navCtrl.push(CatalogGridPage, {
      txtSearch: strsearch,
      txtgroupId: groupId,
      searchactive: true
    }); */

    let navigationExtras: NavigationExtras = {
      state: {
        txtSearch: strsearch,
        txtgroupId: groupId,
        searchactive: true
      }
    };
    this.router.navigate(["catalog-grid"], navigationExtras);
    //console.log(txt);
  }

  backButton(){
    this.navCtrl.back();
  }

  /* gotoProductDetail(sku: string) {
    console.log(sku);
    //this.navCtrl.push(ProductDetailsPage, { set_barcode: sku });
    let navigationExtras: NavigationExtras = {
      state: {
        set_barcode: sku
      }
    };
    this.navCtrl.navigateForward(["tabs/shop/product-details/" + sku] );
  } */

  gotoProductDetail(item) {

    console.log('product_barcode', item);

    let navigationExtras: NavigationExtras = {
      state: {
        product_item:item,
      }
    };
    this.router.navigate(['product'], navigationExtras);

  }

  async alertPopup(title: string, Msg: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      message: Msg,
      buttons: ["OK"]
    });
    await alert.present();
  }

  /* // loading popup
  private popupLoading() {
    this.loader = this.loadingCtrl.create({
      message:
        '<div class="custom-spinner-container"><div class="custom-spinner-box"></div>กำลังโหลด...</div>',
      duration: 5000
    });
    this.loader.present();
    //loader.dismiss();
  }

  private popupDismiss() {
    this.loader.present().then(() => {
      this.loadingCtrl.dismiss();
    });
  } */
}
