import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SearchproductPage } from './searchproduct.page';

const routes: Routes = [
  {
    path: '',
    component: SearchproductPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchproductPage],

})
export class SearchproductPageModule {}
