import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localData$ = new BehaviorSubject<any>([]);
  constructor(public storage: Storage) {
  }

  changeLocalData(data: any) {
    this.localData$.next(data);
  }
  deleteLocalData(msgIndex: number) {
    let data = [];
    let currentFeedData = this.getCurrentFeedData();
    currentFeedData.splice(msgIndex, 1);
    let newFeedUpdateData = data.concat(currentFeedData);
    this.changeLocalData(newFeedUpdateData);
  }
  clearLocalData() {
    let data = [];
    this.changeLocalData(data);
  }
  getCurrentFeedData() {
    return this.localData$.getValue();
  }

  // Store the value
  async store(storageKey: string, value: any) {
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await localStorage.setItem(storageKey, encryptedValue);
  }
  // Get the value
  async get(storageKey: string) {
    const ret = await localStorage.getItem(storageKey);
    if (ret) {
      return JSON.parse(unescape(atob(ret)));
    } else {
      return false;
    }
  }

  // JSON "get" example
  async getObject() {
    //const ret = await this.storage.get({ key: 'user' });
   // const user = JSON.parse(ret.value);
  }

  async setItem() {
    /* await this.storage.set({
      key: 'name',
      value: 'Max'
    }); */
  }

  async keys() {
    const keys = await this.storage.keys();
    //console.log('Got keys: ', keys);
  }

  async clearKey(storageKey: string) {
    await localStorage.removeItem(storageKey);
  }

  async clear() {
    await localStorage.clear();
  }
}
