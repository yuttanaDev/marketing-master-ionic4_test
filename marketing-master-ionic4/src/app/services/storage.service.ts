import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor(public storage: Storage) {
  }

  // Store the value
  async store(storageKey: string, value: any) {
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await this.storage.set(storageKey, {
      id: storageKey,
      value: encryptedValue
    });

  }
  // Get the value
  async get(storageKey: string) {
    const ret = await this.storage.get(storageKey);
    if (ret) {
      return JSON.parse(unescape(atob(ret.value)));
    } else {
      return false;
    }
  }

  // JSON "get" example
  async getObject() {
    //const ret = await this.storage.get({ key: 'user' });
   // const user = JSON.parse(ret.value);
  }

  async setItem() {
    /* await this.storage.set({
      key: 'name',
      value: 'Max'
    }); */
  }

  async keys() {
    const keys = await this.storage.keys();
    console.log('Got keys: ', keys);
  }

  async clear() {
    await this.storage.clear();
  }
}
