import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { environment2 } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  headers = new HttpHeaders({
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1MTQyNDQiLCJpYXQiOjE1ODU1NjMxOTk3OTN9.XEvPPWTuq6H-Oly5hT58cWCkI5eGoaTPgMls6eAOZwE',
    'Content-Type': 'application/json',
    });
  options = { headers: this.headers };

  constructor(private http: HttpClient) {}

  post(serviceName: string, data: any) {
    const url = environment2.apiUrl+ 'api/' + serviceName;
    return this.http.post(url, JSON.stringify(data), this.options);
  }
  get(serviceName: string, data: any) {
    const url = environment2.apiUrl+ 'api/' + serviceName;
    return this.http.get(url, this.options);
  }
}
