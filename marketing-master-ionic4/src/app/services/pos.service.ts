import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class PosService {
  posData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  changePosData(data: any) {
    this.posData$.next(data);
  }

  getCurrentPosData() {
    return this.posData$.getValue();
  }

  updatePosData(newFeed: any) {
    let data = [];
    data.push(newFeed);
    let currentFeedData = this.getCurrentPosData();
    let newFeedUpdateData = data.concat(currentFeedData);
    this.changePosData(newFeedUpdateData);
  }


  deletePosData(msgIndex: number) {
    let data = [];
    let currentFeedData = this.getCurrentPosData();
    currentFeedData.splice(msgIndex, 1);
    let newFeedUpdateData = data.concat(currentFeedData);
    this.changePosData(newFeedUpdateData);
  }

  posData(postData: any): Observable<any> {
    return this.httpService.post('pos/creatbilling', postData);
  }

  posDelete(postData: any): Observable<any> {
    return this.httpService.post('feedDelete', postData);
  }

  posUpdate(postData: any): Observable<any> {
    return this.httpService.post('feedUpdate', postData);
  }
}
