import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class startbillService {
  userData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  getData(postData: any): Observable<any> {
    return this.httpService.post('survey/checkneedle2', postData);
  }

  getkimgcar(postData: any): Observable<any> {
    return this.httpService.post('survey/checkimgcar', postData);
  }

  getdata(postData: any): Observable<any> {
    return this.httpService.post('survey/checkimgmile', postData);
  }



}
