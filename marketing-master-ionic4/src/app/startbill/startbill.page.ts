import { startbillService } from './startbill.service';
import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { DismissModalnormalPage } from '../dismiss-modalnormal/dismiss-modalnormal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {  URLIMG, API_URL } from '../../providers/constants/constants';
import { from } from 'rxjs';
// import { resolve } from 'path';

@Component({
  selector: 'app-startbill',
  templateUrl: './startbill.page.html',
  styleUrls: ['./startbill.page.scss'],
})
export class StartbillPage implements OnInit {

  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';
  datacount: any;
  pictureimg: any;
  docno: any;
  employeecode: any;
  newbranch: any;
  private empname: any;
  survey_incout = null;
  needle: any;

  cucumber: boolean = false;
  public survaycar: boolean = false;

  public survey_imgmile: any;
  public survey_imgcar: any ;
  public survey_doc_no: any;

  photos: any=[];
  base64Image: any=[];
  photos2: any=[];
  base64Image2: any=[];

  public linkimg: any = API_URL+'Uploaded/';
  public linkimg2: any = API_URL+'Uploaded/';

  public isShown = true;
  public doc_no: any;
  public branchcode: any;
  public branchname: any;

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    private camera: Camera,
    public alertCtrl: AlertController,
    public startbillService : startbillService,
    ) { 

      this.datacount = {};



    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.doc_no;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        //โชว์รายชื่อคนออกสำรวจ
        // this.imgcarshow();
        this.imgcarshow();
        this.imgmile();
        // this.checkmile();
        this.checkmile();
      }
    });

    }

    // fucntion checkmile_new
    checkmile(){
    //set value data
    let myData = {"docno" : this.docno};
    // console.log(" myData", myData);
    //GET DATA NODEJS API SERVICE
    this.startbillService.getData(myData).subscribe(
      (res : any) => {
        // console.log("response data",res);
        if(res != null){

          this.datacount.poquantity = res[0]['survey_incout'];
          this.datacount.carname = res[0]['survay_carname'];
          this.datacount.id_car = res[0]['survay_carcode'];
          this.survey_doc_no = res[0]['survey_doc_no'];
          let car = res[0]['survay_car'];
          if (car == 't') {
            this.survaycar = true;
            console.log("survaycar1" , this.survaycar);
          } if (car == 'f') {
            this.survaycar = false;
            console.log("survaycar2" , this.survaycar);
          }
          // this.needle = data;
          this.isShown = false;

        }else{
          this.isShown = false;
        }
      }, error => {
        this.isShown = false;
        console.log("error!");
      });

    }


    // checkmile() {
    //   //เช็คปิดป้อนเข็มไมล์

    //   this.http.get(API_URL+'check_needle2.php?docno=' + this.docno)
    //     .subscribe(data => {
    //       // loader3.dismissAll();
    //       // console.log("checkmile!" , data);
    //       if (data != null) {

    //         this.datacount.poquantity = data[0]['survey_incout'];
    //         this.datacount.carname = data[0]['survay_carname'];
    //         this.datacount.id_car = data[0]['survay_carcode'];
    //         this.survey_doc_no = data[0]['survey_doc_no'];
    //         let car = data[0]['survay_car'];
    //         if (car == 't') {
    //           this.survaycar = true;
    //           console.log("survaycar1" , this.survaycar);
    //         } if (car == 'f') {
    //           this.survaycar = false;
    //           console.log("survaycar2" , this.survaycar);
    //         }
    //         // this.needle = data;
    //         this.isShown = false;
    //       } else {
            
    //         this.isShown = false;
    //       }
    //     }, error => {
    //       this.isShown = false;
    //       console.log("error!");
    //       //  loader3.dismissAll();
    //     });
    // }

    imgcarshow(){ //เช็คปิดป้อนเข็มไมล์ NODEJS API DATA
      let myData = {"docno" : this.docno};
      this.startbillService.getkimgcar(myData).subscribe(
        (res : any) => {
          if(res != null){
            this.survey_imgcar = res[0]['survey_img'];
          }else{
           
          }
        }, error => {
          console.log("error!");
        });
    }


    // imgcarshow() {
    //   //เช็คปิดป้อนเข็มไมล์
  
    //   this.http.get(API_URL+'check_img_car.php?docno=' + this.docno)
    //     .subscribe(data => {
    //       console.log("imgcarshow!" , data);
    //       if (data != null) {
            
    //         //this.survey_incout = data[0]['survey_incout']; 
    //         this.survey_imgcar = data[0]['survey_img'];
    //         //this.survey_doc_no = data[0]['survey_doc_no'];
    //        // console.log("survey_img!",this.linkimg+this.img_car);
    //       } else {
    //         //this.img_car = null;
    //       }
    //     }, error => {
    //       console.log("error!");
    //     });
    // }

    imgmile() {
      //เช็คปิดป้อนเข็มไมล์

      let myData = {"docno" : this.docno};
      this.startbillService.getdata(myData).subscribe(
        (res : any) => {
          if(res != null){
          console.log("data res node ",res);
          this.survey_imgmile = res[0]['survey_img'];
          }else{
           
          }
        }, error => {
          console.log("error!");
        });
  
      // this.http.get(API_URL+'check_img_mile1.php?docno=' + this.docno)
      //   .subscribe(data => {
        
      //     if (data != null) {
      //       console.log("imgmile! PHP" , data);
      //       //this.survey_incout = data[0]['survey_incout']; 
      //       this.survey_imgmile = data[0]['survey_img'];
      //       //this.survey_doc_no = data[0]['survey_doc_no'];
  
      //       // this.needle = data;
  
      //     } else {
  
      //     }
      //   }, error => {
      //     console.log("error!");

      //   });
  
    }

  ngOnInit() {
  }

  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'md-images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          icon: 'md-camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    var option = {
      quality: 75,
      targetWidth: 960,
      targetHeight: 690,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE

    };

    this.camera.getPicture(option).then((imageData) => {

      if (sourceType == 0) {

          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();
  
          const myData = {
            docno: this.docno,
            employee: this.employeecode, 
            branch: this.newbranch ,
            photos:this.photos
          };
          this.http.post(API_URL+'upload_inbase.php', JSON.stringify(myData))
            .subscribe(data => {
               console.log('detail', data);
              
              if (data != null) {
               
              this.showalert("อัพโหลดประสบความสำเร็จ");
              this.pictureimg = data;
              //this.imgcar();
              this.imgmile()
              this.photos=[];
      
              } else {
      
              }
            }, error => {
             
              this.showalert("ไม่สำเร็จ");
            });

      } else {

        
          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();
  
          
          const myData = {
            docno: this.docno,
            employee: this.employeecode, 
            branch: this.newbranch,
            photos:this.photos
          };
          this.http.post(API_URL+'upload_inbase.php', JSON.stringify(myData))
            .subscribe(data => {
               console.log('detail', data);
              
              if (data != null) {
               
              this.showalert("อัพโหลดประสบความสำเร็จ");
              this.pictureimg = data;
              //this.imgcar();
              this.imgmile();
              this.photos=[];
      
              } else {
      
              }
            }, error => {
             
              this.showalert("ไม่สำเร็จ");
            });

      }

    }, (err) => {
      this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
      // alert(JSON.stringify(err));
    });

  }

  async presentActionSheetcar() {
    let actionSheetcar = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'md-images',
          handler: () => {
            this.takePicturecar(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          icon: 'md-camera',
          handler: () => {
            this.takePicturecar(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheetcar.present();
  }
  takePicturecar(sourceType) {
    var option = {
      quality: 75,
      targetWidth: 960,
      targetHeight: 690,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(option).then((imageData) => {


      if (sourceType == 0) {
        this.base64Image2 = "data:image/jpeg;base64," + imageData;
        this.photos2.push(this.base64Image2);
        this.photos2.reverse();

        const myData = {
          docno: this.docno,
          employee: this.employeecode, 
          branch: this.newbranch,
          photos:this.photos2
        };
        this.http.post(API_URL+'upload_carbase.php', JSON.stringify(myData))
          .subscribe(data => {
             console.log('detail', data);
            if (data != null) {
             
            this.showalert("อัพโหลดประสบความสำเร็จ");
            this.pictureimg = data;
            this.imgcarshow();
            //this.imgmile()

            this.photos2=[];
    
            } else {
    
            }
          }, error => {

            this.showalert("ไม่สำเร็จ");
          });

      } else {

        this.base64Image2 = "data:image/jpeg;base64," + imageData;
        this.photos2.push(this.base64Image2);
        this.photos2.reverse();

        const myData = {
          docno: this.docno,
          employee: this.employeecode, 
          branch: this.newbranch,
          photos:this.photos2
        };
        this.http.post(API_URL+'upload_carbase.php', JSON.stringify(myData))
          .subscribe(data => {
             console.log('detail', data);
            if (data != null) {
             
            this.showalert("อัพโหลดประสบความสำเร็จ");
            this.pictureimg = data;
            this.imgcarshow();
            //this.imgmile();
            this.photos2=[];
    
            } else {
    
            }
          }, error => {
            this.showalert("ไม่สำเร็จ");
          });
      }

    }, (err) => {
      this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
      //alert(JSON.stringify(err));
    });

  }

  async openModal() {

    let modal = await this.modalController.create({
      component: DismissModalnormalPage,
      componentProps: {
        doc_no: this.docno,
        branchcode: this.newbranch,
        branchname: this.branchname,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      console.log('DismissModalnormalPage',data);
      if (data.dismissed == true) {

        
      }else{

        this.datacount.id_car = data.item.id_car;
        this.datacount.carname = data.item.character_car + ' ' + data.item.car_number + ' ' +  data.item.province_car;

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }


  async showalert(text) {
    let alert = await this.alertCtrl.create({
      header: 'การบันทึก',
      message: text,
      buttons: ['ตกลง']
    });
    alert.present();
  }



 async showConfirm() {
    let confirm = await this.alertCtrl.create({
      header: 'ท่านต้องการบันทึก?',
      message: 'ท่านต้องการบันทึกข้อมูลหรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Agree clicked');
            this.isShown = true;
            this.saveproduct()
          }
        }
      ]
    });
    confirm.present();
  }

  async alertPopup(title: string, Msg: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      message: Msg,
      buttons: ['ตกลง']
    })
    await alert.present();
  }

  private saveproduct() {


    if (this.datacount.carname == ""  && this.datacount.id_car == "" || this.datacount.carname  == null && this.datacount.id_car == null ) {
      this.alertPopup('เลือกทะเบียนรถ', 'กรุณาเลือกทะเบียนรถ ที่ออกสำรวจ');
      return;
    }

    let poquantity = this.datacount.poquantity;
    let carname = this.datacount.carname;
    let carcode = this.datacount.id_car;
    if(carcode == undefined)
    {
      carcode = 0;
    }
    let pay = this.cucumber;
    let survaycar = this.survaycar;
    console.log("pay", pay);

    //this.http.get(API_URL+'addcount_in.php?poquantity='
    //  + poquantity + '&' + 'docno=' + this.docno + '&' + 'employee=' + this.employeecode + '&' + 'branch=' + this.newbranch + '&' + 'pay=' + pay + '&' + 'carname=' + carname + '&' + 'survaycar=' + survaycar)
    let myData = {
      poquantity: poquantity,
      employee: this.employeecode,
      pay: pay,
      carname: carname,
      survaycar: this.survaycar,
      docno: this.docno,
      branch: this.newbranch,
      carcode: carcode
      //branchname: this.branchname,
    }

    console.log("myData", myData);
    let link = API_URL + "addcount_in2.php?";
    this.http.post(link, JSON.stringify(myData))
    .subscribe(
        data => {
          if(data['status'] === 200){

            this.showalert(data['ms']);
            console.log("addcount_in2", data['ms2']);

          }else if(data['status'] === 401){
            this.showalert(data['ms']);
            console.log("addcount_in2", data['ms2']);
          }else {
            this.showalert(data['ms']);
            console.log("addcount_in2", data['ms2']);
          }
          
          this.isShown = false;
        }, error => {
          //console.log(JSON.stringify(error.json()));
          this.showalert('ไม่สามารถบันทึกได้');
          this.isShown = false;
        });

  }


  doRefresh() {
    this.isShown = true;
    //this.showemployyee();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }


}
