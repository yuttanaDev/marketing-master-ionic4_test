import { OnInit } from '@angular/core';
import { Component, Injector, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';
import { Device } from '@ionic-native/device/ngx';
//UPDATE App Auto ionic 4
import { AppVersion } from '@ionic-native/app-version/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { LoginService } from './login.service';
import { AuthService } from './../services/auth.service';
import { ToastService } from './../services/toast.service';
import { StorageService } from './../services/storage.service';
import { AuthConstants } from '../config/auth-constants';

import { ApiServiceProvider } from '../../providers/api-service/api-service';
const linkqr = 'https://globalhouse.co.th/AppStore/Api/Install/';

import { API_URL } from '../../providers/constants/constants';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  data: any;
  newbranch: any;
  namebranch: any;
  employeecode: any;
  password: any;
  public remember: any = '';
  private isLogedin: any = 0;
  type_admin: any;
  public token: any = null;

  public typecus: any = [];
  public typecusitem: any = [];

  public appname: any;
  public install: any;
  public version: any;
  public url_link: any;
  public linkqrcode: string = null;
  public progress: any;
  public uploaded: any;
  public totalBytes: any;
  sectioncode: any;
  sectionname: any;
  positionname: any;
  positioncode: any;

  appName: any;
  packageName: any;
  versionNumber: any;
  versionCode: any;

  constructor(
    private navCtrl: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public storage: Storage,
    public platform: Platform,
    private iab: InAppBrowser,
    private router: Router,
    private route: ActivatedRoute,
    public apiServiceProvider: ApiServiceProvider,
    private device: Device,
    public loadingController: LoadingController,
    private http: HttpClient,
    private fcm: FCM,
    public appVersion: AppVersion,
    public webIntent: WebIntent,
    public androidPermissions: AndroidPermissions,
    public loginSerive: LoginService,
    public toastService: ToastService,
    private storageService: StorageService,
  ) {

    this.platform = platform;
    this.customertype();
    this.appVersion.getAppName().then(version => {
      this.appName = version;
    });
    this.appVersion.getPackageName().then(version => {
      this.packageName = version;
    });
    this.appVersion.getVersionNumber().then(version => {
      this.versionNumber = version;
    });
    this.appVersion.getVersionCode().then(version => {
      this.versionCode = version;
    });

    this.data = {};



    this.fcm.subscribeToTopic('ScanbillLoss');
    this.fcm.getToken().then(token => {
      this.token = token;
      console.log(token);
    });

   


    this.storage.get('remember').then((remember) => {
      this.remember = remember;
      console.log('remember3' + this.remember);

      if (this.remember == 1) {
        this.storage.get('type_admin').then((type_admin) => {
          this.type_admin = type_admin;

          //this.router.navigate(['/list']); 
         // this.navCtrl.navigateRoot('/list');
          //his.navCtrl.goForward(menuOption);

        });
        console.log('remember3' + this.remember);

      } else if (this.remember == 0) {
        //alert('ไม่มี');
      }
    });



  }

  public customertype() {

    this.apiServiceProvider.typeLocation().then(response => {
      if (response != null) {

        localStorage.setItem('typecus1', this.typecus)
        console.log('Insert typecus1:', this.typecus);

        localStorage.setItem('typecus', JSON.stringify(response))
        console.log('Insert typecus:', response);
      } else {
      }

    }, err => {

    });

    this.apiServiceProvider.subtypeLocation().then(response => {
      if (response != null) {

        localStorage.setItem('typesubcus', this.typecus)
        console.log('Insert typesubcus:', this.typecus);

        localStorage.setItem('typesubcus', JSON.stringify(response))
        console.log('Insert typesubcus:', response);
      } else {
      }

    }, err => {

    });
    this.apiServiceProvider.subtypeGlobalclub().then(response => {
      if (response != null) {

        // this.items = data['items_type'];
        // this.itemssub = data['items_sub'];
        localStorage.setItem('items_type', this.typecus)
        console.log('Insert items_type:', this.typecus);

        localStorage.setItem('items_type', JSON.stringify(response))
        console.log('Insert items_type:', response);
      } else {
      }

    }, err => {

    });
    this.apiServiceProvider.masterCategory().then(response => {
      if (response != null) {

        localStorage.setItem('items_category', this.typecus)
        console.log('Insert items_category:', this.typecus);

        localStorage.setItem('items_category', JSON.stringify(response))
        console.log('Insert items_category:', response);
      } else {
      }

    }, err => {

    });
    this.apiServiceProvider.masterGroup().then(response => {
      if (response != null) {

        localStorage.setItem('items_group', this.typecus)
        console.log('Insert items_group:', this.typecus);

        localStorage.setItem('items_group', JSON.stringify(response))
        console.log('Insert items_group:', response);
      } else {
      }

    }, err => {

    });
    this.apiServiceProvider.masterPattern().then(response => {
      if (response != null) {

        localStorage.setItem('items_pattern', this.typecus)
        console.log('Insert items_pattern:', this.typecus);

        localStorage.setItem('items_pattern', JSON.stringify(response))
        console.log('Insert items_pattern:', response);
      } else {
      }

    }, err => {

    });
    this.apiServiceProvider.masterDelivery().then(response => {
      if (response != null) {

        localStorage.setItem('items_delivery', this.typecus)
        console.log('Insert items_delivery:', this.typecus);

        localStorage.setItem('items_delivery', JSON.stringify(response))
        console.log('Insert items_delivery:', response);
      } else {
      }

    }, err => {

    });



  }


  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }


  async login() {
    let username = this.data.username;
    let password = this.data.password;


    let loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 2000
    });
    await loading.present();

    //let link = API_URL + "loginadmin.php?";
    //this.showLoading();
    let myData = {
      username: username,
      userpass: password,
    };

    this.loginSerive.loginData(myData).subscribe(
      (res: any) => { 
        console.log('res : ', res.data);
        if(res.status === 200){
          this.storageService
              .store(AuthConstants.AUTH, res.data)
              .then(async res => {
                //push notify
                this.fcm.getToken().then(token => {
                  this.token = token;
                  if (this.platform.is('android')) {
                    //this.user_fcmnotify();
                  }
                });
                this.navCtrl.navigateRoot('/list');
                const ret = await this.storageService.get(AuthConstants.AUTH);
                console.log('storageService',ret);
              });
        }else{
          this.toastService.presentToast(res.subTitle);
        }
      },
      (error: any) => {
        this.toastService.presentToast('Network Issue.');
      }
    );
  }

  user_fcmnotify() {

    let link = API_URL + "save_user_fcmnotify.php?";
    //this.showLoading();
    let myData = {
      package_name: 'com.globalhouse.exploredmarket',
      employeecode: this.employeecode,
      token: this.token,
      branchcode: this.newbranch,
      appname: 'สำรวจตลาด',
      appversion: '0.5.1',
      platform: this.device.platform,
      serial: this.device.serial,
      model: this.device.model,
      uuid: this.device.uuid,
      manufacturer: this.device.manufacturer,
      version: this.device.version,
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {

      },
        error => {

        })
  }

  async noProductAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่พบรหัสใช้งาน',
      subHeader: 'ไม่พบรหัสใช้งาน, หรือรหัสผ่านไม่ถูกต้อง',
      //buttons: ['OK']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }
  async serverAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้',
      subHeader: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้, หรือให้ทำการแสกนใหม่อีกครั้ง!',
      //buttons: ['OK']
    });

  }

  ngOnInit() {
  }

}
