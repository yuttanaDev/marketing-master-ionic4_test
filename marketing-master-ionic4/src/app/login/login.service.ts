import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  changeLoginData(data: any) {
    this.userData$.next(data);
  }

  getCurrentLoginData() {
    return this.userData$.getValue();
  }

  updateLoginData(newFeed: any) {
    let data = [];
    data.push(newFeed);
    let currentFeedData = this.getCurrentLoginData();
    let newFeedUpdateData = data.concat(currentFeedData);
    this.changeLoginData(newFeedUpdateData);
  }


  deleteLoginData(msgIndex: number) {
    let data = [];
    let currentFeedData = this.getCurrentLoginData();
    currentFeedData.splice(msgIndex, 1);
    let newFeedUpdateData = data.concat(currentFeedData);
    this.changeLoginData(newFeedUpdateData);
  }

  loginData(postData: any): Observable<any> {
    return this.httpService.post('survey/login', postData);
  }

  loginDelete(postData: any): Observable<any> {
    return this.httpService.post('feedDelete', postData);
  }

  loginUpdate(postData: any): Observable<any> {
    return this.httpService.post('feedUpdate', postData);
  }
}
