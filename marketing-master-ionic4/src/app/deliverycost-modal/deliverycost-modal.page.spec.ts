import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverycostModalPage } from './deliverycost-modal.page';

describe('DeliverycostModalPage', () => {
  let component: DeliverycostModalPage;
  let fixture: ComponentFixture<DeliverycostModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverycostModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverycostModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
