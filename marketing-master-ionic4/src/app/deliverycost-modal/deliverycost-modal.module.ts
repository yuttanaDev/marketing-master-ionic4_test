import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DeliverycostModalPage } from './deliverycost-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DeliverycostModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DeliverycostModalPage]
})
export class DeliverycostModalPageModule {}
