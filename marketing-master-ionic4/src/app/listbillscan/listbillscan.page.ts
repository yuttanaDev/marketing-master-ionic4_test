import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissRatingsPage } from '../dismiss-ratings/dismiss-ratings.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { BASEURL, URLIMG } from '../../providers/constants/constants';



@Component({
  selector: 'app-listbillscan',
  templateUrl: 'listbillscan.page.html',
  styleUrls: ['listbillscan.page.scss']
})
export class ListbillscanPage implements OnInit {

  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';

  options: BarcodeScannerOptions;
  product: any;
  public isShown = true;
  public imglink: any = URLIMG;
  public typekong: any;
  private posts: any;
  private searchQuery: string = '';
  private chacknumer: string = '';
  private items: any;
  private itemcheck: any;
  private itemscheckdc: any;
  private categories: any;
  public month_sale: any;
  public year_sale: any;
  public editid: any = 0;

  public newbranch: any;
  checkdata: any;
  public employeecode: any;
  public itemproduct: any;
  public itemsproduct: any = [];
  public namebranch: any;
  public branch: any;
  private setting: any;
  public replacement = '0';

  private isLogedin: any = 0;
  public remember: any = '';
  public showqrcode: any;

  public barcode: any;
  public idtype: any;
  public gencode: any;
  public approve: boolean;
  private idedit: any;
  private genbillcode: any;

  selectdocuno: any;

  private sellerid: any;
  public docuno: any;
  private branchcode: any;
  private docustatus: any;
  private approvedata: any;
  private lastImage: string = null;
  checklength: any = 0;
  public productcode: any;
  private codeData: any = null;
  public detailItem: any;
  public detailItem2: any;
  public detailItem1: any;
  public detailItem3: any;
  public dataimage: any;
  public abc: any;
  public docudate: any;
  product_name1: any;
  product_code: any;
  product_unit_rate: any;
  minstock: any;
  maxstock: any;
  stockdc: any;
  nostockdc: any;

  datacount: any;
  datefirst: any;
  dateend: any;
  myDate: any;
  linkurl: any;
  iddocuno: any = null;
  type_docuno: any;
  docno: any;
  typeitems: any;
  public BASEURL: any;
  signInType: string;

  pic: any;
  cate_id: any;
  box_id: any;
  public itemspro: any = [];
  barchcode: any;

  branch_product_pro_id: any;
  pro_id: any;
  product_name: any;
  stockqty: any;
  gradebybranch: any;
  active: any;
  datesavestock: any;
  saleamntyear: any;
  yearnow: any;
  barcode2: any;
  linkimg: any;
  pic2: any;
  idmanu: any;
  public stockdccheck = null;
  public brchcode: any;
  clearance_id: any;
  boxclearance_id: any;
  pattern_papersale_id: any;
  list_no: any;
  clearance_img_id: any;
  brchcode2: any;
  barchcode222: any;
  capt_img: any;
  remark: any = '';
  public branchshorten: any;

  numbermenu: any = 2;
  showcate_id: any;
  showcate_name: any;
  cate: any = '';
  box: any = '';
  postsbox: any;
  postscat: any;
  reason: any;

  cate_id_edit: any;
  docuno_edit: any;
  box_id_edit: any;
  qrcode_edit: any;
  running_id: any = null;
  branch_code: any;

  public scannedText: string;
  public buttonText: string;
  public loadingscan: boolean;

  public selectedCategory: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;

  public photos: any = null;
  public base64Image: string;
  public itemsimg: any = [];

  typepage: any;

  constructor(
    private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    private camera: Camera,
    private barcodeScanner: BarcodeScanner,
    public alertCtrl: AlertController,
  ) {
    this.typekong = '1';
    this.datacount = {};
    this.product = {
      scanData: '',
    }

    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
      this.datacount.namebranch = namebranch;
    });

    this.photos = [];
    this.itemsimg = [];

    /*  this.running_id = this.navParams.get('running_id');
     if(this.running_id != null ){

     this.cate_id_edit = this.navParams.get('cate_id');
     this.docuno_edit = this.navParams.get('docuno');
     this.box_id_edit = this.navParams.get('box_id');
     this.qrcode_edit = this.navParams.get('qrcode'); 
     this.branch_code = this.navParams.get('branch_code');
    // this.showImageItem();

     } */

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.typepage = this.router.getCurrentNavigation().extras.state.type;
        this.running_id = this.router.getCurrentNavigation().extras.state.running_id;

        if (this.running_id != null) {

          this.cate_id_edit = this.router.getCurrentNavigation().extras.state.cate_id;
          this.docuno_edit = this.router.getCurrentNavigation().extras.state.docuno;
          this.box_id_edit = this.router.getCurrentNavigation().extras.state.box_id;
          this.qrcode_edit = this.router.getCurrentNavigation().extras.state.qrcode;
          this.branch_code = this.router.getCurrentNavigation().extras.state.branch_code;

          this.showImageItem();


        }


      }
    });

    this.storage.get('branchshorten').then((branchshorten) => {
      this.branchshorten = branchshorten;
    });
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
    });
    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
    });
    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
    });
    this.showCat_id();

  }

  showImageItem() {


    let link = BASEURL + 'showimage_kongpro_item.php?';
    let myData = {
      cate_id_edit: this.cate_id_edit,
      docuno_edit: this.docuno_edit,
      box_id_edit: this.box_id_edit,
      qrcode_edit: this.qrcode_edit,
      branch_code: this.branch_code
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data3 => {

        if (data3 != null) {

          this.detailItem1 = data3['items'];
          this.editid = data3['editid'];
          this.itemsimg = data3['image_items'];
          this.cate = data3['cate_id'];
          this.datacount.box_id = data3['box_id'];
          this.datacount.remark = data3['remark'];
          this.itemspro = data3['items'];
          this.barchcode = data3['barchcode'];
          this.barcode = data3['qrcode'];
          this.reason = data3['capt_img'];
          this.selectdocuno = data3['docuno'];
          this.typekong = data3['typekong'];
          
          console.log("detailItem1 1", data3);
          // loader.dismissAll();
        } else {
          // this.pic2 = null;
          // console.log("image 2", this.dataimage);
          //this.showdetail(this.codeData);
          //loader.dismissAll();
        }

      }, error => {
        console.log("image 3", this.dataimage);

      })

    setTimeout(() => {

    }, 10000);

  }

  scanbar() {

    this.options = {
      prompt: "แสกนรหัสสินค้า"
    }
    this.barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData === null) {
        return;
      }
      let barcode = barcodeData.text;
      let productCode: any = barcode.substring(barcode.lastIndexOf('/') + 1, barcode.length);
      this.prefillFromDB(productCode);
    }, (err) => {
      console.log("Error occured : " + err);
    });
  }
  scanbar2() {
    this.options = {
      prompt: "แสกนรหัสสินค้า"
    }
    this.barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData === null) {
        return;
      }
      let barcode = barcodeData.text;
      let productCode: any = barcode.substring(barcode.lastIndexOf('/') + 1, barcode.length);
      this.prefillFromDBData(productCode);

    }, (err) => {
      console.log("Error occured : " + err);
      //alert(err);
    });

  }
  searchForTerm() {
    let productCode: string = this.product.scanData.substring(this.product.scanData.lastIndexOf('/') + 1, this.product.scanData.length);
    this.prefillFromDB(productCode);
    // alert(productCode);
    console.log("items", productCode);
    // this.prefillFromDB(this.product.scanData);
  }


  searchForTerData() {
    let productCode: string = this.product.scanData2.substring(this.product.scanData2.lastIndexOf('/') + 1, this.product.scanData2.length);
    this.prefillFromDBData(productCode);
    // alert(productCode);
    console.log("items", productCode);
    // this.prefillFromDB(this.product.scanData);
  }
  prefillFromDB(barcodeDataText) {
    //this.cleardata();
    this.barcode = barcodeDataText;
    console.log("barcode", this.barcode);
    // this.showImage(this.barcode);
  }


  prefillFromDBData(barcodeDataText) {
    this.barcode2 = barcodeDataText;
    console.log("barcode", this.barcode2);
    //this.showdetail2(this.barcode2);

  }

  async deletePhoto(index) {
    let confirm = await this.alertCtrl.create({
      header: "Sure you want to delete this photo? There is NO undo!",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  async deletePhotoOnServer(index) {
    let confirm = await this.alertCtrl.create({
      header: "Sure you want to delete this photo? There is NO undo!",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked", index);
            const sentApi = {
              running_id: index
            };

            this.http.post(BASEURL + 'delect_img_kongpro.php', JSON.stringify(sentApi))
              .subscribe(data => {
                console.log("Delete Success" , data['status']);

                if (data['status'] === 200) {
                  this.showImageItem();
                }else{

                }
              }, error => {
                console.log("Delete error " , error);
                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    await confirm.present();
  }

  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePhoto(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePhoto(sourceType) {
    console.log("coming here");

    const options = {
      quality: 85,
      targetWidth: 960,
      targetHeight: 690,
      correctOrientation: true,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(
      imageData => {

        if (sourceType == 0) {

          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();

        } else {

          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.photos.push(this.base64Image);
          this.photos.reverse();

        }

      },
      err => {
        console.log(err);
      }
    );
  }

  async showBox_id(cate) {
    console.log('delete data', cate, this.newbranch);

    let link = BASEURL + 'showDocno.php?';
    let myData = {
      pro_id: cate,
      newbranch: this.newbranch,
      /*     QRcode: this.codeData,
          idmanu: this.idmanu */
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        if (data != null) {
          this.postsbox = data;
          //console.log("box data", data);

        } else {
          console.log("box no data", data);
        }
      },
        error => {

        })

    setTimeout(() => {

    }, 10000);
  }

  async showCat_id() {
    let link = BASEURL + 'showCat_id.php?';
    let myData = {
      /*     QRcode: this.codeData,
          idmanu: this.idmanu */
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {
        if (data != null) {
          this.postscat = data;
          //console.log("cat data", data);
          this.isShown = false;
        } else {
          console.log("cat no data", data);
          this.isShown = false;
        }

      },
        error => {
          this.isShown = false;
        })

    setTimeout(() => {
      this.isShown = false;
    }, 10000);
  }

  optionsFnCat() {
    // this.cate = cate;
    this.cate;
    console.log("cate_id", this.cate);

    this.showBox_id(this.cate);
  }
  optionsFnBox() {
    // this.box = box;
    this.box;
    console.log("box", this.box);
  }

  async showConfirm() {

    const alert = await this.alertCtrl.create({
      header: 'บันทึกข้อมูล?',
      message: 'ท่านยืนยันการการบันทึกนี้หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'บันทึก',
          handler: () => {

            this.uploadImageKongpro();
          }
        }
      ]
    });

    await alert.present();

  }

  async uploadImageKongpro() {


    console.log('editid', this.editid);

    this.presentLoading();

    let link = BASEURL + 'uploadimagekongpro_item.php';
    let myData = {
      boxcode: this.box,
      getEmpcode: this.employeecode,
      newbranch: this.newbranch,
      cat_id: this.cate,
      brachCode: this.barchcode,
      empcode: this.employeecode,
      caption: this.reason,
      qrcode: this.barcode,
      idmanu: this.idmanu,
      boxclearance_id: this.boxclearance_id,
      clearance_img_id: this.clearance_img_id,
      clearance_id: this.clearance_id,
      branchshorten: this.branchshorten,
      brchcode: this.brchcode,
      cate: this.cate,
      box: this.box,
      docuno: this.docuno,
      box_id: this.datacount.box_id,
      cate_id: this.datacount.cate_id,
      docno: this.datacount.docuno,
      qr_code: this.datacount.qrcode,
      dateapprvend: this.datacount.dateapprvend,
      remark: this.datacount.remark,
      sentPhotos: this.photos,
      sentItemspro: this.itemspro,
      editid: this.editid,
      barchcode: this.barchcode,
      barcode: this.barcode,
      reason: this.reason,
      typekong: this.typekong,

    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {

        //loading.dismiss();

        if (data['status'] === 200) {
          this.showAlert(data['status_mess']);
          this.nav.back();

        } else if (data['status'] === 500) {
          this.showAlert(data['status_mess']);
        }

      }, Error => {
        //loading.dismiss();

      });


  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'รอสักครู่',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async openModal() {
    let modal = await this.modalController.create({
      component: DismissRatingsPage,
      componentProps: {
        selectdocuno: this.selectdocuno,
        newbranch: this.newbranch,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        let box_id = data.box_id;
        let cate_id = data.cate_id;
        let docuno = data.docuno;
        let qrcode = data.qrcode;
        let dateapprvend = data.dateapprvend;
        let remark = data.remark;

        this.datacount.box_id = box_id;
        this.datacount.cate_id = cate_id;
        this.datacount.docuno = docuno;
        this.datacount.qrcode = qrcode;
        this.datacount.dateapprvend = dateapprvend;
        this.datacount.remark = remark;

        console.log("dateapprvend", this.datacount.dateapprvend);

      } else {
        let box_id = '';
        let cate_id = '';
        console.log("Supplier2", cate_id, box_id);
      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });
  }
  async showAlert(data:any){
    let alert = await this.alertCtrl.create({
      header: 'อัพโหลดข้อมูล?',
      message:  data,
      buttons: ['ตกลง']
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 2500);
  }




  doRefresh() {

    this.posts = [];
    this.items = [];
    setTimeout(() => {
      this.showImageItem();
    }, 1500);
  }


  clickToHome(item: any) {

    /* this.nav.push(HomePage, {
      idtype: typeorderid,
      gencode: docuno,
      idedit: sellerid,
    }); */

    this.router.navigate(["/listbillscan"], { queryParams: { handh_docuno: item.handh_docuno, handh_branchcode: item.handh_branchcode, countadd: item.countadd, countkey: item.countkey }, skipLocationChange: true });
    //this.router.navigate(["/home", item]);
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
