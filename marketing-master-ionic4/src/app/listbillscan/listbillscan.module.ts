import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';

import { ListbillscanPage } from './listbillscan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    NgxIonicImageViewerModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListbillscanPage
      }
    ])
  ],
  providers: [
   
  ],
  declarations: [ListbillscanPage]
})
export class ListbillscanPageModule {}
