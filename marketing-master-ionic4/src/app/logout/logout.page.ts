import { OnInit } from '@angular/core';
import { Component, Injector, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';

//UPDATE App Auto ionic 4
import { AppVersion } from '@ionic-native/app-version/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

const linkqr = 'https://globalhouse.co.th/AppStore/Api/Install/';

import { API_URL } from '../../providers/constants/constants';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  data: any;
  newbranch: any;
  namebranch: any;
  employeecode: any;
  password: any;
  public remember: any = '';
  private isLogedin: any = 0;
  type_admin: any;

  public token: any = null;

  public appname: any;
  public install: any;
  public version: any;
  public url_link: any;
  public linkqrcode: string = null;
  public progress: any;
  public uploaded: any;
  public totalBytes: any;

  appName: any;
  packageName: any;
  versionNumber: any;
  versionCode: any;
  sectioncode: any;
  sectionname: any;
  positionname: any;
  positioncode: any;

  constructor(
    private navCtrl: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public storage: Storage,
    public platform: Platform,
    private iab: InAppBrowser,
    private router: Router,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    private http: HttpClient,
    private fcm: FCM,
    public appVersion: AppVersion,
    public webIntent: WebIntent,
    public androidPermissions: AndroidPermissions,
  ) {


    this.storage.clear().then(() => {
      console.log('Keys have been cleared');
    });

    this.platform = platform;

    this.data = {};

    this.fcm.subscribeToTopic('ScanbillLoss');
    this.fcm.getToken().then(token => {
      this.token = token;
      console.log(token);
    });

  }

  

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }

  async showConfirm() {

    const alert = await this.alertController.create({
      header: 'UPDATE NOW??',
      message: 'แอพเวอร์ชั่นปัจจุบันของคุณล้าสมัยแลัว ต้องการอัพเดทหรือไม่??',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'อัพเดท',
          handler: () => {
            console.log('Agree clicked');
            this.platform.ready().then(() => {
              const browser = this.iab.create('https://www.globalhouse.co.th/AppStore/#/detail?appid=7', "_system");
            });
          }
        }
      ]
    });

    await alert.present();

  }

  async login() {
    let username = this.data.username;
    let password = this.data.password;


    let loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 2000
    });
    await loading.present();

    let link = API_URL + "loginadmin.php?";
    //this.showLoading();
    let myData = {
      getUser: username,
      getPass: password,
    };
    this.http.post(link, JSON.stringify(myData))
      //this.http.get( link + 'username=' + '0355042601' + '&' + 'password=' + '0355042601' )
      .subscribe(data => {

        loading.dismiss();

        if (data != null) {

          this.newbranch = data['newbranch'];
          this.namebranch = data['branchnamesubstr'];
          this.employeecode = data['employeecode'];
          this.password = data['password'];
          this.type_admin = data['type_admin'];
          this.sectioncode = data['sectioncode'];
          this.sectionname = data['sectionname'];
          this.positioncode = data['positioncode'];
          this.positionname = data['positionname'];

          console.log('type_admin login', data,this.type_admin);
          this.storage.set('namebranch', this.namebranch);
          this.storage.set('remember', 1);
          this.storage.set('newbranch', this.newbranch);
          this.storage.set('employeecode', this.employeecode);
          this.storage.set('password', this.password);
          this.storage.set('type_admin', this.type_admin);
          this.storage.set('sectioncode', this.sectioncode);
          this.storage.set('sectionname', this.sectionname);
          this.storage.set('positionname', this.positionname);

          //push notify
          this.fcm.getToken().then(token => {
            this.token = token;
            if (this.platform.is('android')) {
              this.user_fcmnotify();
            }
          });


          this.navCtrl.navigateRoot('/list');

        } else {

          this.noProductAlert();


        }

      },
        error => {

          loading.dismiss();
          this.serverAlert();

        })
  }

  user_fcmnotify() {

    let link = API_URL + "ScanbillLoss/save_user_fcmnotify.php?";
    //this.showLoading();
    let myData = {
      package_name: 'com.globalhouse.cyclemobile',
      employeecode: this.employeecode,
      token: this.token,
      branchcode: this.newbranch,
      appname: 'Cycle Mobile',
      appversion: '0.1.8',
    };
    this.http.post(link, JSON.stringify(myData))
      .subscribe(data => {

      },
        error => {

        })
  }

  async noProductAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่พบรหัสใช้งาน',
      subHeader: 'ไม่พบรหัสใช้งาน, หรือรหัสผ่านไม่ถูกต้อง',
      //buttons: ['OK']
    });

    await alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 3000);
  }
  async serverAlert() {
    const alert = await this.alertController.create({
      header: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้',
      subHeader: 'ไม่สามารถเชื่อมต่อเซริฟเวอร์ได้, หรือให้ทำการแสกนใหม่อีกครั้ง!',
      //buttons: ['OK']
    });

  }


  ngOnInit() {
  }

}
