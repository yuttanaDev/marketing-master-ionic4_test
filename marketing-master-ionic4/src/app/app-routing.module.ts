import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { HomeGuard } from './guards/home.guard';
import { UserDataResolver } from './resolvers/user-data.resolver';
const routes: Routes = [
  {
    path: '',
    canActivate: [HomeGuard],
    resolve: {
      userData: UserDataResolver
    },
    pathMatch: "full",
    loadChildren: "./login/login.module#LoginPageModule"
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule',

  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  }, {
    path: 'dismiss-ratings',
    loadChildren: './dismiss-ratings/dismiss-ratings.module#DismissRatingsPageModule'
  },
  {
    path: 'listbill',
    loadChildren: './listbill/listbill.module#ListbillPageModule'
  },
  {
    path: 'listbillscan',
    loadChildren: './listbillscan/listbillscan.module#ListbillscanPageModule'
  },
  /*{
   path: 'homepos',
   loadChildren: './homepos/homepos.module#HomeposPageModule'
 },
 {
   path: 'cartnormal',
   loadChildren: './cartnormal/cartnormal.module#CartnormalPageModule'
 },
 {
   path: 'cart',
   loadChildren: './cart/cart.module#CartPageModule'
 }, */
  {
    path: 'dismiss-modal',
    loadChildren: './dismiss-modal/dismiss-modal.module#DismissModalPageModule'
  },
  {
    path: 'dismiss-modalnormal',
    loadChildren: './dismiss-modalnormal/dismiss-modalnormal.module#DismissModalnormalPageModule'
  },
  {
    path: 'logout',
    loadChildren: './logout/logout.module#LogoutPageModule'
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule'
  },
  { path: 'timepurchase', loadChildren: './timepurchase/timepurchase.module#TimepurchasePageModule' },
  { path: 'second', loadChildren: './second/second.module#SecondPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
  { path: 'nameteam', loadChildren: './nameteam/nameteam.module#NameteamPageModule' },
  { path: 'startbill', loadChildren: './startbill/startbill.module#StartbillPageModule' },
  { path: 'endbill', loadChildren: './endbill/endbill.module#EndbillPageModule' },
  { path: 'addlistlocation', loadChildren: './addlistlocation/addlistlocation.module#AddlistlocationPageModule' },
  { path: 'listcustomer', loadChildren: './listcustomer/listcustomer.module#ListcustomerPageModule' },
  { path: 'addlocation', loadChildren: './addlocation/addlocation.module#AddlocationPageModule' },
  { path: 'globalclub', loadChildren: './globalclub/globalclub.module#GlobalclubPageModule' },
  { path: 'listreport', loadChildren: './listreport/listreport.module#ListreportPageModule' },
  { path: 'selectprovice', loadChildren: './selectprovice/selectprovice.module#SelectprovicePageModule' },
  { path: 'listorder', loadChildren: './listorder/listorder.module#ListorderPageModule' },
  { path: 'orderhome', loadChildren: './orderhome/orderhome.module#OrderhomePageModule' },
  { path: 'deliverycost-modal', loadChildren: './deliverycost-modal/deliverycost-modal.module#DeliverycostModalPageModule' },
  { path: 'ordercategory', loadChildren: './ordercategory/ordercategory.module#OrdercategoryPageModule' },
  { path: 'product', loadChildren: './product/product.module#ProductPageModule' },
  { path: 'provincemodal', loadChildren: './provincemodal/provincemodal.module#ProvincemodalPageModule' },
  { path: 'searchproduct', loadChildren: './searchproduct/searchproduct.module#SearchproductPageModule' },
  { path: 'catalog-grid', loadChildren: './catalog-grid/catalog-grid.module#CatalogGridPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
