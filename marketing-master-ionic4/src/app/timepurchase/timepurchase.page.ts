import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { LoadingController, Platform, NavController, MenuController, Events } from '@ionic/angular';


import { API_URL } from '../../providers/constants/constants';

@Component({
  selector: 'app-timepurchase',
  templateUrl: './timepurchase.page.html',
  styleUrls: ['./timepurchase.page.scss'],
})
export class TimepurchasePage {


  public newbranch: any;
  public employeecode: any;
  public datacount: any = {};

  public items: any;
  public posts: any;
  public namebranch: any;

  title = 'angular-datatables';
  rows = [];
  isShown = true;
  constructor(public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    public router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public http: HttpClient) {


    let dateString = new Date();
    let date = new Date(dateString.getFullYear(), dateString.getMonth(), dateString.getDate());

    let dateString2 = new Date();
    let date2 = new Date(dateString2.getFullYear(), dateString2.getMonth(), dateString2.getDate());

    this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');

    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
    });
    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        this.loadreporttime();
      });

    });

  }




  async loadreporttime() {
    

    let myData = {  
      sentbranch: this.newbranch,
      datefirst: this.datacount.datefirst,
      dateend: this.datacount.dateend,
      tokenapi:'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
    };

    this.http.post(API_URL + 'CompetitorSurvey/select_location_product_rival.php', JSON.stringify(myData))
      .subscribe(data => {

        console.log('load time', data);
        
        //this.posts2 = data;
        // this.items = data; 
        if(data != null){

          this.posts = data;
          this.initializeItems();
          console.log(this.posts);
          setTimeout(() => {
            this.isShown = false;
          }, 1000);

        }else{

          setTimeout(() => {
            this.isShown = false;
          }, 1000);
        }




      }, error => {
        setTimeout(() => {
          this.isShown = false;
        }, 1000);
        //console.log(JSON.stringify(error.json()));
      });

  }
  initializeItems() {
    this.items = this.posts;
   }
   getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.product_code.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  presentConfirm() {
    this.loadreporttime();
        
  }

  doRefresh() {
    console.log('Begin async operation');
    // this.listNormal();
    setTimeout(() => {
      console.log('Async operation has ended');
      //event.target.complete();
    }, 5000);
  }

  ngOnInit() {
  }

}
