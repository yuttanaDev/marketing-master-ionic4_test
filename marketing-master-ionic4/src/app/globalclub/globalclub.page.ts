import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController, ToastController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { URLIMG, API_URL } from '../../providers/constants/constants';
import { ProvincemodalPage } from '../provincemodal/provincemodal.page';

import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from './../services/http.service';
declare var google;
declare var AdvancedGeolocation: any;

class Province {
  public province_id: number;
  public province_name: string;
}
class Amphur {
  public amphur_id: number;
  public amphur_name: string;
}
class District {
  public district_id: number;
  public district_name: string;
}

const BASEURL = API_URL;
const BASEURLIMG = API_URL + 'ImgGlobalclub/';


@Component({
  selector: 'app-globalclub',
  templateUrl: './globalclub.page.html',
  styleUrls: ['./globalclub.page.scss'],
})
export class GlobalclubPage implements OnInit {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;

  ports: Province[];
  // port: Port;

  public defaultImage = 'assets/imgs/preloader.png';
  public imageToShowOnError = 'assets/imgs/preloader.png';

  province: Province[];
  amphur: Amphur[];
  district: District[];

  public base64Image: string;

  itemstitle_name: any;

  customer_barcode: any = null;
  flag_newcus: any;
  fullname: any;
  branch_code: any;
  //full_address : any;
  //identification_card : any;
  //mobile  : any;

  public datalength: any = '';
  public datalength2: any = '';

  latLng: any;
  coords: any = {};
  datacount: any;
  pictureimg: any;
  public docno: any;
  public val: any;
  data: any = {};
  id_suvey: any;
  id: any;
  ideditcus: any;
  maxid: any;
  cardid: any;
  idedit = null;
  imgupload: any;
  employeecode: any;
  customertype: any;
  idedittype: any;
  typecus: any;
  subtypecus: any;
  catid: any;
  newbranch: any;
  branchname: any;

  firstname: any;
  lastname: any;
  identification_card: any;
  mobile: any;
  full_address: any;
  email: any;
  age: any;
  public items: any;
  public itemssub: any;
  datacustomer: any = null;

  public approve: boolean;
  public registerglobalclub: any;
  public approveglobalclub: any;


  // dataSource: Object;
  // chartConfig: Object;


  images: any = null;
  cutomer_img: any;

  public photos: any = null;

  public imglink: any = BASEURLIMG;

  // images: Array<string>;  
  grid: Array<Array<string>>; //array of arrays

  myDate: any;
  maxdate: string;
  gbh_customer_id: any;
  idcard: any;

  public identity: boolean = false;

  genders: Array<string>;

  public width: any = '100%';
  public height: any = '50%';
  public type: any = 'Column2D';
  public dataFormat: any = 'json';
  public dataSource: any;

  public items_data: any = [];
  public items_score: any = [];

  public typeid: any = [];

  public score_net: any = 0;
  public score_pay: any = 0;
  public balance_score: any = 0;

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public toastCtrl: ToastController,
    private camera: Camera,
    public alertCtrl: AlertController,
    public HttpService : HttpService, ) {

    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.doc_no;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.datacount.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        this.customer_barcode = this.router.getCurrentNavigation().extras.state.customer_barcode;

        this.ideditcus = this.router.getCurrentNavigation().extras.state.ideditcus;
        this.gbh_customer_id = this.router.getCurrentNavigation().extras.state.gbh_customer_id;
        this.branch_code = this.router.getCurrentNavigation().extras.state.branchcode;
        this.idcard = this.router.getCurrentNavigation().extras.state.identification_card;

        console.log('docno 123', this.docno);

        //โชว์รายชื่อคนออกสำรวจ
        //this.loadData();
        if (this.gbh_customer_id == null || this.gbh_customer_id == '' || this.gbh_customer_id == 'undefined') {
          console.log('I am an customer_barcode! NULL', this.gbh_customer_id);
          this.loadMap();
        } else {
          this.addEditid();
          console.log('I am an customer_barcode!', this.gbh_customer_id);
        }
      }
    });

    this.datacount = {};

    this.photos = [];
    this.datacount.province_id = 0;
    this.datacount.amphur_id = 0;
    this.datacount.housenumber = '';
    this.datacount.village = '';

    this.datacount.title_name = 'นาย';
    this.datacount.customer_type_globalclub = 1016;
    //this.datacount.membership_type_globalclub = 1016;
    this.datacount.sex = 'ชาย';

    // this.datacount.membership_type_globalclub = ["10", "11"];

    this.approve = true;
    this.registerglobalclub = 1;
    this.approveglobalclub = 'false';

    let typeitem = JSON.parse(localStorage.getItem('items_type'));
    console.log("items_type", typeitem);
    this.items = typeitem['items_type'];
    this.itemssub = typeitem['items_sub'];

    this.myDate = new Date();
    let date2 = new Date(this.myDate.getFullYear() + 543, this.myDate.getMonth(), this.myDate.getDate());
    this.datacount.birthday = this.datepipe.transform(date2, 'yyyy-MM-dd');

    let maxdate5: string = this.datepipe.transform(date2, 'yyyy');

    this.maxdate = maxdate5;

    //this.customer_barcode = navParams.get('customer_barcode');

    //this.datacount.birthday2 =this.datepipe.transform(this.datacount.birthday, 'yyyyMMdd'); 
    console.log("birthday" + this.maxdate, 'day', maxdate5);

    
    this.titleName();



  }

  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);
    let length = selectedValue.detail.value;
    if (length.length == 13) {
      console.log(" No1 length!", length.length);
      this.datalength = '';
    } else if (length.length < 13) {
      this.datalength = 'เลขบัตรประจำตัวประชาชนของคุณน้อยกว่า 13 หลัก!';
      console.log(" No2 length!", length.length);
    } else if (length.length > 13) {
      this.datalength = 'เลขบัตรประจำตัวประชาชนของคุณมากกว่า 13 หลัก!';
      console.log(" No3 length!", length.length);
    } else {
      this.datalength = 'กรุณากรอกเลขบัตรประจำตัวประชาชนให้ครบ 13 หลัก!';
    }

  }
  onSelectChange2(selectedValue: any) {
    console.log('Selected', selectedValue);
    let length = selectedValue.detail.value;
    if (length.length == 10) {
      console.log(" No1 length!", length.length);
      this.datalength2 = '';
    } else if (length.length < 10) {
      this.datalength2 = 'เบอร์โทรศัพท์น้อยกว่า 10 หลัก!';
      console.log(" No2 length!", length.length);
    } else if (length.length > 10) {
      this.datalength2 = 'บอร์โทรศัพท์มากกว่า 10 หลัก!';
      console.log(" No3 length!", length.length);
    } else {
      this.datalength2 = 'กรุณากรอกเบอร์โทรศัพท์ครบ 10 หลัก!';
    }

  }

  titleName() {
    // this.http.get(BASEURL + 'checkcustomer/title_name_globalclub.php')
    let myData = {data:""};
    this.HttpService.post("survey/titlenameglobalclub",myData).subscribe(data => {
        // console.log("typeglobalclub!",data);
        if (data != null) {
          this.itemstitle_name = data;
          console.log("typeglobalclub!", this.items);
        } else {
          console.log("typeglobalclub NULL", data);
        }
      }, error => {
        console.log("error!");
      });
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }


  onChangehousenumber(housenumber) {
    console.log(housenumber);
    this.datacount.full_address = this.datacount.housenumber;
  }

  onChangepostcode(postcode) {
    console.log(postcode);
    if (this.datacount.province_id == 1) {
      this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + this.datacount.district_name + this.datacount.amphur_name + this.datacount.province_name + ' ' + this.datacount.postcode;
    } else {
      this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + ' ตำบล' + this.datacount.district_name + 'อำเภอ' + this.datacount.amphur_name + 'จังหวัด' + this.datacount.province_name + '' + this.datacount.postcode;
    }
  }

  

  async openModalBranch(){

    let modal = await this.modalController.create({
      component: DismissModalPage,
      componentProps: {
        //barCode: couponCode,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        let branchcode = data.branchcode;
        let namebranch = data.namebranch;

        this.newbranch = branchcode;
        this.datacount.branchname = namebranch;


        console.log("Supplier", branchcode, namebranch);

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });

  }

  async openModalProvince() {

    let modal = await this.modalController.create({
      component: ProvincemodalPage,
      componentProps: {
        typepage: 1,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        let province_id = data.province_id;
        let province_name = data.province_name;
        let region_id = data.region_id;

        this.datacount.province_id = province_id;
        this.datacount.province_name = province_name;
        this.datacount.region_id = region_id;

        console.log("province_name", province_id, province_name);

        if (this.datacount.province_id == 1) {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + this.datacount.province_name;
        } else {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + 'จังหวัด' + this.datacount.province_name;
        }

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });


  }

  async openModalAmphur() {

    let modal = await this.modalController.create({
      component: ProvincemodalPage,
      componentProps: {
        typepage: 2,
        province_id :this.datacount.province_id,
        amphur_id:0
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        let amphur_id = data.amphur_id;
        let amphur_name = data.amphur_name;
        let province_id = data.province_id;

        this.datacount.amphur_id = amphur_id;
        this.datacount.amphur_name = amphur_name;
        this.datacount.province_id = province_id;

        if (this.datacount.province_id == 1) {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + this.datacount.amphur_name + this.datacount.province_name;
        } else {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + 'อำเภอ' + this.datacount.amphur_name + 'จังหวัด' + this.datacount.province_name;
        }

        console.log("Amphur_name", amphur_id, amphur_name);

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });

  }

  async openModalDistrict() {

    let modal = await this.modalController.create({
      component: ProvincemodalPage,
      componentProps: {
        typepage: 3,
        province_id :this.datacount.province_id,
        amphur_id:this.datacount.amphur_id
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onWillDismiss().then(dataReturned => {
      let data = dataReturned.data;
      if (data != null) {

        let district_id = data.district_id;
        let district_code = data.district_code;
        let district_name = data.district_name;

        this.datacount.district_id = district_id;
        this.datacount.district_code = district_code;
        this.datacount.district_name = district_name;

        if (this.datacount.province_id == 1) {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + this.datacount.district_name + this.datacount.amphur_name + this.datacount.province_name;
        } else {
          this.datacount.full_address = this.datacount.housenumber + '' + this.datacount.village + ' ตำบล' + this.datacount.district_name + 'อำเภอ' + this.datacount.amphur_name + 'จังหวัด' + this.datacount.province_name;
        }

      }

    });
    return await modal.present().then(_ => {
      // triggered when opening the modal
    });

  }


  public async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'md-images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          icon: 'md-camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    var option = {
      quality: 90,
      targetWidth: 960,
      targetHeight: 690,
      correctOrientation: true,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(option).then((imageData) => {

      if (sourceType == 0) {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      } else {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();

      }

    }, (err) => {
      this.showalert("ไม่สำเร็จ! เชริฟเวอร์เชื่อมต่อช้า กรุณาลองใหม่ภายหลัง");
      //alert(JSON.stringify(err));
    });
  }

  async deletePhoto(index) {
    let confirm = await this.alertCtrl.create({
      header: "Sure you want to delete this photo? There is NO undo!",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  doRefresh() {

    let typeitem = JSON.parse(localStorage.getItem('items_type'));
    console.log("items_type", typeitem);
    this.items = typeitem['items_type'];
    this.itemssub = typeitem['items_sub'];

    /* let loader3 = this.loadingCtrl.create({
      content: "กรุณารอสักครู่..."
       });
      loader3.present(); */

    this.images = null;

    // this.http.get(BASEURL + 'imageshowglobalclub.php?cusid=' + this.customer_barcode)
    let myData = {cusid :  this.customer_barcode};
    this.HttpService.post("survey/imageshowglobalclub",myData).subscribe(data => {
        console.log("images1!" + data);
        //loader3.dismissAll();
        if (data != null) {

          this.idedit = data[0]['catagory_id'];
          this.cutomer_img = data[0]['cutomer_img'];
          this.images = data;
          console.log("images2!", this.images);
        } else {
          //loader1.dismissAll();
        }
      }, error => {
        console.log("error!");
        // loader3.dismissAll();
      });
    /* setTimeout(() => {
     loader3.dismissAll();
   }, 3000); */


  }


  public LoadData() {

    if (this.ideditcus == null || this.ideditcus == '' || this.ideditcus == 'undefined') {
      this.addShowid();

    } else {
      this.addEditid();
      console.log("ideditcus234!" + this.ideditcus);
    }
  }

  async addEditid() {

    this.images = [];
    this.items_data = [];

    let loader5 = await this.loadingCtrl.create({
      message: "กรุณารอสักครู่..."
    });
    loader5.present();
    let link = BASEURL + 'edit_customer_registration_mobile.php?';
    let myData = {
      tokenapi: 'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
      customer_barcode: this.customer_barcode,
      newbranch: this.newbranch,
      branch_code: this.branch_code,
      idcard: this.idcard

    }
    console.log('Sentar myData' , myData);
    // this.http.post(link, JSON.stringify(myData))
    this.HttpService.post("survey/eiditcustomerreigistrationmobile",myData).subscribe(async data => { /******* ส่วนที่แก้ไข  *****/

        console.log('items data' ,data);
        if (data != null) {
          loader5.dismiss();
         // console.log('Sentar data' + data);
          //this.datacustomer = data;

          this.items_score = data['items_score'];
          if(this.items_score.length > 0){
            this.score_net = this.items_score[0].score_net;
            this.score_pay = this.items_score[0].score_pay;
            this.balance_score = this.items_score[0].balance_score;
          }

          console.log('items_score' + this.items_score);
         

          this.items_data = data['items_data'];
          this.images = data['items_img'];
          
          let subtype  = data['items_subtype'];
          subtype.forEach(element => {
            let type = element.customer_subtype_id;
            this.typeid.push(type);
          });

          this.datacount.membership_type_globalclub = this.typeid;

          console.log('items_data', this.items_data,this.items_score);

          if(this.items_data.length > 0){

          this.gbh_customer_id = this.items_data[0]['gbh_customer_id'];
          this.datacount.cusid = this.items_data[0]['customer_barcode'];
          this.datacount.firstname = this.items_data[0]['firstname'];
          this.datacount.lastname = this.items_data[0]['lastname'];
          this.datacount.age = this.items_data[0]['age'];
          this.datacount.full_address = this.items_data[0]['full_address'];
          this.datacount.identification_card = this.items_data[0]['identification_card'];
          this.datacount.mobile = this.items_data[0]['mobile'];
          this.datacount.email = this.items_data[0]['email'];
          this.datacount.province_name = this.items_data[0]['province_name'];
          this.datacount.amphur_name = this.items_data[0]['amphur_name'];
          this.datacount.district_name = this.items_data[0]['district_name'];
          this.datacount.sex = this.items_data[0]['sex'];
          this.datacount.customer_type_globalclub = this.items_data[0]['customer_type_id'];
         // this.datacount.membership_type_globalclub = this.items_data[0]['member_type_id'];

          this.datacount.facebook = this.items_data[0]['facebook'];
          this.datacount.title_name = this.items_data[0]['titlename'];
          this.datacount.birthday = this.items_data[0]['date_birthday'];
          this.datacount.housenumber = this.items_data[0]['house_no'];
          this.datacount.province_id = this.items_data[0]['province_id'];
          this.datacount.amphur_id = this.items_data[0]['amphur_id'];
          this.datacount.district_id = this.items_data[0]['district_id'];
          this.datacount.postcode = this.items_data[0]['zipcode'];
          this.newbranch = this.items_data[0]['branch_code'];
          this.datacount.branchname = this.items_data[0]['branch_name'];
          this.flag_newcus = '0';

          this.coords.latitude = this.items_data[0]['latitude'];
          this.coords.longitude = this.items_data[0]['longitude'];

          var date = new Date(this.datacount.birthday);

          console.log('date', date.toDateString());
          let date2 = new Date(date.getFullYear() + 543, date.getMonth(), date.getDate());
          this.datacount.birthday = this.datepipe.transform(date2, 'yyyy-MM-dd');
          console.log('birthday', this.datacount.birthday);

          this.loadMapEdit();

          }

          this.dataSource = {
            "chart": {
              "caption": "คะแนน Global Club",
              "palettecolors": "#0075c2,#FFC107,#1aaf5d",
              "showValues": "1",
              "theme": "fusion",

            },
            "data": [{
              "label": "คะแนนทั้งหมด",
              "value": this.score_net
            }, {
              "label": "คะแนนที่ใช้",
              "value": this.score_pay
            }, {
              "label": "คะแนนคงเหลือ",
              "value": this.balance_score
            }]
          };

         
        } else {
          let alert = await this.alertCtrl.create({
            header: 'ไม่มีข้อมูล ลองอีกครั้ง',
            subHeader: 'ไม่มีข้อมูลหรือแสกนใหม่อีกครั้ง',
            buttons: ['ตกลง']
          });
          alert.present();
          loader5.dismiss();
          console.log('ไม่มีข้อมูล' + this.mobile);
        } 
      }, error => {
        console.log("error!");
        loader5.dismiss();
      });

  }


  loadMapEdit() {

    // this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    //  this.coords.latitude = position.coords.latitude;
    // this.coords.longitude = position.coords.longitude;

    this.latLng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);

    let mapOptions = {
      center: this.latLng,
      zoom: 17,
      compass: true,
      myLocationButton: true,
      indoorPicker: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      setMyLocationEnabled: true,
      timeout: 4000,
      enableHighAccuracy: true,

    }
    console.log("latLng2" + this.coords.latitude + "latLng2" + this.coords.longitude);

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //let locationOptions = { timeout: 30000, enableHighAccuracy: true }; 
    var marker = new google.maps.Marker({ position: this.latLng, map: this.map, title: 'Hello!' });



  }


  async showConfirm() {
    let confirm = await this.alertCtrl.create({
      header: 'ท่านต้องการบันทึก?',
      message: 'ท่านต้องการบันทึกข้อมูลหรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Agree clicked');
            this.saveproduct();
          }
        }
      ]
    });
    confirm.present();
  }

  saveproducttest() {

    console.log('membership_type_globalclub clicked', this.datacount.membership_type_globalclub);

    // let link = BASEURL + 'insertedit_customer_registration_mobiletest.php'; 
    let myData = {
      membership_type_globalclub: this.datacount.membership_type_globalclub
    }
    // this.http.post(link, JSON.stringify(myData))
    this.HttpService.post("survey/inserteditcustomerregistrationmobiletets",myData).subscribe(data => {        /**** ส่วนที่แก้ไข ****/

      console.log('Insert Customer Registration Mobile', data);

    }, Error => {

    });


   // let string = '(1,1311100188500,CUSRE-610829-0003,"MrTest Cus","232 Roiet 50000",088888888)';
   // string = string.replace(new RegExp('('.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), '');
   // let string2 = string.replace(new RegExp(')'.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), '');
    //let string3 = string2.replace(new RegExp(''.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), '');

    

    
  }



  async saveproduct() {


    //this.datacount.birthday2 =this.datepipe.transform(this.datacount.birthday, 'yyyyMMdd'); 
    // console.log("birthday2" + this.datacount.birthday2);

    

    let loader1 = await this.loadingCtrl.create({
      message: "กรุณารอสักครู่..."
    });

    loader1.present();

    let link = BASEURL + 'insertedit_customer_registration_mobile.php';
    let myData = {
      tokenapi: 'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
      getIdedit: this.idedit,
      getIdedittype: this.idedittype,
      gbh_customer_id: this.gbh_customer_id,
      customer_barcode: this.datacount.cusid,
      getDocno: this.docno,
      getNewbranch: this.newbranch,
      getFirstname: this.datacount.firstname,
      getLastname: this.datacount.lastname,
      getFullname: this.datacount.firstname + ' ' + this.datacount.lastname,
      getAge: this.datacount.age,
      getAddress: this.datacount.full_address,
      getIdcard: this.datacount.identification_card,
      getMobile: this.datacount.mobile,
      getEmail: this.datacount.email,
      getDetail: this.datacount.detail,
      getEmployeecode: this.employeecode,
      getsex: this.datacount.sex,
      getRegisterglobalclub: this.registerglobalclub,
      getApproveglobalclub: this.approveglobalclub,
      getcustomer_type_globalclub: this.datacount.customer_type_globalclub,
      getmembership_type_globalclub: this.datacount.membership_type_globalclub,
      gettelephone: this.datacount.telephone,
      getfacebook: this.datacount.facebook,
      gettitle_name: this.datacount.title_name,
      getbirthday: this.datacount.birthday,
      gethousenumber: this.datacount.housenumber,
      getprovince_id: this.datacount.province_id,
      getamphur_id: this.datacount.amphur_id,
      getdistrict_id: this.datacount.district_id,
      getpostcode: this.datacount.postcode,
      sentPhotos: this.photos,
      sentPhotoscount: this.photos.length,
      employeecode: this.employeecode,
      latitude: this.coords.latitude,
      longitude: this.coords.longitude,

    };

    console.log('registerglobalclub clicked', this.datacount.identification_card,this.datacount.mobile,myData);

    this.http.post(link, JSON.stringify(myData))
      .subscribe(async data => {

        console.log('Insert Customer Registration Mobile', data);
        loader1.dismiss();

        if (data['status'] === 400) {

          let alert = await this.alertCtrl.create({
            header: 'ไม่สำเร็จ',
            subHeader: 'บันทึกข้อมูลไม่สำเร็จ, ลองตรวจสอบข้อมูลแล้วบันทึกอีกครั้ง!',
            buttons: ['ตกลง']
          });
          alert.present();
          setTimeout(() => {
            alert.dismiss();
          }, 3000);

        } else if (data['status'] === 200) {
          this.photos = [];

          this.customer_barcode = data['data']['customer_barcode'];
          this.flag_newcus = data['data']['flag_newcus'];
          this.full_address = data['data']['full_address'];
          this.fullname = data['data']['fullname'];
          this.identification_card = data['data']['identification_card'];
          this.mobile = data['data']['mobile'];
          this.branch_code = data['data']['branch_code'];

          if (this.flag_newcus == '0') {
            //this.addEditid();
            let alert = await this.alertCtrl.create({
              header: 'สมาชิกโกลบอลคลับ',
              //subTitle: 'ท่านสามารถสมัครสมาชิกโกลบอลคลับได้หรือไม่',
              message: 'สมัครสมาชิกสำเร็จแล้ว, รหัสสมาชิกของท่านคือ' + this.customer_barcode,
              buttons: ['ตกลง']
            });
            alert.present();
            setTimeout(() => {
              alert.dismiss();
            }, 3000);

          } else if (this.flag_newcus == '1') {

            let alert = await this.alertCtrl.create({
              header: 'เป็นสมาชิกโกลบอลคลับอยู่แล้ว',
              //subTitle: 'ท่านสามารถสมัครสมาชิกโกลบอลคลับได้หรือไม่',
              message: 'ท่านเป็นสมาชิกโกลบอลคลับอยู่แล้ว, รหัสสมาชิกของท่านคือ' + this.customer_barcode,
              buttons: ['ตกลง']
            });
            alert.present();
            setTimeout(() => {
              alert.dismiss();
            }, 3000);

          }
          this.nav.back();
          /* this.events.publish('reloadDetails');
          this.navCtrl.popTo(ListCustomerPage); */

        } else if (data['status'] === 201) {
          this.photos = [];

          let alert = await this.alertCtrl.create({
            header: 'แก้ไขข้อมูล?',
            //subTitle: 'ท่านสามารถสมัครสมาชิกโกลบอลคลับได้หรือไม่',
            message: data['data'],
            buttons: ['ตกลง']
          });
          alert.present();
          setTimeout(() => {
            alert.dismiss();
          }, 3000);

          this.addEditid();

          //this.events.publish('reloadDetails');
          //this.navCtrl.popTo(ListCustomerPage);


        } else {
          let alert = await this.alertCtrl.create({
            header: 'ไม่สำเร็จ',
            subHeader: 'บันทึกข้อมูลไม่สำเร็จ, ลองตรวจสอบข้อมูลแล้วบันทึกอีกครั้ง!',
            buttons: ['ตกลง']
          });
          alert.present();
          setTimeout(() => {
            alert.dismiss();
          }, 3000);
        }





      }, Error => {

        this.showalert(Error);
        loader1.dismiss();
      });

  }

  saveglobalclub() {

    let link = BASEURL + 'checkcustomer/insert_registration_globalclub.php'; /**** กำลังทำ ****/
    let myData = {
      tokenapi: 'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
      customer_barcode: this.customer_barcode,
      flag_newcus: this.flag_newcus,
      full_address: this.full_address,
      fullname: this.fullname,
      identification_card: this.identification_card,
      mobile: this.mobile,
      branch_code: this.branch_code,
      employeecode: this.employeecode,
      docno: this.docno,
      email: this.datacount.email
    }
    this.http.post(link, JSON.stringify(myData))
      .subscribe(async data => {
        if (data != null) {
   
        } else {
          
        }
      }, Error => {
        this.showalert(Error);
      });

  }

  public async addShowid() {
    let loader9 = await this.loadingCtrl.create({
      message: "กรุณารอสักครู่..."
    });
    loader9.present();
    this.http.get(BASEURL + 'check_id_customer.php?docno=' + this.docno)
      .subscribe(data => {
        if (data != null) {
          loader9.dismiss();
          this.maxid = data[0]['max'];
          console.log("maxid!" + this.maxid);

          this.ideditcus = this.maxid;

          this.addEditid();
        } else {
          loader9.dismiss();
        }
      });

  }




  public getItems(ev: any) {
    // set val to the value of the searchbar
    this.val = ev.target.value;

  }
  public async searchForTerm() {

    this.images = [];
    this.items_data = [];

    let val2 = this.val;
    console.log('Sentar clicked' +val2 );


    let loader2 = await this.loadingCtrl.create({
      message: "กรุณารอสักครู่..."
    });
    loader2.present();

    let link = BASEURL + "checkscorecustomer.php?";
    console.log('link data' + link);
   // this.http.post(link + 'idcard=' + val2)
    let myData = {
      idcard: val2,
      newbranch: this.newbranch,
      branch_code: this.branch_code
    }
    this.http.post(link, JSON.stringify(myData))
      .subscribe(async data => {
       
        if (data != null) {
          loader2.dismiss();
         // console.log('Sentar data' + data);
          //this.datacustomer = data;

          this.items_score = data['items_score'];
          if(this.items_score.length > 0){
            this.score_net = this.items_score[0].score_net;
            this.score_pay = this.items_score[0].score_pay;
            this.balance_score = this.items_score[0].balance_score;
          }

          console.log('items_score' + this.items_score);
         

          this.items_data = data['items_data'];
          this.images = data['items_img'];
          
          let subtype  = data['items_subtype'];
          subtype.forEach(element => {
            let type = element.customer_subtype_id;
            this.typeid.push(type);
          });

          this.datacount.membership_type_globalclub = this.typeid;

          console.log('items_data', this.items_data,this.items_score);

          if(this.items_data.length > 0){

          this.gbh_customer_id = this.items_data[0]['gbh_customer_id'];
          this.datacount.cusid = this.items_data[0]['customer_barcode'];
          this.datacount.firstname = this.items_data[0]['firstname'];
          this.datacount.lastname = this.items_data[0]['lastname'];
          this.datacount.age = this.items_data[0]['age'];
          this.datacount.full_address = this.items_data[0]['full_address'];
          this.datacount.identification_card = this.items_data[0]['identification_card'];
          this.datacount.mobile = this.items_data[0]['mobile'];
          this.datacount.email = this.items_data[0]['email'];
          this.datacount.province_name = this.items_data[0]['province_name'];
          this.datacount.amphur_name = this.items_data[0]['amphur_name'];
          this.datacount.district_name = this.items_data[0]['district_name'];
          this.datacount.sex = this.items_data[0]['sex'];
          this.datacount.customer_type_globalclub = this.items_data[0]['customer_type_id'];
         // this.datacount.membership_type_globalclub = this.items_data[0]['member_type_id'];

          this.datacount.facebook = this.items_data[0]['facebook'];
          this.datacount.title_name = this.items_data[0]['titlename'];
          this.datacount.birthday = this.items_data[0]['date_birthday'];
          this.datacount.housenumber = this.items_data[0]['house_no'];
          this.datacount.province_id = this.items_data[0]['province_id'];
          this.datacount.amphur_id = this.items_data[0]['amphur_id'];
          this.datacount.district_id = this.items_data[0]['district_id'];
          this.datacount.postcode = this.items_data[0]['zipcode'];
          this.newbranch = this.items_data[0]['branch_code'];
          this.datacount.branchname = this.items_data[0]['branch_name'];
          this.flag_newcus = '0';

          this.coords.latitude = this.items_data[0]['latitude'];
          this.coords.longitude = this.items_data[0]['longitude'];

          var date = new Date(this.datacount.birthday);

          console.log('date', date.toDateString());
          let date2 = new Date(date.getFullYear() + 543, date.getMonth(), date.getDate());
          this.datacount.birthday = this.datepipe.transform(date2, 'yyyy-MM-dd');
          console.log('birthday', this.datacount.birthday);

          }

          this.dataSource = {
            "chart": {
              "caption": "คะแนน Global Club",
              "palettecolors": "#0075c2,#FFC107,#1aaf5d",
              "showValues": "1",
              "theme": "fusion",

            },
            "data": [{
              "label": "คะแนนทั้งหมด",
              "value": this.score_net
            }, {
              "label": "คะแนนที่ใช้",
              "value": this.score_pay
            }, {
              "label": "คะแนนคงเหลือ",
              "value": this.balance_score
            }]
          };

         
        } else {
          let alert = await this.alertCtrl.create({
            header: 'ไม่มีข้อมูล ลองอีกครั้ง',
            subHeader: 'ไม่มีข้อมูลหรือแสกนใหม่อีกครั้ง',
            buttons: ['ตกลง']
          });
          alert.present();
          loader2.dismiss();
          console.log('ไม่มีข้อมูล' + this.mobile);
        } 

      },
        async error => {
          //this.showError(error);
          let alert = await this.alertCtrl.create({
            header: 'ไม่มีข้อมูล',
            subHeader: error,
            buttons: ['ตกลง']
          });
          alert.present();
          loader2.dismiss();
        });

  }

  async showalert(text) {
    let alert = await this.alertCtrl.create({
      header: 'การบันทึก',
      subHeader: text,
      buttons: ['ตกลง']
    });
    alert.present();
  }

  /* imgCustumer2() {
    this.navCtrl.push(ImagecustomerPage, {
      docno: this.docno,
      idedit: this.idedit
    });
  } */


  async deletePhotoOnServer(id_img) {
    let confirm = await this.alertCtrl.create({
      header: "คุณต้องการลบรูปภาพนี้ ?",
      message: "",
      buttons: [
        {
          text: "ยกเลิก",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "ตกลง",
          handler: () => {
            console.log("Agree clicked" + id_img);
            this.http.get(BASEURL + 'Delete_gbh_customer_globalclub_img.php?id_img=' + id_img)
              .subscribe(data => {
                console.log("data clicked" + data);
                this.addEditid();
                //this.doRefresh();
              }, error => {
                this.addEditid();
                //this.doRefresh();
                console.log("error clicked" + error);
                //console.log(JSON.stringify(error.json()));
              });
          }
        }
      ]
    });
    confirm.present();
  }


  loadMap() {

    if (this.platform.is('android')) {
      this.platform.ready().then(() => {
        AdvancedGeolocation.start((success) => {
          //loading.dismiss();
          // this.refreshCurrentUserLocation();
          try {
            var jsonObject = JSON.parse(success);
            console.log("Provider " + JSON.stringify(jsonObject));
            switch (jsonObject.provider) {
              case "gps":
                console.log("setting gps ====<<>>" + jsonObject.latitude);
                this.latLng = new google.maps.LatLng(jsonObject.latitude, jsonObject.longitude);

                this.coords.latitude = jsonObject.latitude;
                this.coords.longitude = jsonObject.longitude;
                this.showmap(this.latLng);
                break;

              case "network":
                console.log("setting network ====<<>>" + jsonObject.latitude);


                this.latLng = new google.maps.LatLng(jsonObject.latitude, jsonObject.longitude);

                this.coords.latitude = jsonObject.latitude;
                this.coords.longitude = jsonObject.longitude;
                this.showmap(this.latLng);

                break;

              case "satellite":
                //TODO
                break;

              case "cell_info":
                //TODO
                break;

              case "cell_location":
                //TODO
                break;

              case "signal_strength":
                //TODO
                break;
            }
          }
          catch (exc) {
            console.log("Invalid JSON: " + exc);
          }
        },
          function (error) {
            console.log("ERROR! " + JSON.stringify(error));
          },
          {
            "minTime": 500,         // Min time interval between updates (ms)
            "minDistance": 1,       // Min distance between updates (meters)
            "noWarn": true,         // Native location provider warnings
            "providers": "all",     // Return GPS, NETWORK and CELL locations
            "useCache": true,       // Return GPS and NETWORK cached locations
            "satelliteData": false, // Return of GPS satellite info
            "buffer": false,        // Buffer location data
            "bufferSize": 0,         // Max elements in buffer
            "signalStrength": false // Return cell signal strength data
          });

      });
    } else {

      let options = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 30000
      };


      this.geolocation.getCurrentPosition(options).then((position) => {

        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        this.coords.latitude = position.coords.latitude;
        this.coords.longitude = position.coords.longitude;
        this.showmap(this.latLng);


      }, (err) => {
        console.log(err);
      });
    }

  }

  showmap(latLng) {
    let mapOptions = {
      center: latLng,
      zoom: 17,
      compass: true,
      myLocationButton: true,
      indoorPicker: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      setMyLocationEnabled: true,
      timeout: 5000,
      enableHighAccuracy: true,

    }
    console.log("latLng" + this.coords.latitude + "latLng" + this.coords.longitude);




    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //let locationOptions = { timeout: 30000, enableHighAccuracy: true }; 
    var marker = new google.maps.Marker({ position: this.latLng, map: this.map, title: 'Hello!' });
  }

  showConfirmMap(){
    
  }

}
