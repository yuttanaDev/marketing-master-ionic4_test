import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { GlobalclubPage } from './globalclub.page';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ProvincemodalPage } from '../provincemodal/provincemodal.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';
 
// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import mscombi2D from 'fusioncharts/viz/mscombi2d'; 
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
 
// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

const routes: Routes = [
  {
    path: '',
    component: GlobalclubPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    TranslateModule,
    NgxIonicImageViewerModule,
    FusionChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GlobalclubPage,ProvincemodalPage],
  providers: [
    LocationAccuracy,
    Geolocation,
    Camera,
  ],
  entryComponents: [
    ProvincemodalPage,
  ],
})
export class GlobalclubPageModule {}
