import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatePipe } from '@angular/common';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';
import { NetworkServiceProvider } from '../providers/network-service/network-service';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { FormsModule, FormBuilder  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
//UPDATE App Auto ionic 4
import { AppUpdate } from '@ionic-native/app-update/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { Device } from '@ionic-native/device/ngx';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//Modal
import { DismissModalPageModule } from './dismiss-modal/dismiss-modal.module';
import { DismissModalnormalPageModule } from './dismiss-modalnormal/dismiss-modalnormal.module';
import { DismissRatingsPageModule } from './dismiss-ratings/dismiss-ratings.module';
import { CartPageModule } from './cart/cart.module';


import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(), 
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    DismissModalPageModule,
    DismissModalnormalPageModule,
    DismissRatingsPageModule,
    CartPageModule,
    LazyLoadImageModule,
    NgxIonicImageViewerModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
    
  providers: [
    StatusBar,
    SplashScreen,
    FCM,
    BarcodeScanner,
    InAppBrowser,
    DatePipe,
    AppVersion,
    AndroidPermissions,
    AppUpdate,
    WebIntent,
    Camera,
    Device,
    Network,
    FormBuilder,
    ReactiveFormsModule,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NetworkServiceProvider,
    ApiServiceProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
