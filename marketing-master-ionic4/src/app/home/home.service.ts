import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class homeService {
  userData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

 

  getData(postData: any): Observable<any> {

    return this.httpService.post('survey/checkdoc', postData);
  }

}
