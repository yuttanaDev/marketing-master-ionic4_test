
import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";

import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { homeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public namebranch: any;
  public newbranch: any;
  public doc_no: any;
  public branchcode: any;
  public branchname: any;

  public isShown = true;
  public docno: any;
  public count_id: any = 0;
  public id_suvey: any = 0;
  public team_name: any =null;
  public id_img: any = 0;
  public gbh_customer_id: any = 0;
  public mile: any = 0;
  public speedometer: any=null;
  public speedometer2: any=null;
  constructor(
    private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public alertCtrl: AlertController,
    public homeSerive: homeService,
  ) {

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.doc_no = this.router.getCurrentNavigation().extras.state.doc_no;
        this.branchcode = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        // this.checkData_old();
        this.checkData();
      }
    });

  }


  async checkData(){
    let mydata = {
      doc_no : this.doc_no
    };

    this.homeSerive.getData(mydata).subscribe(
      (res: any) => { 
        // console.log('res data : ', res);
        this.team_name = res['team_name'];
        this.speedometer = res['speedometer'];
        this.speedometer2 = res['speedometer2'];
        this.count_id = res['count_id'][0]['count_id'];
        this.gbh_customer_id = res['gbh_customer_id'][0]['gbh_customer_id'];
        this.isShown = false;
      }, error => {
        this.isShown = false;
        //console.log(JSON.stringify(error.json()));
      });
      setTimeout(() => {
        this.isShown = false;
      }, 7000);
  
  }

  // checkData_old() {

  //   this.isShown = true;
  //   let link = API_URL + 'check_doc.php?docno=' + this.doc_no;

  //   this.http.get(link)
  //     .subscribe(data => {
  //       //console.log('data',data);
  //       this.team_name = data['team_name'];
  //       this.speedometer = data['speedometer'];
  //       this.speedometer2 = data['speedometer2'];
  //       this.count_id = data['count_id'][0]['count_id'];
  //       this.gbh_customer_id = data['gbh_customer_id'][0]['gbh_customer_id'];
        
  //       //console.log('data count_id',data['count_id'][0]['id_img']);
  //       this.isShown = false;
  //     }, error => {
  //       this.isShown = false;
  //       //console.log(JSON.stringify(error.json()));
  //     });
  //   setTimeout(() => {
  //     this.isShown = false;
  //   }, 7000);

  // }

  doRefresh() {
    this.isShown = true;
    this.team_name = null;
    this.speedometer = null;
    this.speedometer2 = null;
    this.count_id = 0;
    this.gbh_customer_id = 0;

    setTimeout(() => {
      this.checkData();
    }, 1500);
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }




  itemNameteam() {

    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: this.doc_no,
        branchcode:this.branchcode,
        branchname:this.branchname
      }
    };
    this.router.navigate(['nameteam'], navigationExtras);
  }
  itemMile1() {
    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: this.doc_no,
        branchcode:this.branchcode,
        branchname:this.branchname
      }
    };
    this.router.navigate(['startbill'], navigationExtras);
  }
  itemMile2() {

    //this.nav.navigateForward(['/endbill']);
    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: this.doc_no,
        branchcode:this.branchcode,
        branchname:this.branchname
      }
    };
    this.router.navigate(['endbill'], navigationExtras);
  }

  itemListlocation() {

   // this.nav.navigateForward(['/addlistlocation']);
    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: this.doc_no,
        branchcode:this.branchcode,
        branchname:this.branchname
      }
    };
    this.router.navigate(['addlistlocation'], navigationExtras);
  }

  itemListGlobalclub() {

    //this.nav.navigateForward(['/listcustomer']);
    let navigationExtras: NavigationExtras = {
      state: {
        doc_no: this.doc_no,
        branchcode:this.branchcode,
        branchname:this.branchname
      }
    };
    this.router.navigate(['listcustomer'], navigationExtras);
  }

  



}