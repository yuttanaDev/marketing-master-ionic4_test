import { Component } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform,NavController,AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';
import { AppUpdate } from '@ionic-native/app-update/ngx';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './services/storage.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [
    
    /* {
      title: 'หน้าหลัก',
      url: '/home',
      icon: 'home'
    }, */
     {
      title: 'หน้าหลัก',
      url: '/list',
      icon: 'list-box'
    },
    {
      title: 'รายงานปิดบิลสำรวจ',
      url: '/listreport',
      icon: 'list-box'
    },
    {
      title: 'รายการสั่งสินค้า',
      url: '/listorder',
      icon: 'list-box'
    },
    {
      title: 'สั่งสินค้าสาขา',
      url: '/orderhome',
      icon: 'basket'
    },
    /* {
      title: 'แสกนบิล ลูกค้า',
      url: '/listbillscan',
      icon: 'barcode'
    }, */
    
    
  ];
  public appPages2 = [
    
    /* {
      title: 'ออกจากระบบ',
      url: '/logout',
      icon: 'lock'
    }, */
    
  ];

  public version_app: any = '0.5.1';
  public version: any;
  userData$ = new BehaviorSubject<any>([]);

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private fcm: FCM,
    public storage: Storage,
    public alertController: AlertController,
    private appUpdate: AppUpdate,
    private httpClient: HttpClient,
    public translate: TranslateService,
    private router: Router,
    private storageService: StorageService,
  ) {
    this.initializeApp();
    this.translate.setDefaultLang('th');

    if (this.platform.is('android')) {

      const link = 'https://global-house.co.th/AppStore/Api/detail.php';
      const myData = {
        tokenapi: "EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD",
        appid: '8',
      };
      this.httpClient.post(link, JSON.stringify(myData))
        .subscribe(data => {
           console.log('detail', data);
          if (data != null) {
            this.version = data[0].app_version;
            if (this.version_app == this.version) {

            } else {
              const updateUrl = 'https://global-house.co.th/AppStore/Api/update/update.php?appid=8';
              this.appUpdate.checkAppUpdate(updateUrl).then((data) => { //alert('Update available');
                //console.log('app_version', JSON.stringify(data))
                //alert('Update available'+ JSON.stringify(data.json()));
              }, error => {
                //alert('error'+ JSON.stringify(error.json()));
              });;
            }
  
          } else {
  
          }
        }, error => {
          //console.log(JSON.stringify(error.json()));
        });

  } else {
    //this.handleSplashScreen();
  }

  
  }

  async showConfirm() {

    const alert = await this.alertController.create({
      header: 'ออกจากระบบหรือไม่??',
      message: 'ท่านต้องการ ออกจากระบบหรือไม่??',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ออกจากระบบ',
          handler: () => {
            console.log('Agree clicked');
            this.itemTapped();
          }
        }
      ]
    });

    await alert.present();

  }

  itemTapped() {


    this.storage.clear().then(() => {
      console.log('Keys have been cleared');
    });
    //Api Token Logout
    setTimeout(() => this.backToLogout(), 1000);

    //this.app.getRootNav().setRoot(LogoutPage);
    // this.navCtrl.setRoot(LogoutPage);
  }
  backToLogout() {
    this.navCtrl.navigateRoot("/login");
  }

  logout() {
    this.storageService.clear();
    this.userData$.next('');
    this.router.navigate(['login']);
  }



  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.fcm.subscribeToTopic('ordernow');
      this.fcm.getToken().then(token => {
        console.log(token);
      });

      this.fcm.onNotification().subscribe(data => {
        console.log(data);
        if (data.wasTapped) {
          console.log('Received in background');
          this.router.navigate([data.landing_page, data.price]);
        } else {
          console.log('Received in foreground');
          this.router.navigate([data.landing_page, data.price]);
        }
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token);
      });

      // this.fcm.unsubscribeFromTopic('marketing');
    });
  }
}
