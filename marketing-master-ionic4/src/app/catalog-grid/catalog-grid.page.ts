import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController, NavController, IonContent ,/* NavParams , */ IonSearchbar, ModalController, ActionSheetController, AlertController, } from '@ionic/angular';

import { HttpClient } from '@angular/common/http';
import { Storage } from "@ionic/storage";
import { BASEURL, URLIMG, API_URL,systemsetting } from '../../providers/constants/constants';

import { Router, ActivatedRoute, NavigationExtras,NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { FormControl } from "@angular/forms";
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { ScrollDetail } from '@ionic/core';

@Component({
  selector: 'app-catalog-grid',
  templateUrl: './catalog-grid.page.html',
  styleUrls: ['./catalog-grid.page.scss'],
})
export class CatalogGridPage implements OnInit {


  
  @ViewChild('pageTop', { static: true }) pageTop: IonContent;
  @ViewChild('search', { static: true }) searchbar: IonSearchbar;
  @ViewChild(IonContent, { static: false }) content: IonContent;

  public searchControl: FormControl;
  productData: any;
  getcatID: number;
  getsearch: string = "";

  trail: any;

  //fillter
  orderbyFilter: string = "";
  getgroupId: number = 0;
  allbrandItems: any=[];
  tabselect: string = "";
  priceLanght: any = {};

  filterBrand: string = ""; // simple date 'brandName1','brandName2','brandName3'
  filterPricelength: string = ""; // simple date $price1 AND $price2
  //fillter
  newbranch: any;
  pageNumber: number = 1;
  pageAllpage: number = 0;
  pageAllproduct: number = 0;
  public showToolbar: any;
  imgBanner: string = "";

  isshowTopbutton: boolean = false;
  isloadcat: boolean = true;
  loader: any;
  base_url: any = systemsetting.base_url;
  base_token: any = systemsetting.token;
  items: any;
  public sentpush: any=null;
  public searchactive: boolean = false;
  someDefaultImage = 'assets/imgs/preloader.png';
  imageToShowOnError = 'assets/imgs/preloader.png';
  public pageUrlParent: any;
  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public storage: Storage,
    public route: ActivatedRoute,
    public router: Router,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController
  ) {

    // this.route.queryParams.subscribe(params => {
    //   console.log(params);
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     console.log(this.router.getCurrentNavigation().extras.state.setcatID);

    //     this.getcatID = this.router.getCurrentNavigation().extras.state.item['setcatID'];

    //   }
    // });

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) { // form cat page
        if (this.router.getCurrentNavigation().extras.state.setcatID !== undefined) {
          this.getcatID = this.router.getCurrentNavigation().extras.state.setcatID;
          this.filterBrand = this.router.getCurrentNavigation().extras.state.setFilterBrand;
          console.log(this.getcatID,this.filterBrand);
        } else {  // form cat page search page
          this.getsearch = this.router.getCurrentNavigation().extras.state.txtSearch;
          this.getgroupId = this.router.getCurrentNavigation().extras.state.txtgroupId;
          this.searchactive = this.router.getCurrentNavigation().extras.state.searchactive;
          console.log('filterBrand',this.filterBrand,this.getsearch+" "+this.getgroupId+" "+this.searchactive);
        }
        this.sentpush = this.router.getCurrentNavigation().extras.state.sentpush;
        console.log('sentpush',this.sentpush);
      }
    });
    


  }
  backButton(){
    this.navCtrl.back();
  }

  ngOnInit() {
    this.checkParam();
  }

  onScroll($event: CustomEvent<ScrollDetail>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
    const scrollTop = $event.detail.scrollTop;
    //this.showToolbar = scrollTop >= 225;

    if (scrollTop >= 850) {
      // console.log("scrolling down, hiding footer...");
      this.showToolbar = true;
     } else if (scrollTop < 850 ){
      // console.log("scrolling up, revealing footer...");
       this.showToolbar = false;
     };  

    }

    

    }

    ScrollToTop(){
      this.showToolbar = false;
      this.content.scrollToTop(1500);
    }


  ionViewWillEnter() {



    /*  this.getcatID = this.navparms.get("setcatID");
        this.getsearch = this.navparms.get("txtSearch");
        this.getgroupId = this.navparms.get("txtgroupId"); */

    this.tabselect = "PROMOT";
    this.orderbyFilter = " ORDER BY pro_sale_price DESC ";
    this.priceLanght.min = 0;
    this.priceLanght.max = 10;

    /* this.searchactive = this.navparms.get("searchactive");
    if (this.navparms.get("setFilterBrand") !== undefined) {
        this.filterBrand = this.navparms.get("setFilterBrand");
    } */
    

        
  }

  checkParam(){
    
     this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((e: any) => {
      //console.log('tabBar e',e);
      const urlArray = e.url.split('/');
      this.pageUrlParent = urlArray[urlArray.length - 2];
      
      //console.log('pageUrlParent',this.pageUrlParent);

    }); 
  }

  clickToSearch() {

    if (this.searchactive == true) {
      this.navCtrl.pop();
    } else {

      /*    this.navCtrl.push(SearchproductPage, {
           searchactive: true
         }); */

      let navigationExtras: NavigationExtras = {
        state: {
          searchactive: true
        }
      };
      this.router.navigate(['searchproduct'], navigationExtras);
    }

  }



  setFilteredItems() {
    /* this.searchControl.valueChanges
    .pipe(debounceTime(1000))
    .subscribe(search => {
    console.log(search);
    if(search != ""){
    this.txtSearchAll = search;
    this.searchProductincat(search);
    }else{
    this.items = [
    { pro_name_new: "ประตู" },
    { pro_name_new: "หน้าต่าง" },
    { pro_name_new: "ไม้อัด" },
    { pro_name_new: "เครื่องปรับอากาศ" },
    { pro_name_new: "เหล็กเส้น" },
    { pro_name_new: "เครื่องทำน้ำอุ่น" },
    { pro_name_new: "ทีวี" }
    ];
    }
    }); */

  }

  /* ngOnInit(){
   
  this.CheckFromLoadProduct();
  } */

  ngAfterViewInit() {
    this.content.ionScroll.subscribe((data) => {
      if (data.scrollTop > 400 && this.isshowTopbutton == false) {
        this.isshowTopbutton = true;
      } else if (data.scrollTop < 400 && this.isshowTopbutton == true) {
        this.isshowTopbutton = false;
      }
    });

    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      console.log('newbranch : ' + this.newbranch);
       //เช็ค Product
       this.CheckFromLoadProduct();

    });

   
  }

  public pageScrollertop() {
    //scroll to page top
    this.isshowTopbutton = false;
    this.content.scrollToTop();
  }



  // Check From Load Product
  CheckFromLoadProduct() {
    this.pageAllpage = 0;
    //this.productData = null;
    this.pageNumber = 1;
    

    if (this.getsearch == "") { // ถ้าไม่ได้มาจาก Search
      this.loadProductallpage();
      this.loadProduct(this.pageNumber);
      console.log('no getsearch',this.getsearch);

    } else {
      this.loadsearchProductallpage();
      this.loadProductSearch(this.pageNumber);
      console.log('getsearch',this.getsearch);
    }
  }

  //load product cat
  loadProduct(setpageNum: number) {
    //this.popupLoading();
    console.log(' getcatID', this.getcatID,this.filterBrand);

    this.isloadcat = true;
    var postData = JSON.stringify({
      catid: this.getcatID,
      textsearch: '',
      orderbyFilter: this.orderbyFilter,
      filterBrand: this.filterBrand,
      fillterPrice: this.filterPricelength,
      grouplevelid: 0,
      pageNum: setpageNum,
      token: this.base_token,
      branch_code: this.newbranch,
    });
    console.log("filData:",postData);
    this.http.post(this.base_url + 'ProductsBranch/getproductcatagory', postData)

      .subscribe(data => {
        console.log(data);

        if (data != null) {

          if (this.productData == null) {
            this.productData = data;

          } else {
            this.items = data;
            this.items.forEach(element => {
              //console.log(element);
              this.productData.push({
                pro_barcode: element.pro_barcode,
                pro_img1: element.pro_img1,
                pro_name_new: element.pro_name_new,
                pro_price: element.pro_price,
                pro_sale_price: element.pro_sale_price,
                status: 200
              }
              );
            });
            //this.popupDismiss();

            //return true;
          }
          this.isloadcat = false;

          // console.log('productData ',this.productData);

        }
      },
        error => {
          console.log('no Data ');
          this.isloadcat = false;
        });

  }

  //load product Search
  loadProductSearch(numPage: number) {
    //this.popupLoading();
    this.isloadcat = true;
    var postData = JSON.stringify({
      catid: 0,
      textsearch: this.getsearch,
      orderbyFilter: this.orderbyFilter,
      filterBrand: this.filterBrand,
      fillterPrice: this.filterPricelength,
      grouplevelid: this.getgroupId,
      pageNum: numPage,
      token: this.base_token,
      branch_code: this.newbranch,
    });

    console.log('search', postData);
    this.http.post(this.base_url + 'ProductsBranch/getproductcatagory', postData)

      .subscribe(data => {
        if (data != null) {

          if (this.productData == null) {
            this.productData = data;

          } else {
            this.items = data;
            this.items.forEach(element => {
              //console.log(element);
              this.productData.push({
                pro_barcode: element.pro_barcode,
                pro_img1: element.pro_img1,
                pro_name_new: element.pro_name_new,
                pro_price: element.pro_price,
                pro_sale_price: element.pro_sale_price,
                status: 200
              }
              );
            });
            //this.popupDismiss();

            //return true;
          }
          this.isloadcat = false;
          console.log('productData ',this.productData);

        }else{
          console.log('no Data ');
          this.isloadcat = false;
        }
      },
        error => {
          console.log('no Data ');
          this.isloadcat = false;
        });

  }

  loadProductallpage() {
    var postData = JSON.stringify({
      catid: this.getcatID,
      filterBrand: this.filterBrand,
      fillterPrice: this.filterPricelength,
      token: this.base_token
    });
    this.http.post(this.base_url + 'ProductsBranch/getproductcatagoryall', postData)

      .subscribe(data => {
        if (data['status'] == 200) {
          this.pageAllpage = data['pro_allpage'];
          this.pageAllproduct = data['pro_all'];
          this.allbrandItems = data['allbrand'];
          this.priceLanght.min = parseFloat(data['minPrice']);
          this.priceLanght.max = parseFloat(data['maxPrice']);
          this.getcatID = parseInt(data['groupid']);
          this.trail = data['trail'];
          this.loadCatbanner();

          console.log('AllbrandItems:' , this.allbrandItems);
          //console.log('mim:'+data.minPrice+" max:"+data.maxPrice);
        }
      },
        error => { console.log('no all Data Cat'); });
  }

  // search All
  loadsearchProductallpage() {
    var postData = JSON.stringify({
      txtsearch: this.getsearch,
      grouplevelid: this.getgroupId,
      filterBrand: this.filterBrand,
      fillterPrice: this.filterPricelength,
      token: this.base_token
    });
    console.log('searchAll', postData);
    this.http.post(this.base_url + 'ProductsBranch/getproductsearchyall', postData)

      .subscribe(data => {
        if (data['status'] == 200) {
          this.pageAllpage = data['pro_allpage'];
          this.pageAllproduct = data['pro_all'];
          this.allbrandItems = data['allbrand'];
          this.priceLanght.min = parseFloat(data['minPrice']);
          this.priceLanght.max = parseFloat(data['maxPrice']);
          this.getcatID = parseInt(data['groupid']);
          this.trail = data['trail'];
          console.log('Allproduct ', this.pageAllproduct);
          console.log('Allpage ', this.pageAllpage);
        }
      },
        error => {
          this.pageAllpage = 0;
          this.pageAllproduct = 0;
          this.allbrandItems = [];
          console.log('no all Data search');
        });
  }

  loadCatbanner() {
    var postData = JSON.stringify({ catid: this.getcatID, token: this.base_token });

    this.http.post(this.base_url + 'ProductsBranch/getbannercategory', postData)

      .subscribe(data => {
        if (data['status'] == 200) {
          this.imgBanner = data['catbanner'];
          console.log('banner img', this.imgBanner);
        }
      },
        error => { console.log('no Data '); });
  }

  loadProductInfinite(infiniteScroll) {
    setTimeout(() => {

      if (this.getsearch == null || this.getsearch == "") { // ถ้าไม่ได้มาจาก Search
        if (this.pageNumber <= this.pageAllpage) {
          this.pageNumber++;
          this.loadProduct(this.pageNumber);
        }
      } else { // ถ้าไม่ได้มาจาก cat
        if (this.pageNumber <= this.pageAllpage) {
          this.pageNumber++;
          //this.loadProduct(this.pageNumber);
          this.loadProductSearch(this.pageNumber);
        }

      }

      //console.log('Async operation has ended');
      infiniteScroll.target.complete();

    }, 500);
  }

  gotoSearch() {

    if (this.getsearch == "") {
      // this.navCtrl.push(SearchproductPage); 
      let navigationExtras: NavigationExtras = {
        state: {
          // searchactive: true
        }
      };
      this.router.navigate(['SearchproductPage'], navigationExtras);


    } else {
      // this.navCtrl.pop();
    }
  }

  selectFillter(ev: any) {

    if (ev.detail.value == 'HIT') {

    } else if (ev.detail.value == 'PROMOT') {
      this.productData = null;
      this.orderbyFilter = " ORDER BY pro_sale_price DESC ";// เซ็ต
      console.log('Fillter', ev.detail.value);

    } else if (ev.detail.value == 'TOPSALE') {
      console.log('Fillter', ev.detail.value);
    }/* else if(ev.value == 'PRICE'){
    this.selectOrderPrice();
    } */
    console.log('Fillter', ev.detail.value);
    // reload Product filter
    this.productData = null;
    this.CheckFromLoadProduct();

  }

  // open select Filter
  async openFilterpopup() {

    /* const modal = await this.modalCtrl.create({
      component: SelectFilterPage,
      componentProps: {
        allBrand: this.allbrandItems,
        catID: this.getcatID,
        priceLanght: this.priceLanght,
      },
      showBackdrop: true,
      backdropDismiss: true,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation
    });
    modal.onDidDismiss().then((dataReturned) => {
      //callback modal
      const data = dataReturned.data;
      if (data != null) {

        this.filterBrand = data.filterBrand; // simple date 'brandName1','brandName2','brandName3'
        this.filterPricelength = data.filterprice; // simple date $price1 AND $price2
        this.productData = null;

        if (data.catID > 0) {
          // this.getcatID = data.catID;
          this.gotosubcat(data.catID);
        } else {
          this.CheckFromLoadProduct();
        }

        console.log("filterBrand " ,  this.filterBrand ,this.filterPricelength);
      }
    });
    return await modal.present().then(ret => {
      // console.log('Sending: ', this.lunch);
    }); */

  }

  /* 
    async onClickremark(number: any) {
      const modal = await this.modalController.create({
        component: RemarkPage,
        componentProps: {
          number: number,
          datastatus: this.datastatus,
          employeecode: this.employeecode
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        console.log(dataReturned['dismissed']);
        console.log(dataReturned);
        if (dataReturned.data['dismissed'] == true) {
  
           */

  async selectOrderPrice() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'เลือกการจัดเรียงสินค้า',
      buttons: [
        {
          text: 'ราคาจาก น้อย -> มาก',
          handler: () => {
            this.productData = null;
            this.orderbyFilter = " ORDER BY pro_price ASC "; // ราคา น้อยไปมาก
            // reload Product filter
            this.CheckFromLoadProduct();
          }
        },
        {
          text: 'ราคาจาก มาก -> น้อย',
          handler: () => {
            this.productData = null;
            this.orderbyFilter = " ORDER BY pro_price DESC "; // ราคา มากไปน้อย
            // reload Product filter
            this.CheckFromLoadProduct();
          }
        }
      ]
    });
    actionSheet.present();
  }

  gotosubcat(catID: number) {
    this.getcatID = catID;
    this.getsearch = "";
    this.getgroupId = 0;
    this.tabselect = "PROMOT";
    this.orderbyFilter = " ORDER BY pro_sale_price DESC ";
    this.priceLanght.min = 0;
    this.priceLanght.max = 10;
    this.productData = null;
    this.CheckFromLoadProduct();
  }

  gotoProductDetail(item) {

    console.log('product_barcode cate', item);

    let navigationExtras: NavigationExtras = {
      state: {
        product_item:item,
      }
    };
    this.router.navigate(['product'], navigationExtras);

  }



  // loading popup
  private popupLoading() {
    this.loader = this.loadingCtrl.create({
      //   content: '<div class="custom-spinner-container"><div class="custom-spinner-box"></div>กำลังโหลด...</div>',
      message: '<div class="custom-spinner-container"><div class="custom-spinner-box"></div>กำลังโหลด...</div>',
      duration: 1000
    });
    this.loader.present();
    //loader.dismiss();
  }

  private popupDismiss() {
    this.loader.present().then(() => {
      this.loader.dismiss();
    });

  }

  async removeBrandConfirm() {
    let alert = await this.alertCtrl.create({
      header: 'ยืนยันนำตัวกรองออก',
      message: "คุณต้องการนำตัวกรองยี่ห้อ " + this.filterBrand + " ออกหรือไม่?",
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: 'นำออก',
          handler: () => {
            this.filterBrand = "";
            this.productData = null;
            this.CheckFromLoadProduct();
          }
        }
      ]
    });
    alert.present();
  }

  async removePriceConfirm() {
    let alert = await this.alertCtrl.create({
      header: 'ยืนยันนำตัวกรองออก',
      message: 'คุณต้องการนำตัวกรองราคาออก หรือไม่?',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: 'นำออก',
          handler: () => {
            this.filterPricelength = "";
            this.productData = null;
            this.CheckFromLoadProduct();
          }
        }
      ]
    });
    alert.present();
  }

}
