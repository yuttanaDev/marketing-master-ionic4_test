import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogGridPage } from './catalog-grid.page';

describe('CatalogGridPage', () => {
  let component: CatalogGridPage;
  let fixture: ComponentFixture<CatalogGridPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogGridPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogGridPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
