import { HttpService } from './../services/http.service';
import { Component, Injector, ViewChild, OnInit, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController, Events, ActionSheetController } from '@ionic/angular';
import { DismissModalPage } from '../dismiss-modal/dismiss-modal.page';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { myEnterAnimation } from "../../animations/enter";
import { myLeaveAnimation } from "../../animations/leave";
import { TranslateService } from '@ngx-translate/core';
import { BASEURL, URLIMG, API_URL } from '../../providers/constants/constants';
import { from } from 'rxjs';
@Component({
  selector: 'app-listcustomer',
  templateUrl: './listcustomer.page.html',
  styleUrls: ['./listcustomer.page.scss'],
})
export class ListcustomerPage implements OnInit {

  defaultImage = 'assets/No_Image.jpg';
  imageToShowOnError = 'assets/No_Image.jpg';

  public linkimg: any = API_URL + 'UploadedLocation/';
  public isShown = true;

  public posts: any;
  public items: any;
  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public docno: any;
  public branchname: any;
  public select_survey_type: any;

  constructor(private nav: NavController,
    public menu: MenuController,
    public datepipe: DatePipe,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private http: HttpClient,
    public translate: TranslateService,
    public alertCtrl: AlertController, 
    public HttpService :  HttpService,
    ) {

    this.storage.get('employeecode').then((employeecode) => {
      this.employeecode = employeecode;
      console.log(employeecode);

    });

    this.translate.get('addlistlocation.Select_survey_type').subscribe((res: string) => {
      this.select_survey_type = res;
      console.log('menu OK', this.select_survey_type);
    });

    //get
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.docno = this.router.getCurrentNavigation().extras.state.doc_no;
        this.newbranch = this.router.getCurrentNavigation().extras.state.branchcode;
        this.branchname = this.router.getCurrentNavigation().extras.state.branchname;
        //โชว์รายชื่อคนออกสำรวจ
        this.loadData();
      }
    });

  }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.doRefresh();
  }
  doRefresh() {

    this.isShown = true;
    this.items = [];
    setTimeout(() => {
      this.loadData();
    }, 1500);

  }

  async loadData() {

    let myData = {docno : this.docno};
    this.HttpService.post("survey/customerlist",myData).subscribe(
      (res : any) => {
        this.posts = res;
        this.initializeItems();
        console.log('typecustomer_list', this.posts);
        this.isShown = false;
      }, error => {
        this.isShown = false;
      }
    );

    // this.http.get(API_URL + 'customer_list.php?docno=' + this.docno)
    //   .subscribe(data => {
    //     this.posts = data;
    //     this.initializeItems();
    //     console.log('typecustomer_list', this.posts);
    //     this.isShown = false;
    //   }, error => {
    //     this.isShown = false;
    //     //console.log(JSON.stringify(error.json()));
    //   });

    setTimeout(() => {
      this.isShown = false;
    }, 10000);

  }
  initializeItems() {
    this.items = this.posts;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.fullname.toLowerCase().indexOf(val.toLowerCase()) > -1) || (item.mobile.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  itemFabAddCustomer() {

    let navigationExtras: NavigationExtras = {
      state: {
        gbh_customer_id: null,
        branchcode: this.newbranch,
        branchname: this.branchname,
        identification_card:null,
        customer_barcode: null,
        doc_no: this.docno
      }
    };
    this.router.navigate(['globalclub'], navigationExtras);

  }

  itemTapped(item:any) {
    let navigationExtras: NavigationExtras = {
      state: {
        gbh_customer_id: item.gbh_customer_id,
        branchcode: this.newbranch,
        branchname: this.branchname,
        identification_card:item.identification_card,
        customer_barcode: item.customer_barcode,
        doc_no: this.docno
      }
    };
    this.router.navigate(['globalclub'], navigationExtras);
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menu.enable(false);
  }
  /* ionViewWillEnter() {
    this.menu.enable(false);
  } */

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menu.enable(true);
  }


}

