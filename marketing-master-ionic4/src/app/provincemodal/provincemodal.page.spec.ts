import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvincemodalPage } from './provincemodal.page';

describe('ProvincemodalPage', () => {
  let component: ProvincemodalPage;
  let fixture: ComponentFixture<ProvincemodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvincemodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvincemodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
