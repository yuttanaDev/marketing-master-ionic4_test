import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProvincemodalPage } from './provincemodal.page';

const routes: Routes = [
  {
    path: '',
    component: ProvincemodalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
 // declarations: [ProvincemodalPage]
})
export class ProvincemodalPageModule {}
