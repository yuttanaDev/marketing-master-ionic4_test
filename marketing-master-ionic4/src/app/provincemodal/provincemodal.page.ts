import { Component, OnInit } from '@angular/core';
import { Platform, NavController, AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { BASEURL, URLIMG, API_URL, token } from '../../providers/constants/constants';

@Component({
  selector: 'app-provincemodal',
  templateUrl: './provincemodal.page.html',
  styleUrls: ['./provincemodal.page.scss'],
})
export class ProvincemodalPage implements OnInit {

  public items: any = [];
  public itemsAll: any = [];
  public datacode: any = null;

  public typepage: any;
  public province_id: any = 0;
  public amphur_id: any = 0;
  constructor(public navCtrl: NavController,
    public platform: Platform,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public http: HttpClient,
    public modalController: ModalController,
    public navParams: NavParams,
    private router: Router) {

    this.typepage = navParams.get('typepage');
    this.province_id = navParams.get('province_id');
    this.amphur_id = navParams.get('amphur_id');

    this.loadProvice();
  }

  // load Provice
  loadProvice() {

    let Link_url;
    if (this.typepage == 1) {
      Link_url = 'addressglobalclub.php?idaddress=' + 1;
    }
    else if (this.typepage == 2) {
      Link_url = 'addressglobalclub.php?idaddress='+ 2 +'&province_id=' +this.province_id;
    } else if (this.typepage == 3) {
      Link_url = 'addressglobalclub.php?idaddress=' + 3 + '&province_id=' + this.province_id + '&amphur_id=' + this.amphur_id;
    }
    this.http.get(API_URL + Link_url)
      .subscribe(data => {
        if (data != null) {
          this.itemsAll = data;
          this.items = this.itemsAll;
          //console.log(data);
        }
      }, error => {
        this.items = [];

      });
  }

  fillter(ev) {
    this.items = this.itemsAll;
    var val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        if (this.typepage == 1) {
          return (item.province_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
        if (this.typepage == 2) {
          return (item.amphur_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
        if (this.typepage == 3) {
          return (item.district_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }

      })
    }
  }

  selectedItem(item: any) {



    if (this.typepage == 1) {
      let province_id = item.province_id;
      let province_name = item.province_name;
      let region_id = item.region_id;

      this.datacode = { 'province_id': province_id, 'province_name': province_name, 'region_id': region_id }
    }
    if (this.typepage == 2) {
      let amphur_id = item.amphur_id;
      let amphur_name = item.amphur_name;
      let province_id = item.province_id;

      this.datacode = { 'amphur_id': amphur_id, 'amphur_name': amphur_name, 'province_id': province_id }

    }
    if (this.typepage == 3) {
      let district_id = item.district_id;
      let district_code = item.district_code;
      let district_name = item.district_name;

      this.datacode = { 'district_id': district_id, 'district_code': district_code, 'district_name': district_name }

    }


    console.log(this.datacode);
    this.modalController.dismiss(this.datacode);
  }

  closeModal() {
    this.modalController.dismiss(this.datacode);
  }


  ngOnInit() {
  }

}
