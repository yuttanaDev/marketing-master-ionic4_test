import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AlertController } from '@ionic/angular';
import { IonItemSliding } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router, NavigationExtras,ActivatedRoute } from '@angular/router';
import { LoadingController, Platform, NavController, MenuController, Events } from '@ionic/angular';


import { API_URL } from '../../providers/constants/constants';

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {

  price: any = '';
  dataparams: any;
  items: any = [];
  isShown = true;
  rival_id: any;
  rival_code: any;
  rival_name: any;

  public newbranch: any;
  public namebranch: any;
  public employeecode: any;
  public datacount: any = {};

  constructor(public menu: MenuController,
    public datepipe: DatePipe,
    public alertController: AlertController,
    public storage: Storage,
    public platform: Platform,
    public events: Events,
    public router: Router,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public http: HttpClient) {


      let dateString = new Date();
      let date = new Date(dateString.getFullYear(), dateString.getMonth(), 1);
  
      let dateString2 = new Date();
      let date2 = new Date(dateString2.getFullYear(), dateString2.getMonth(), dateString2.getDate());
  
      this.datacount.datefirst = this.datepipe.transform(date, 'yyyy-MM-dd');
      this.datacount.dateend = this.datepipe.transform(date2, 'yyyy-MM-dd');


    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.dataparams = this.router.getCurrentNavigation().extras.state.data;
        console.log('second ',this.dataparams);
        this.rival_id = this.dataparams.rival_id ;
        this.rival_code = this.dataparams.rival_code ;
        this.rival_name =this.dataparams.rival_name ;

        /* if (this.datacount.rival_add_id == null  || this.datacount.rival_add_id == '' || this.datacount.rival_add_id == undefined ) {
          console.log("rival_add_id!" + this.datacount.rival_add_id);
          this.addEditid();
        } else {
          //this.addEditid();
          this.addEditid();

        } */

      }
    });
  }

  ionViewWillEnter() {


  
    this.items = [];
    this.storage.get('namebranch').then((namebranch) => {
      this.namebranch = namebranch;
    });
    this.storage.get('newbranch').then((newbranch) => {
      this.newbranch = newbranch;
      this.storage.get('employeecode').then((employeecode) => {
        this.employeecode = employeecode;
        this.productlist();
      });

    });


    
  }

  presentConfirm(){
    this.isShown = true;
    this.items = [];
    this.productlist();
  }

  async productlist() {
    

    let myData = {  
      sentempcode:this.employeecode,
      datefirst: this.datacount.datefirst,
      dateend: this.datacount.dateend,
      newbranch: this.newbranch,
      rival_id: this.rival_id, 
      rival_code: this.rival_code,
      rival_name: this.rival_name,
      tokenapi:'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
    };

    this.http.post(API_URL + 'CompetitorSurvey/select_rival_product_new.php', JSON.stringify(myData))
      .subscribe(data => {

        console.log('load time', data);
        
        //this.posts2 = data;
        // this.items = data; 
        if(data != null){

          this.items = data;
          //this.initializeItems();
          console.log(this.items);
          setTimeout(() => {
            this.isShown = false;
          }, 1000);

        }else{

          setTimeout(() => {
            this.isShown = false;
          }, 1000);
        }




      }, error => {
        setTimeout(() => {
          this.isShown = false;
        }, 1000);
        //console.log(JSON.stringify(error.json()));
      });

  }

  goToAdd(typepage:any,item:any){
    

    let itemdata;
    if(typepage == 1){
      itemdata = item;
    }else{
      itemdata = this.dataparams;
    }
    console.log('idpage',typepage,itemdata);
    let navigationExtras: NavigationExtras = {
      state: {
        data: itemdata,
        typepage:typepage
      }
    };
    this.router.navigate(['listbillscan'], navigationExtras);
  }
  async removeProduct(item: any, slidingItem: IonItemSliding){
    slidingItem.close();

    const alert = await this.alertController.create({
      header: 'ต้องการลบสินค้านี้',
      message: 'สินค้าชื่อ  ' + '<strong>' + item.product_name + '</strong>!!! นี้',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ลบสินค้านี้',
          handler: () => {
            console.log('Confirm Okay');
            //ฟังก์ชั่นลบสินค้า จำนวนสินค้า
            let myData = {  
              rival_add_id: item.rival_add_id,
              tokenapi:'EAAGiRJBp4S8BAIm9yMdGRrkH99qgNE1sIaipyIxiit1E1CIXwVsy3ui4ZCsfapH85ewjb5ZCZAxMFpEdTXLw54Tu6WFxTnwbQPwBskZB2rAaywgleu36HAjUmirbXDSWvEI01dNEoTZBZCaWslv170ZCzYAkCnClag1hFR4qT6RJ9E27EuyOctO3oZBlRzoELo4ZD',
            };
            let link = API_URL +'CompetitorSurvey/Delete_listproduct_rival.php';
            this.http.post(link, JSON.stringify(myData))
                .subscribe(data => {
                  //loader.dismissAll();
                  this.items= [];
                  this.productlist();

                  console.log('Agree data',data);
                  
              }, error => {
               // loader.dismissAll();
                //console.log(JSON.stringify(error.json()));
            });
          }
        }
      ]
    });

    await alert.present();


  }

  ngOnInit() {
  }

}
