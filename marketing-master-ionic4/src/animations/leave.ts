import { Animation } from '@ionic/core';

export function myLeaveAnimation(AnimationC: Animation, baseEl: HTMLElement): Promise<Animation> {

    const baseAnimation = new AnimationC();

    const backdropAnimation = new AnimationC();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));

    const wrapperAnimation = new AnimationC();
    const wrapperEl = baseEl.querySelector('.modal-wrapper');
    wrapperAnimation.addElement(wrapperEl);
    const wrapperElRect = wrapperEl!.getBoundingClientRect();

    wrapperAnimation
      .beforeStyles({ 'transform': 'translateY(100%)', 'opacity': 1 })
      .fromTo('transform', 'translateY(0)', 'translateY(100%)')
      .fromTo('opacity', 1, 1);

    backdropAnimation.fromTo('opacity', 1, 0);

    return Promise.resolve(baseAnimation
      .addElement(baseEl)
      .easing('cubic-bezier(.1, 1, .1, 1)')
      .duration(200)
      .add(backdropAnimation)
      .add(wrapperAnimation));

}