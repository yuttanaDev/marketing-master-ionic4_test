//import { Injectable } from '@angular/core';
/* import {Http, Headers,Response, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map'; */

import { NetworkServiceProvider } from '../network-service/network-service';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../constants/constants';
import { Injectable } from '@angular/core';
@Injectable()
export class ApiServiceProvider {

  isSyncNeeded: boolean;
  
  constructor(public HttpClient: HttpClient, public networkServiceProvider: NetworkServiceProvider) {
    console.log('Hello ApiServiceProvider Provider');
    console.log('this.networkServiceProvider.isConnected :', this.networkServiceProvider.getConnectionStatus());
  }

  //addlistlocation
  typeLocation() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {
        //console.log('addproduct() barcode : ', params.barcode );
        /* let requestData = {
           "items": params.items,
           "barcode": params.barcode,
           "emp": params.emp,
           "branch": params.branch, 
        } */
        let requestData = {};
        self.HttpClient.post(API_URL+'suvey_type_customer.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
          console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }


  //addlocation
  subtypeLocation() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'subtype_location.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }

  subtypeGlobalclub() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'typesub_globalclub.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }
  masterCategory() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'master_category.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }
  masterGroup() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'master_group.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }
  masterPattern() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'master_pattern.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }
  masterDelivery() {
    let self = this;
    return new Promise((resolve, reject) => {
      if (self.networkServiceProvider.getConnectionStatus()) {

        let requestData = {};
        self.HttpClient.post(API_URL+'OrderNow/delivery_cost_type.php', JSON.stringify(requestData))
	        .subscribe(response => {
	          //console.log('success addproduct response : ', response);
       
              resolve(response);
            
        }, err => {
         // console.log('response Error : ', err);
          let returnError = err;
          if (err._body){
            let error = JSON.parse(err._body);
            console.log('err._body', error);
            returnError = '';
          }
	        reject(returnError);
        });

      } else {
        	alert("Network connection error!");
      }

    });

  }

}
